<?php


namespace App\Traits;


use App\User;
use Firebase\JWT\JWT;

trait JWTTrait
{
    protected static function jwtEncode(User $user, $exp = null): string
    {
        $exp = $exp ?? time() + (8 * 60 * 60);
        $payload = [
            'iss' => config('app.name'), // Issuer of the token
            'sub' => $user['ID'], // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => $exp, // Expiration time,
            'data' => [
                'user' => collect($user)->toArray()
            ]
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    protected static function jwtDecode($jwt): object
    {
        return JWT::decode($jwt, env('JWT_SECRET'), ['HS256']);
    }

    protected static function jwtHeaderExtractor($authorization) {
        [$jwt] = sscanf($authorization, 'Bearer %s');
        return $jwt;
    }
}
