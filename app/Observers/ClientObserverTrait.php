<?php


namespace App\Observers;

use Illuminate\Support\Str;

trait ClientObserverTrait
{
    protected static function boot() {

        parent::boot();

        static::saving(function($user) {
            $user->setAttribute($user->getKeyName(), strtoupper(Str::uuid()));
            $user->setAttribute('Sex', app('utility')->resolveGender($user->Sex));
            $user->setAttribute('Zip', app('utility')->resolveZipCode($user->Zip));
            $user->setAttribute('phone', app('utility')->resolvePhone($user->phone));
        });
    }
}
