<?php

namespace App;

use App\Observers\ClientObserverTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class User extends Model
{
    use ClientObserverTrait;

    protected $table = 'dbo.Client';

    protected $keyType = 'string';

    protected $primaryKey = 'ID';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Firstname',
        'Lastname',
        'Middlename',
        'Photo',
        'username',
        'email',
        'Ssn',
        'Dob',
        'Sex',
        'memberid',
        'Address1',
        'Address2',
        'City',
        'State',
        'Zip',
        'remember_token',
        'created_by',
        'email_verified_at',
        'password',
        'Npi',
        'Organization',
        'Longitude',
        'Latitude',
        'facility_name',
        'Race',
        'Ethnicity',
        'phone',
        'County',
        'sms_received_notification'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
