<?php

namespace App\Services;

use App\Helpers\Helpers;
use App\Models\ClientContact;
use App\Models\HL7Request;
use App\Models\Vaccination;
use App\Models\VaccineRouteAdmin;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Str;
use DB;

class Hl7Service
{
    public function send(Vaccination $vaccination)
    {
         // client
         $client = User::findOrFail($vaccination->patientid);

         if (Str::startsWith($client->Firstname, 'test') || Str::startsWith($client->Lastname, 'test')) {
             Log::info('[Hl7 Service] Patient is a test.');
             return false;
         }

         //get vaccine information
         $vaccine_information = DB::table('Vaccine_Information')->where('id', $vaccination->vaccine_information_id)->first();

         if (!$vaccine_information) {
            Log::error("[Hl7 Service] No Vaccine Information found ID: [{$vaccination->id}]");
            return false;
         }

        try {
            //check hl7 data first
            if (!$vaccine_information->msh_3 || !$vaccine_information->msh_4 || !$vaccine_information->msh_8 || !$vaccine_information->hl7_endpoint || !$vaccine_information->hl7_targetnamespace || !$vaccine_information->order_provider) {
                Log::error("[Hl7 Service] Missing HL7 information in Vaccine_Information ID: [{$vaccination->id}]");
                return false;
            }

            $client_contact = ClientContact::where('ClientID',$vaccination->patientid)->where('Type','Home')->first();

            // vaccinator
            $vaccinator = User::findOrFail($vaccination->vaccinator_id);

            /*
            *----------
            * PID START
            *----------
            */
            $patient_id = 1;
            $patient_internal_identifier = $client->SeqNum;
            $patient_lastname = $client->Lastname;
            $patient_firstname = $client->Firstname;
            $patient_dob = Carbon::parse($client->Dob)->format('Ymd');
            $patient_gender = $client->Sex;
            $patient_ssn = $client->Ssn;

            // patient address
            $patient_street = $client->Address1;
            $patient_city = $client->City;
            $patient_state = $client->State;
            $patient_postal_code = $client->Zip;

            // patient contact
            $patient_home_phone = (isset($client_contact->Number) ? str_replace(' ','',$client_contact->Number) : '');
            /*
            *----------
            * PID END
            *----------
            */

            /*
            *----------
            * ORC START
            *----------
            */
            $filler_order_number = $client->SeqNum;
            $transaction_date = Carbon::now()->format('Ymd');
            $entered_by = $vaccinator->SeqNum.'^'.$vaccinator->Lastname.'^'.$vaccinator->Firstname; // compose of npi^lastname^firstname
            $order_provider = $vaccine_information->order_provider; // compose of npi^lastname^firstname - ex. 1801824727^SMITH^MICHAEL
            $entering_organization = '^'.$vaccination->entering_organization; // compose of npi^name^NPI - ex. 123^123^NPI
            /*
            *----------
            * ORC END
            *----------
            */

            /*
            *----------
            * RXA START
            *----------
            */
            $vaccine_date = Carbon::parse($vaccination->vaccine_date)->format('Ymd');
            $cvx = $vaccination->cvx_code.'^'.$vaccination->cvx.'^CVX'; // compose of cvx code and description - ex. 43^hepatitis B vaccine, adult dosage^CVX
            $dosage = $vaccination->dosage;
            $admenistering_provider = $vaccinator->SeqNum.'^'.$vaccinator->Lastname.'^'.$vaccinator->Firstname; // compose of npi, lastname and firstname - ex. 123^MORRISON^VIRGINIA
            $msh_4 =  $vaccine_information->msh_4;
            $lot = $vaccination->lot_no;
            $vaccine_expiration_date = Carbon::parse($vaccination->vaccine_exp_date)->format('Ymd');
            $manufacturer = $vaccination->mvx_code.'^'.$vaccination->mvx.'^MVX'; // compose of code and name - ex. MSD^Merck and Co., Inc.^MVX
            $completion_status = ($vaccination->vaccine_series_complete=='Y' ? 'CP': 'PA');
            /*
            *----------
            * RXA END
            *----------
            */

            /*
            *----------
            * RXR START
            *----------
            */
            // get vaccine route details
            $vaccince_route_admin = VaccineRouteAdmin::where('vaccine_route','=',$vaccination->vaccine_route_admin)->first();
            $vaccince_route_code = $vaccince_route_admin->vaccine_route_code;
            $vaccine_route = $vaccince_route_admin->vaccine_route;

            // set RXR data
            $route = $vaccince_route_code.'^'.$vaccine_route.'^NCIT';
            $admin_site = implode('^',explode(' - ',$vaccination->vaccine_admin_site)).'^HL70163';
            /*
            *----------
            * RXR START
            *----------
            */

            // set hl7 data
            $env = 'T';
            $msh_3 = $vaccine_information->msh_3;
            $msh_4 = $vaccine_information->msh_4;
            $msh_8 = $vaccine_information->msh_8;
            $endpoint = $vaccine_information->hl7_endpoint;
            $parseDate = date('YmdHi');
            $hl7_target_namespace = $vaccine_information->hl7_targetnamespace;

            $patient_home_phone = Helpers::cleanPhoneNumber($client->phone);
            //race
            $race_code = '';
            if ($client->Race == 'American Indian or Alaska Native') {
                $race_code = '1002-5';
            } else if ($client->Race == 'Asian') {
                $race_code = '2028-9';
            } else if ($client->Race == 'Black or African American') {
                $race_code = '2054-5';
            } else if ($client->Race == 'Native Hawaiian or other Pacific Islander') {
                $race_code = '2076-8';
            } else if ($client->Race == 'White') {
                $race_code = '2106-3';
            }

            //ethnicity
            $ethinicity_code = '';
            if ($client->Ethnicity == 'Not Hispanic or Latino') {
                $ethinicity_code = '2186-5';
            } else if ($client->Ethnicity == 'Hispanic or Latino') {
                $ethinicity_code = '2135-2';
            }

            if(env('APP_ENV')=='production') { $env = 'P'; }

$body = '
<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
<Body>
    <aliiashl7 xmlns="'.$hl7_target_namespace.'">
        <payload>MSH|^~\&amp;|'. $msh_3 .'|'. $msh_4 .'|ImmPRINT|ImmPRINT|'.$parseDate.'|'. $msh_8 .'|VXU^V04^VXU_V04|123|'.$env.'|2.5.1|||ER|AL|USA||||Z22^CDCPHINVS
PID|'.$patient_id.'||'.$patient_internal_identifier.'^^^^PI||'.$patient_lastname.'^'.$patient_firstname.'||'.$patient_dob.'|'.$patient_gender.'||'.$race_code.'|'.$patient_street.'^^'.$patient_city.'^'.$patient_state.'^'.$patient_postal_code.'^USA||'.$patient_home_phone.'||||||'.$patient_ssn.'|||'.$ethinicity_code.'
ORC|RE||'.$filler_order_number.'||||||'.$transaction_date.'|'.$entered_by.'||'.$order_provider.'^^^^^^^^^^NPI|||||'.$entering_organization.'^
RXA|1|1|'.$vaccine_date.'||'.$cvx.'|'.$dosage.'|mL^per MilliLiter^UCUM||00^New Immunization Record^NIP001|'.$admenistering_provider.'|'.$msh_4.'||||'.$lot.'|'.$vaccine_expiration_date.'|'.$manufacturer.'|||'.$completion_status.'|A
RXR|'.$route.'|'.$admin_site.'</payload>
    </aliiashl7>
</Body>
</Envelope>
';

            // curl
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $endpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>$body,
                CURLOPT_SSL_VERIFYPEER=>false,
                CURLOPT_VERBOSE=>true,
                CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
            ));
            $status = curl_getinfo($curl,CURLINFO_HTTP_CODE);
            $response = curl_exec($curl);
            $error = false;

            if($response === false) {
                // return response
                $error = 'Curl error: ' . curl_error($curl);
                $response_response = $error;
            }

            curl_close($curl);

            // check response
            $response = strtolower($response);
            if ( $error || strpos($response,'error')!==false || strpos($response,'err')!==false || strpos($response,'failed')!==false || strpos($response,'faultcode')!==false) {
                // update sent to adph date
                $status = 'error';
                $response = $response_response ?? $response;
                Log::error('[Hl7 Service] ' . $response);
            } else {
                Log::info('[Hl7 Service] imprint sent.');
                $vaccination->sent_to_adph_date = Carbon::now();
                $vaccination->save();
            }

            // save hl7 request
            $hl7_request = new HL7Request;
            $hl7_request->client_id = $vaccination->patientid;
            $hl7_request->status = $status;
            $hl7_request->request = $body;
            $hl7_request->response = $response;
            $hl7_request->vaccinations_id = $vaccination->id;
            $hl7_request->save();
            Log::info('[Hl7 Service] Hl7 record created.');
        } catch (\Exception $e) {
			Log::error('[Hl7 Service] exception error.', [
                'exception' => $e->getMessage(),
            ]);
	    }
	}
  }
