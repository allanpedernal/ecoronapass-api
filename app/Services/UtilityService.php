<?php


namespace App\Services;


use App\User;
use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use PDO;

class UtilityService
{
    public static function validateRequest($validator): array
    {
        $err_arr = [];
        if ($validator->fails()) {
            $has_error = checkEmptyParam($validator->errors()->all());
            if (count($has_error) > 0) {
                foreach ($validator->errors()->messages() as $key => $err) {
                    if (is_array($err)) {
                        foreach ($err as $item) {
                            $err_arr[$key] = $item;
                        }
                    }
                }
            }
        }
        return $err_arr;
    }

    public function attempt($data = []) {
        $username = $data['username'];
        $password = $data['password'];
        $user = null;
        #check username.
        $client = User::where('username', $username)->first();
        if ($client) {
            #check password.
            if (Hash::check($password, $client->password)) {
                $user = $client;
            }
        }
        return $user;
    }

    public function clientExistingEntityVerifier($id, $entity, $column, $table, $new = false): bool
    {
        $connection = app('pdo_connection')->getPdo();
        $bool = true;

        if ($new) {
            $sql = "SELECT * FROM $table WHERE $column = :entity";
            $statement = $connection->prepare($sql);
            $statement->execute([':entity' => $entity]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if ($result) $bool = false;
        } else {
            $sql = "SELECT * FROM $table WHERE ID = :id";
            $statement = $connection->prepare($sql);
            $statement->execute([':id' => $id]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                if ($result[$column] !== $entity) {
                    #check column existence.
                    $sql = "SELECT * FROM $table WHERE $column = :entity";
                    $statement = $connection->prepare($sql);
                    $statement->execute([':entity' => $entity]);
                    $result = $statement->fetch(PDO::FETCH_ASSOC);
                    if ($result) $bool = false;
                }
            }
        }
        return $bool;
    }

    public function resolveGender($gender) {
        if (strlen($gender) > 1) {
            $gender = strtoupper(substr($gender, 0,1));
        } else if (strlen($gender) === 1) {
            $gender = strtoupper($gender);
        }
        return $gender;
    }

    public function resolveZipCode($zip) {
        return substr($zip, 0, 5);
    }

    public function resolvePhone($phone) {
        return preg_replace('~\D~', '', $phone);
    }

    public function generateRandomPin($digit): string
    {
        $i = 0;
        $pin = '';
        while($i < $digit){
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function sendVerificationCode($phone, $message, $other = null): bool
    {
        $success = false;
        $sns = new SnsClient([
            'region' => env('SNS_REGION'),
            'version' => env('SNS_VERSION'),
            'credentials' => [
                'key' => env('SNS_KEY'),
                'secret' => env('SNS_SECRET')
            ]
        ]);

        try {
            $result = $sns->publish([
                'Message' => $message,
                'PhoneNumber' => '+1' . $phone
            ]);
            Log::channel('SMS_VERIFICATION_LOG')->info('SENT SMS Result [SUCCESS] ' . date('Y-md'), [
                'result' => $result,
                'receiver' => $other
            ]);
            $success = true;
        } catch (SnsException | Exception $e) {

            Log::channel('SMS_VERIFICATION_LOG')->info('SENT SMS Result [ERROR] ' . date('Y-md'), [
                'result' => $e->getMessage(),
                'receiver' => $other
            ]);
        }
        return $success;
    }

    public function sendAppointmentSmsNotification($phone, $message, $other = null): bool
    {
        $success = false;
        $sns = new SnsClient([
            'region' => env('SNS_REGION'),
            'version' => env('SNS_VERSION'),
            'credentials' => [
                'key' => env('SNS_KEY'),
                'secret' => env('SNS_SECRET')
            ]
        ]);

        try {
            $result = $sns->publish([
                'Message' => $message,
                'PhoneNumber' => '+1' . $phone
            ]);
            Log::channel('SMS_APPOINTMENT_NOTIF_LOG')->info('SENT SMS Result [SUCCESS] ' . date('Y-md'), [
                'result' => $result,
                'receiver' => $other
            ]);
            $success = true;
        } catch (SnsException | Exception $e) {

            Log::channel('SMS_APPOINTMENT_NOTIF_LOG')->info('SENT SMS Result [ERROR] ' . date('Y-md'), [
                'result' => $e->getMessage(),
                'receiver' => $other
            ]);
        }
        return $success;
    }

    public function phoneVerification($phone): array
    {
        $invalid = false;
        $msg = 'Invalid phone';

        if (!$phone) $invalid = true;

        for ($i = 0; $i <= strlen($phone)-1; $i++) {
            if(!is_numeric($phone[$i]))  {
                $invalid = true;
                break;
            }
        }

        if (!$invalid) {
            $connection = app('pdo_connection')->getPdo();
            $sql = "SELECT phone FROM dbo.Client WHERE phone = :phone";
            $statement = $connection->prepare($sql);
            $statement->execute([':phone' => $phone]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if (!$result) {
                $invalid = true;
                $msg = 'Not exist in our records!';
            }
        }
        return compact('invalid', 'msg');
    }
}
