<?php

namespace App\Services;

use App\Helpers\Helpers;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Str;

class Hl7DemographicsUpdateService
{
    public function send($patients)
    {
        foreach ($patients as $key => $client) {
            //skip test patient
            if (Str::startsWith($client->Firstname, 'test') || Str::startsWith($client->Lastname, 'test')) {
                Log::info('[Hl7DemographicsUpdate Service] Patient is a test.');
                continue;
            }

            //check hl7 data first
            if (!$client->msh_3 || !$client->msh_4 || !$client->msh_8 || !$client->hl7_endpoint || !$client->hl7_targetnamespace || !$client->order_provider) {
                Log::error("[Hl7DemographicsUpdate Service] Missing HL7 information in Patient ID: [{$client->ID}]");
                throw new \Exception("[Hl7DemographicsUpdate Service] Missing HL7 information in Patient ID: [{$client->ID}]");
            }

            /*
            *----------
            * PID START
            *----------
            */
            $patient_id = 1;
            $patient_internal_identifier = $client->SeqNum;
            $patient_lastname = $client->Lastname;
            $patient_firstname = $client->Firstname;
            $patient_dob = Carbon::parse($client->Dob)->format('Ymd');
            $patient_gender = $client->Sex;
            $patient_ssn = $client->Ssn;
            // patient address
            $patient_street = $client->Address1;
            $patient_city = $client->City;
            $patient_state = $client->State;
            $patient_postal_code = $client->Zip;
            // patient contact
            $patient_home_phone = Helpers::cleanPhoneNumber($client->phone);
            /*
            *----------
            * PID END
            *----------
            */

            // set hl7 data
            $env = env('APP_ENV')=='production' ? 'P' : 'T';
            $msh_3 = $client->msh_3;
            $msh_4 = $client->msh_4;
            $msh_8 = $client->msh_8;
            $endpoint = $client->hl7_endpoint;
            $hl7_target_namespace = $client->hl7_targetnamespace;
            $parseDate = date('YmdHi');

            //race
            $race_code = '';
            if ($client->Race == 'American Indian or Alaska Native') {
                $race_code = '1002-5';
            } else if ($client->Race == 'Asian') {
                $race_code = '2028-9';
            } else if ($client->Race == 'Black or African American') {
                $race_code = '2054-5';
            } else if ($client->Race == 'Native Hawaiian or other Pacific Islander') {
                $race_code = '2076-8';
            } else if ($client->Race == 'White') {
                $race_code = '2106-3';
            }

            //ethnicity
            $ethinicity_code = '';
            if ($client->Ethnicity == 'Not Hispanic or Latino') {
                $ethinicity_code = '2186-5';
            } else if ($client->Ethnicity == 'Hispanic or Latino') {
                $ethinicity_code = '2135-2';
            }

            try {

$body = '
<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
<Body>
    <aliiashl7 xmlns="'.$hl7_target_namespace.'">
        <payload>MSH|^~\&amp;|'. $msh_3 .'|'. $msh_4 .'|ImmPRINT|ImmPRINT|'.$parseDate.'|'. $msh_8 .'|VXU^V04^VXU_V04|123|'.$env.'|2.5.1|||ER|AL|USA||||Z22^CDCPHINVS
PID|'.$patient_id.'||'.$patient_internal_identifier.'^^^^PI||'.$patient_lastname.'^'.$patient_firstname.'||'.$patient_dob.'|'.$patient_gender.'||'.$race_code.'|'.$patient_street.'^^'.$patient_city.'^'.$patient_state.'^'.$patient_postal_code.'^USA||'.$patient_home_phone.'||||||'.$patient_ssn.'|||'.$ethinicity_code.'</payload>
    </aliiashl7>
</Body>
</Envelope>';
                // curl
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $endpoint,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 5,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>$body,
                    CURLOPT_SSL_VERIFYPEER=>false,
                    CURLOPT_VERBOSE=>true,
                    CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
                ));
                $status = curl_getinfo($curl,CURLINFO_HTTP_CODE);
                $response = curl_exec($curl);
                $error = false;

                if($response === false) {
                    // return response
                    $error = 'Curl error: ' . curl_error($curl);
                    $response_response = $error;
                }

                curl_close($curl);

                // check response
                $response = strtolower($response);
                if ( $error || strpos($response,'error')!==false || strpos($response,'err')!==false || strpos($response,'failed')!==false || strpos($response,'faultcode')!==false) {
                    // update sent to adph date
                    $status = 'error';
                    $response = $response_response ?? $response;
                    Log::error('[Hl7DemographicsUpdate Service] ' . $response);
                } else {
                    Log::info('[Hl7DemographicsUpdate Service] '.$key.'. imprint patient demographic update Patient: '. $patient_firstname);
                    //update
                    $client = User::where('ID',$client->ID)->update(['Latitude' => 1]);
                }

            } catch (\Exception $e) {
                Log::error('[Hl7DemographicsUpdate Service] exception error.', [
                    'exception' => $e->getMessage(),
                ]);
            }
        }
	}
  }
