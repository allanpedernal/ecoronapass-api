<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use GuzzleHttp\Client;

class NpiValidationRules implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
     private $allParams;
     private $err_mess = 'The NPI you entered is Invalid.';

     public function __construct($params)
     {
         $this->allParams = $params;
     }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ret = false;
        if(trim($value) != ""){
            //use npi API to verify
            $client = new Client();

            //check if individual or organization
            if(isset($this->allParams['organization']) && trim($this->allParams['organization']) != ''){ //orgnaization
                $res = $client->request('GET', env('NPI_REGISTRY_URL').'&number='.$value.'&organization_name='.$this->allParams['organization']);
            }else{ //individual
                $res = $client->request('GET', env('NPI_REGISTRY_URL').'&number='.$value.'&first_name='.$this->allParams['firstname'].'&last_name='.$this->allParams['lastname']);
            }

            $status_code = $res->getStatusCode();
            $response = json_decode($res->getBody());

            if($status_code == 200){
                if(isset($response->Errors)){
                    $this->err_mess = 'NPI Error: '.$response->Errors[0]->description;
                    return false;
                }else{
                    if(isset($response->result_count) && $response->result_count > 0){
                        return true;
                    }else{
                        $this->err_mess = 'The NPI you entered is Invalid.';
                        return false;
                    }
                }
            }else{
                $this->err_mess = 'There was an error verifying your NPI. Please try again.';
                return false;
            }
        }else{
            if(isset($this->allParams['organization']) && trim($this->allParams['organization']) != ''){
                $this->err_mess = 'You entered an organizational provider but npi is missing.';
                return false;
            }

            //this is a patient
            $ret = true;
        }

        return $ret;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->err_mess;
    }
}
