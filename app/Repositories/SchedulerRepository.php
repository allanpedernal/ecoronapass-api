<?php

namespace App\Repositories;

use App\Models\PatientSurvey;
use App\Models\Setting;
use DB;

class SchedulerRepository
{
    public function surveyChecker($patient_id)
    {
        $survey_settings = Setting::where('setting','required_survey')->first();
        $return_survey = false;
        if ($survey_settings && $survey_settings->val) {
            $surveys = json_decode($survey_settings->val);
            $patient_surveys = PatientSurvey::where('patient_id', $patient_id)->whereIn('survey', $surveys)->get();
            $patient_current_survey = $patient_surveys->whereIn('status', ['approved','answered'])->pluck('survey')->toArray();
            if (count($patient_current_survey) < count($surveys)) {
                foreach ($surveys as $survey) {
                    if (! in_array($survey, $patient_current_survey)) {
                        $return_survey = $survey;
                        break;
                    }
                }
            }
        }
        return $return_survey;
    }
}

