<?php

namespace App\Repositories;

class CheckoutRepository
{
    public function patientList($request)
    {
        $search             = $request['search'];
        $columns            = $request['columns'];
        $start              = $request['start'];
        $length             = $request['length'];
        $order              = $request['order'];
        $draw               = $request['draw'];
        $order_by_column    = $order[0]['column'];
        $order_by_dir       = $order[0]['dir'];
        $find               = $search['value'] ?? '';

        $facility_id        = auth()->user()->facility_id;
        $date               = date("m/d/Y");

        $connection = $connection = app('sql_srv_connection')->getPdo();

        $statement = $connection->prepare(
            "EXEC sp_PatientList_CheckOut
                    @facility_id = :p_facility_id
                ,	@date = :p_date
                ,	@p_sort	= :p_sort
                ,	@p_offset = :p_offset
                ,	@p_limit = :p_limit
                ,	@p_find	= :p_find"
        );

        $order_inx = 1; //default to fullname
        switch ($order_by_column) {
            case 0: //fullname
                $order_inx = 1;
                break;
            case 1: //Mrn
                $order_inx = 2;
                break;
            case 2: //schedule
                $order_inx = 3;
                break;
            default: //fullname
                $order_inx = 1;
        }

        $sort_value = $order_inx . ' ' . $order_by_dir;
        $statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $start);
        $statement->bindParam(':p_limit', $length);
        $statement->bindParam(':p_find', $find);
        $statement->bindParam(':p_facility_id', $facility_id);
        $statement->bindParam(':p_date', $date);

        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $recordsTotal = $data ? $data[0]['cnt'] : 0;
        $recordsFiltered = $recordsTotal;

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }
}
