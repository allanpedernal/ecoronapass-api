<?php

namespace App\Repositories;

use App\Models\Facility;
use App\User;
use DB;
use Illuminate\Http\JsonResponse;
use PDO;

class ProviderRepository
{
    public function dashboard()
    {
        $events = app(AppointmentRepository::class)
        ->query()
        ->whereIn('status',['pending','approved'])
        ->whereDate('schedule.Start', '<=' ,date('Y-m-d'))
        ->get();
        $approved = $events->count();
        $pending = $events->where('status', 'pending')->count();
        return compact('approved','pending');
    }

    public function recipient($request)
    {
        // return DB::table('schedule')
        //     ->select('schedule.*', 'Client.Firstname', 'Client.Lastname', 'Client.Photo',  'Client.Mrn', 'Facility.facility_name', DB::raw('Client.ID AS patient_id'))
        //     ->join('Client', 'Client.ID', '=', 'schedule.ClientID')
        //     ->join('Facility', 'Facility.id', '=', 'schedule.facility_id')
        //     ->whereDate('schedule.Start', '<=', date('Y-m-d'))
        //     ->get

        $search             = $request['search'];
        $columns            = $request['columns'];
        $start              = $request['start'];
        $length             = $request['length'];
        $order              = $request['order'];
        $draw               = $request['draw'];
        $order_by_column    = $order[0]['column'];
        $order_by_dir       = $order[0]['dir'];
        $find               = $search['value'] ?? '';

        $facility_id        = auth()->user()->facility_id;
        $date               = date("m/d/Y");

        $connection = $connection = app('sql_srv_connection')->getPdo();

        $statement = $connection->prepare(
            // "EXEC sp_PatientList_Vaccinator
            // ,	@date = :p_date
            // ,	@p_sort	= :p_sort
            "EXEC sp_vaccinator_recepient_list
                    @facility_id = :p_facility_id
                ,	@p_offset = :p_offset
                ,	@p_limit = :p_limit
                ,	@p_find	= :p_find"
        );

        $order_inx = 1; //default to fullname
        switch ($order_by_column) {
            case 0: //fullname
                $order_inx = 1;
                break;
            case 1: //Mrn
                $order_inx = 2;
                break;
            case 2: //schedule
                $order_inx = 3;
                break;
            default: //fullname
                $order_inx = 1;
        }

        $sort_value = $order_inx . ' ' . $order_by_dir;
        // $statement->bindParam(':p_sort', $sort_value);
        // $statement->bindParam(':p_date', $date);
        $statement->bindParam(':p_facility_id', $facility_id);
        $statement->bindParam(':p_offset', $start);
        $statement->bindParam(':p_limit', $length);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);

        $statement = $connection->prepare(
            "EXEC dbo.sp_vaccinator_recepient_list_count
                    @facility_id = :p_facility_id
                ,   @p_find = :p_find"
        );

        $statement->bindParam(':p_facility_id', $facility_id);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $count = $statement->fetchAll(PDO::FETCH_ASSOC);

        $recordsTotal = isset($count[0]['patient_count']) ? $count[0]['patient_count'] : 0;
        $recordsFiltered = $recordsTotal;

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function checkNpi($params)
    {
        $resp = false;
        try{
            if(isset($params['npi']) && trim($params['npi']) != ""){
                //use npi API to verify
                $client = new User();
                $res = $client->request('GET', env('NPI_REGISTRY_URL').'&number='.$params['npi']);
                $status_code = $res->getStatusCode();
                $response = json_decode($res->getBody());

                if($status_code == 200){
                    if(isset($response->Errors)){
                        return response()->json([
                                'status' => 'error',
                                'message' => 'NPI Error: '.$response->Errors[0]->description
                            ], 200
                        );
                    }else{
                        if(isset($response->result_count) && $response->result_count > 0){
                            $resp = $response;
                        }else{
                            return response()->json([
                                    'status' => 'error',
                                    'message' => 'There was an error verifying your NPI.'
                                ], 200
                            );
                        }
                    }
                }else{
                    return response()->json([
                            'status' => 'error',
                            'message' => 'There was an error verifying your NPI.'
                        ], 200
                    );
                }
            }else{
                return response()->json([
                        'status' => 'error',
                        'message' => 'Please enter a valid NPI.'
                    ], 200
                );
            }
        } catch (\Exception $e) {
            // something went wrong elsewhere, handle gracefully
            return response()->json([
                    'status' => 'error',
                    'message' => 'There was an error verifying your NPI.'
                ], 200
            );
        }

        return response()->json([
                'data' => $resp,
                'status' => 'success',
            ], 200
        );
    }

    public function getInventoryLocationList()
    {
        $facility_id = auth()->user()->facility_id;
        $facility_name = Facility::find($facility_id)->facility_name;
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # get care team
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Inventory_Location
            @facility_id = :facility_id
            ");

            $statement->bindParam(':facility_id', $facility_id);

        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);

        # return response
        return response()->json([
            'success' =>true,
            'data'    =>$data,
            'facility_name' => $facility_name
        ],200);
    }

    public function getLotNumberList(): JsonResponse
    {
        $facility_id = auth()->user()->facility_id;
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # get care team
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_LotNumber
            @facility_id = :facility_id,
            @inventory_location = :inventory_location
        ");

        $statement->execute([
            ':facility_id' => $facility_id,
            ':inventory_location' => request('inventory_location')
        ]);
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);

        # return response
        return response()->json([
            'success' =>true,
            'data'    =>$data,
        ]);
    }

    public function getVaccineInformation($request): JsonResponse
    {
        $facility_id = auth()->user()->facility_id;
        $lot_no = $request['lot_no'];
        $inventory_location = $request['inventory_location'];
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # get care team
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Vaccine_Information_byLotNo
            @facility_id = :facility_id,
            @inventory_location = :inventory_location,
            @lotno = :lot_no
        ");

        $statement->execute([
            ':facility_id' => $facility_id,
            ':inventory_location' => $inventory_location,
            ':lot_no' => $lot_no
        ]);

        $data = $statement->fetch(PDO::FETCH_ASSOC);
        # return response
        return response()->json([
            'success' =>true,
            'data'    =>$data,
        ]);
    }
}
