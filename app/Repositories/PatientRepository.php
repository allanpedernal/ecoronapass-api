<?php

namespace App\Repositories;

use App\Models\Insurance;
use App\Models\ModelHasRoles;
use App\User;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewPatientEmail;
use App\Models\FlexForm;
use App\Models\PatientContraindicationAnswer;
use App\Models\PatientFlexForm;
use App\Models\PatientSurvey;
use App\Models\RoleModel;
use App\Models\VaccineContraindicationQuestion;

use App\Helpers\SmsHelper;

class PatientRepository
{
    public function patientList($request)
    {
        $search             = $request['search'];
        $columns            = $request['columns'];
        $start              = $request['start'];
        $length             = $request['length'];
        $order              = $request['order'];
        $draw               = $request['draw'];
        $order_by_column    = $order[0]['column'];
        $order_by_dir       = $order[0]['dir'];
        $find               = $search['value'] ?? '';

        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare("
            EXEC sp_PatientList
                @p_sort = :p_sort
            ,   @p_offset = :p_offset
            ,   @p_limit = :p_limit
            ,   @p_find = :p_find
        ");

        $order_inx = 4; //default to fullname
        switch ($order_by_column) {
            case 1: //Mrn
                $order_inx = 21;
                break;
            case 2: //gender
                $order_inx = 7;
                break;
            case 3: //Dob
                $order_inx = 9;
                break;
            case 4: //Email`
                $order_inx = 16;
                break;
            default: //fullname
                $order_inx = 4;
        }

        $sort_value = $order_inx . ' ' . $order_by_dir;
        $statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $start);
        $statement->bindParam(':p_limit', $length);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $recordsTotal = $data ? $data[0]['cnt'] : 0;
        $recordsFiltered = $recordsTotal;

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function store($data)
    {
        $patient_role = RoleModel::where('name', 'patient')->first();
        $email = trim($data['email']);
        $username = trim($data['username']);

        try {

            //verify email and username if already exists
            //$check_user = User::where('email', 'like', '%' . $email . '%')->first();
            // if ($check_user) {
            //     return response()->json([
            //         'status' => 'error',
            //         'message' => 'There is already an existing email in our database. Please enter a new email'
            //     ], 200);
            // }

            $check_user = User::where('username', 'like', '%' . $username . '%')->first();

            if ($check_user) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'There is already an existing Username in our database. Please enter a new Username'
                ], 200);
            }

            //save to client table
            $generated_password = Str::random(8);

            DB::beginTransaction();
            $userData = [
                'Lastname' => $data['lastname'],
                'Firstname' => $data['firstname'],
                'Middlename' => $data['middlename'],
                'username' => $username,
                'email' => $email,
                'Ssn' => $data['ssn'],
                'Dob' => $data['dob'],
                'Sex' => $data['gender'],
                'Photo' => 'img/default.png',
                'password' => bcrypt($generated_password),
                'Address1' => $data['address'],
                // 'Address2' => $data['address2'],
                'City' => $data['city'],
                'State' => $data['state'],
                'Zip' => $data['zip'],
                'email_verified_at' => date('Y-m-d'),
                'Race' => $data['race'],
                'Ethnicity' => isset($data['ethnicity']) ? $data['ethnicity'] : '',
                'Photo' => 'img/default.png',
                'email_verified_at' => now(),
                'County' => isset($data['county']) ? $data['county'] : '',
                'Ssn' => $data['ssn'],
                'phone' => preg_replace( '/[^0-9]/', '', $data['phone'] ),
                'notes' => $data['notes'],
                'suffix' => $data['suffix'],
                'patient_type' => isset($data['patient_type']) ? $data['patient_type'] : '',
                'employer_name' => isset($data['employer_name']) ? $data['employer_name'] : '',
                'employee_id' => isset($data['employee_id']) ? $data['employee_id'] : '',
            ];
            $newuser = User::create($userData);

            //send email to patient added and save contacts
            if ($newuser) {
                //assign role
                $patient_role = RoleModel::where('name', 'patient')->first(); //e2d42960-9f35-11ea-a0e0-dfc29dc0f77d
                ModelHasRoles::create(['role_id' => $patient_role->id, 'model_type' => 'App\User', 'model_uuid' => $newuser->ID]);
                //save insurance
                if (isset($data['insurance_number']) && $data['insurance_number']) {
                    Insurance::create([
                        'ClientID' => $newuser->ID,
                        'InsuranceNumber' => $data['insurance_number'],
                        'InsuranceType' => $data['insurance_type'],
                        'InsuranceProvider' => $data['insurance_provider'],
                        'Priority' => 1,
                    ]);
                }


                if (isset($data['secondary_insurance_number']) && $data['secondary_insurance_number']) {
                    Insurance::create([
                        'ClientID' => $newuser->ID,
                        'InsuranceNumber' => $data['secondary_insurance_number'],
                        'InsuranceType' => $data['secondary_insurance_type'],
                        'InsuranceProvider' => $data['secondary_insurance_provider'],
                        'Priority' => 2,
                    ]);
                }

                if (isset($data['tertiary_insurance_number']) && $data['tertiary_insurance_number']) {
                    Insurance::create([
                        'ClientID' => $newuser->ID,
                        'InsuranceNumber' => $data['tertiary_insurance_number'],
                        'InsuranceType' => $data['tertiary_insurance_type'],
                        'InsuranceProvider' => $data['tertiary_insurance_provider'],
                        'Priority' => 3,
                    ]);
                }
            }

            DB::commit();

            if ($newuser) {
                //send eamil
                if ($data['email']) {
                    $bcc = env('MAIL_BCC');
                    $bcc = $bcc ? explode(',',$bcc) : [];
                    if ($bcc) {
                        Mail::to($email)
                        ->bcc($bcc)
                        ->send(new NewPatientEmail([
                            'firstname' => $newuser->Firstname,
                            'username' => $newuser->username,
                            'password' => $generated_password
                        ]));
                    } else {
                        Mail::to($email)
                        ->send(new NewPatientEmail([
                            'firstname' => $newuser->Firstname,
                            'username' => $newuser->username,
                            'password' => $generated_password
                        ]));
                    }
                }

                $smsMessage = "Dear " . $newuser->Firstname . " \n";
                $smsMessage .= "We have successfully registered your account in " . config('app.name') . " Platform. \n";
                $smsMessage .= "You can login using the credentials below: \n";
                $smsMessage .= "Username: " . $newuser->username . " \n";
                $smsMessage .= "Password: " . $generated_password . " \n";
                $smsMessage .= "Click this Link https://eastalcovidvaccine.com to login to our web app. To change your password, click your account menu and select change password. \n";
                $smsMessage .= "Thanks, \n";
                $smsMessage .= config('app.name') . " Support Team.";

                SmsHelper::send($newuser->phone, $smsMessage);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully registered new Patient. Login credentials has been sent to his/her email.',
                'newuser' => $newuser
            ], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            $message = 'There was an error processing your request.';
            // $message = $e->getMessage();
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 200);
        }
    }

    public function update($data)
    {
        $email = trim($data['email']);
        $username = trim($data['username']);
        $user = User::where('ID', $data['ID'])->first();

        # verify email and username if already exists
        /*$check_user = User::where('email', '!=', $user->email)->where('email', 'like', '%' . $email . '%')->first();
        if ($email && $check_user && $check_user->ID != $data['ID']) {
            return response()->json([
                'status' => 'error',
                'message' => 'There is already an existing email in our database. Please enter a new email'
            ], 200);
        }*/

        $check_user = User::where('username', $username)->first();
        if ($username && $check_user && $check_user->ID != $data['ID']) {
            return response()->json([
                'status' => 'error',
                'message' => 'Cannot update username, username already exists.'
            ], 200);
        }

        try {
            DB::beginTransaction();

            $user = User::where('ID', $data['ID']);
            $oldData = $user->first()->toArray();
            $userData = [
                'Lastname' => $data['lastname'] ?? '',
                'Firstname' => $data['firstname'] ?? '',
                'Middlename' => $data['middlename'] ?? '',
                'username' => $username ?? '',
                'email' => $email ?? '',
                'Dob' => $data['dob'] ?? '',
                'Sex' => $data['gender'] ?? '',
                'Address1' => $data['address'] ?? '',
                // 'Address2' => $data['address2'],
                'City' => $data['city'] ?? '',
                'State' => $data['state'] ?? '',
                'Zip' => $data['zip'] ?? '',
                'Race' => $data['race'] ?? '',
                'Ethnicity' => isset($data['ethnicity']) ? $data['ethnicity'] : '',
                'County' => isset($data['county']) ? $data['county'] : '',
                'username' => $data['username'] ?? '',
                'Ssn' => $data['ssn'] ?? '',
                'phone' => $data['phone'] ?? '',
                'suffix' => $data['suffix'] ?? '',
                'employer_name' => isset($data['employer_name']) ? $data['employer_name'] : '',
                'employee_id' => isset($data['employee_id']) ? $data['employee_id'] : '',
                'notes' => $data['notes'] ?? null
            ];
            $user->update($userData);

            if(isset($data['patient_type'])) {
                $user->update(['patient_type' => $data['patient_type']]);
            }

            //save contacts
            // $connection = $connection = app('sql_srv_connection')->getPdo();
            // $statement = $connection->prepare(
            //     "exec sp_AddUpdateClientContact
            //             @ClientID	= :p_clientId
            //         ,@Phone1	= :p_work
            //         ,@Phone2	= :p_mobile
            //         ,@Phone3	= :p_home
            //     "
            // );

            // $work_phone = 'Work,' . $data['work_phone'] . ',';
            // $cell_phone = 'Mobile,' . $data['cell_phone'] . ',';
            // $home_phone = 'Home,' . $data['home_phone'] . ',';
            // $clientid =  $data['ID'];

            // $statement->bindParam(':p_clientId', $clientid);
            // $statement->bindParam(':p_work', $work_phone);
            // $statement->bindParam(':p_mobile', $cell_phone);
            // $statement->bindParam(':p_home', $home_phone);
            // $statement->execute();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Patient data successfully updated.'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = 'There was an error processing your request.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 200);
        }
    }

    public function details($patient_id)
    {

        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare(
          "EXEC sp_GetClientinfo @ClientID = :p_id"
        );

        $statement->bindParam(':p_id', $patient_id);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $data = $data[0];
        $user_data = [
            'member_id' => $data['MemberID'],
            'lastname' => $data['Lastname'],
            'firstname' => $data['Firstname'],
            'middlename' => $data['Middlename'],
            'gender' => $data['Sex'],
            'age' => $data['age'],
            'dob' => date('Y-m-d', strtotime($data['birthdate'])),
            'ssn' => $data['Ssn'],
            // 'home_phone' => $data['Home'],
            // 'work_phone' => $data['Work'],
            // 'cell_phone' => $data['Mobile'],
            'address' => $data['Address1'],
            'address2' => $data['Address2'],
            'city' => $data['City'],
            'state' => $data['State'],
            'zip' => $data['Zip'],
            'picture' => '/'.$data['Photo'],
            'npi' => isset($data['Npi']) ? $data['Npi'] : '',
            'organization' => isset($data['Organization']) ? $data['Organization'] : '',
            'mrn' => isset($data['Mrn']) ? $data['Mrn'] : '',
            'longitude' => isset($data['Longitude']) ? $data['Longitude'] : '',
            'latitude' => isset($data['Latitude']) ? $data['Latitude'] : '',
            // 'facility_name' => @isset($data['facility_name']) ? $data['facility_name'] : '',
            'race' => isset($data['race']) ? $data['race'] : '',
            'ethnicity' => isset($data['ethnicity']) ? $data['ethnicity'] : '',
            'email' => $data['email'],
            'username' => $data['username'] ?? null,
            'county' => $data['County'],
            'ethnicity' => $data['Ethnicity'],
            'phone' => $data['Phone'],
            'notes' => $data['notes'],
            'suffix' => $data['suffix'],
            'employee_id' => $data['employee_id'],
            'employer_name' => $data['employer_name'],
            'eamc_employee' => $data['eamc_employee'],
            'patient_type' => $data['patient_type'],
        ];

        $showAdminister = true;

        if(!auth()->user()->hasRole('provider')) {
            $showAdminister = $this->validateScheduleByPatient($patient_id);
        }

        //get insurance
        $user_data['insurance'] = Insurance::where('ClientID', $patient_id)->get()->toArray();
        $survey = $this->surveyDetails($patient_id);
        return response()->json([
                'patient'   => $user_data,
                'survey'    => $survey,
                'showAdminister' => $showAdminister
            ], 200
        );
    }

    public function validateScheduleByPatient($patient_id)
    {
        $connection = $connection = app('sql_srv_connection')->getPdo();

        $statement = $connection->prepare(
            "EXEC sp_ValidateSchedule_byPatient	@patient_id = :patient_id"
        );

        $statement->bindParam(':patient_id', $patient_id);
        $statement->execute();

        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return count($data) > 0 ? true : false;
    }

    function ccMasking($number, $maskingCharacter = 'X') {
        return str_repeat($maskingCharacter, strlen($number) - 4) . substr($number, -4);
    }

    public function getPreRegistrationQuestion($request)
    {
        $patient_id = $request['patient_id'] ?? auth()->user()->ID;
        $consent_given = auth()->user()->consent_given;

        if ($request['answer'] && $request['question_id'] && $request['answer'] != 'Back') {
            //save
            PatientContraindicationAnswer::updateOrCreate(
                ['patient_id' => $patient_id, 'vaccine_contraindication_question_id' => $request['question_id']],
                [
                    'patient_id' => $patient_id,
                    'vaccine_contraindication_id' => $request['id'],
                    'vaccine_contraindication_question_id' => $request['question_id'],
                    'answer' => $request['answer']
                ]
            );

            //update survey
            if ($request['question_number'] >= 15) {
                $this->updateSurvey(['status' => 'approved'], 'pre_registration', $patient_id);
            }


        }

        $contraindication = VaccineContraindicationQuestion::where('vaccine_contraindication_id', $request['id'])->where('question_number', $request['question_number'])->first();
        $patient_answer = PatientContraindicationAnswer::where('patient_id', $patient_id)->where('vaccine_contraindication_question_id', $contraindication->id)->first();
        return  [
            'consent_given' => $consent_given,
            'question_id'   => $contraindication->id,
            'question' => $contraindication->question,
            'options' => json_decode($contraindication->options, true),
            'answer'  => $patient_answer->answer ?? null,
        ];

    }

    public function getFlexFormQuestion($flexForm, $patient_id)
    {
        $flexForm = ucwords(str_replace('_', ' ', $flexForm));
        $flex_form = DB::table('flex_form')->where('name', $flexForm)->first();
        $flex_form_questions = DB::table('flex_form_question')->where('flex_form_id', $flex_form->id)->orderBy('order', 'asc')->get();
        $flex_form_options = DB::table('flex_form_option')->where('flex_form_id', $flex_form->id)->get();
        $patient_answer = DB::table('patient_flex_form')->where('client_id', $patient_id)->where('flex_form_id', $flex_form->id)->get();
        $checklist = [];
        foreach ($flex_form_questions as  $question) {

            $checklist[] = [
                'id'        => $question->id,
                'order'     => $question->order,
                'question'  => $question->question,
                'type'      => $question->type,
                'option'    => $flex_form_options->where('flex_form_question_id', $question->id)->toArray(),
                'answer'    => $patient_answer->where('flex_form_question_id', $question->id)->first()
            ];
        }
        return response()->json([
            'flex_form_id' =>  $flex_form->id,
            'data' => $checklist
        ], 200);
    }

    public function saveFlexFormQuestion($request, $failicty_id = null)
    {

        $redirect_url = '';

        try {
            $client_id = $request['patient_id'];
            //delete old answer
            PatientFlexForm::where('client_id', $client_id)->where('flex_form_id', $request['flex_form_id'])->delete();
            //create new
            $patient_answer = [];
            foreach ($request['answer'] as $question_id => $ans) {
                $patient_answer[] = [
                    'client_id' => $client_id,
                    'flex_form_id' => $request['flex_form_id'],
                    'flex_form_question_id' => $question_id,
                    'answer' => json_encode($ans)
                ];
            }

            DB::table('patient_flex_form')->insert($patient_answer);
            DB::commit();

            //check other survey
            if (auth()->user()->hasRole('patient')) {
                $flex_form = FlexForm::find($request['flex_form_id']);
                $transform_name = strtolower(str_replace(' ', '_', $flex_form->name));
                PatientSurvey::updateOrCreate(
                    ['survey' => $transform_name, 'patient_id' => $client_id],
                    ['status' => 'pending']
                );
                $survey = app(SchedulerRepository::class)->surveyChecker(auth()->user()->ID);
                if ($survey) {
                    $redirect_url = route('patient.survey', $survey) . '?facilityId=' . $failicty_id;
                } else {
                    $redirect_url = route('scheduler.facility', $failicty_id);
                }
            } else {
                //for provider
                $redirect_url = route('patient.profile', $client_id);
            }


            $status = 'success';
            $message = 'Screening Checklist saved successful.';

        } catch (\Exception $e) {
            DB::rollBack();
            $status = 'error';
            $message = $e->getMessage();
        }

        return response()->json([
            'redirect_url' => $redirect_url,
            'status' =>  $status,
            'message' => $message
        ], 200);
    }

    public function surveyDetails($patient_id)
    {
        $patient_survey = PatientSurvey::where('patient_id', $patient_id)
        ->pluck('status', 'survey')->toArray();
        return compact('patient_survey');

    }

    public function updateSurvey($data, $survey, $patient_id)
    {
        PatientSurvey::updateOrCreate(
            ['survey' => $survey, 'patient_id' => $patient_id],
            $data
        );
    }

    public function getclientInfo($patient_id)
    {
        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare(
          "EXEC sp_GetClientinfo @ClientID = :p_id"
        );

        $statement->bindParam(':p_id', $patient_id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }
}
