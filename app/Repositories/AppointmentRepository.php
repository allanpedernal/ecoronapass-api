<?php

namespace App\Repositories;

use App\Base\BaseRepository;
use App\Models\Facility;
use App\Models\Schedule;
use DB;

class AppointmentRepository extends BaseRepository
{
    /**
     * @inheritDoc
     */

    protected function model()
    {
        return new Schedule();
    }

    public function getEvents($request)
    {
        $start_date = date('m/d/Y',strtotime($request['start']));
        $end_date = date('m/d/Y',strtotime($request['end']));

        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # events
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Calendar_Appointment
            @start_date = :start_date,
            @end_date = :end_date
        ");
        $statement->bindParam(':start_date', $start_date);
        $statement->bindParam(':end_date', $end_date);
        $statement->execute();
        $events = $statement->fetchAll(\PDO::FETCH_ASSOC);

        # total
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Calendar_Appointment_Totals_Per_Day
            @start_date = :start_date,
            @end_date = :end_date
        ");
        $statement->bindParam(':start_date', $start_date);
        $statement->bindParam(':end_date', $end_date);
        $statement->execute();
        $total_per_day = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return compact('events', 'total_per_day');
    }

    public function save($request)
    {
        try {
            //format data
            $event = [
                'ClientID'      => $request['ClientID']['ID'],
                'Title'         => $request['ClientID']['name'],
                'Start'         => $request['StartDate'] . ' ' . $request['StartTime'],
                'End'           => $request['EndDate'] . ' ' . $request['EndTime'],
                'Description'   => $request['Description'],
                'type'          => $request['type'],
                'facility_id'   => $request['facility_id']
            ];

            $event['Color'] = $request['type'] == 'test' ? "#6f9a3f" : '#0665d0';

            if ($request['ID']) {
                $this->query()->where('ID', $request['ID'])->update($event);
                $message = 'Schedule updated successful.';
            } else {
                $this->query()->create($event);
                $message = 'Schedule saved successful.';
            }

            $status = 'success';
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        return compact('status', 'message');
    }

    public function summary($request)
    {
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # loinc
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Schedule_Summary
        ");
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function getEventPatients($date)
    {
        $date = date('m/d/Y H:i:s',strtotime($date));
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # loinc
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Calendar_Appointment_Detail
            @date = :date
        ");
        $statement->bindParam(':date', $date);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getFacilities()
    {
        return Facility::all()->pluck('facility_name', 'id');
    }

    public function liveStatsToday($request)
    {
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # get live stats
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_live_stats_today");
        $statement->execute();
        return collect($statement->fetchAll(\PDO::FETCH_ASSOC));

    }

    public function noShow()
    {
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();
        # get live stats
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_live_stats_today_no_show");
        $statement->execute();
        return collect($statement->fetchAll(\PDO::FETCH_ASSOC));
    }
}
