<?php

namespace App\Repositories;

use App\User;
use DB;
use Illuminate\Support\Str;

class CheckinRepository
{
    public function patientList($request)
    {
        $search             = $request['search'];
        $columns            = $request['columns'];
        $start              = $request['start'];
        $length             = $request['length'];
        $order              = $request['order'];
        $draw               = $request['draw'];
        $order_by_column    = $order[0]['column'];
        $order_by_dir       = $order[0]['dir'];
        $find               = $search['value'] ?? '';

        $facility_id        = auth()->user()->facility_id;
        $date               = date("m/d/Y");

        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare(
            "EXEC sp_checkin_patient_list
                    @facility_id = :p_facility_id
                ,	@p_offset = :p_offset
                ,	@p_limit = :p_limit
                ,	@p_find	= :p_find"
        );

        $order_inx = 1; //default to fullname
        switch ($order_by_column) {
            case 0: //fullname
                $order_inx = 2;
                break;
            case 1: //Mrn
                $order_inx = 3;
                break;
            case 2: //schedule
                $order_inx = 4;
                break;
            default: //fullname
                $order_inx = 2;
        }

        $sort_value = $order_inx . ' ' . $order_by_dir;
        //$statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $start);
        $statement->bindParam(':p_limit', $length);
        $statement->bindParam(':p_find', $find);
        $statement->bindParam(':p_facility_id', $facility_id);
        // $statement->bindParam(':p_date', $date);

        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $recordsTotal = $data ? $data[0]['cnt'] : 0;
        $recordsFiltered = $recordsTotal;

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function updateStatus($request)
    {
        $schedule_id    = $request['schedule_id'];
        $patient_id     = $request['patient_id'];
        $status         = $request['status'];
        $status_type    = 'check-in-status';
        $reason         = '';

        $connection = $connection = app('sql_srv_connection')->getPdo();

        if(!$schedule_id){
            //get schedule id
            $statement = $connection->prepare(
                "EXEC sp_ValidateSchedule_byPatient	@patient_id = :p_patient_id"
            );

            $statement->bindParam(':p_patient_id', $patient_id);
            $statement->execute();

            $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if($data){
                $schedule_id = $data[0]['schedule_id'];
            }else{
                return response()->json([
                        'status' => false,
                        'message' => 'Please Schedule a Vaccination!',
                    ]);
            }
        }

        $updated_by = auth()->user()->ID;
        $statement = $connection->prepare(
            "EXEC sp_UpdateVaccine_ScheduleStatus
                    @ScheduleID = :p_schedule_id
                ,	@Status = :p_status
                ,	@StatusType = :p_statustype
                ,	@Reason = :p_reason
                ,	@updated_by = :updated_by"
        );

        $statement->bindParam(':p_schedule_id', $schedule_id);
        $statement->bindParam(':p_status', $status);
        $statement->bindParam(':p_statustype', $status_type);
        $statement->bindParam(':p_reason', $reason);
        $statement->bindParam(':updated_by', $updated_by);

        $statement->execute();

        return response()->json([
            'status' => true
        ]);
    }
}
