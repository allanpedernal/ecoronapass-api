<?php

namespace App\Console;


use App\Console\Commands\JWTSecretKeyGenerator;
use App\Console\Commands\SPTesterCommand;
use App\Console\Commands\AppointmentNotificationCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Laravelista\LumenVendorPublish\VendorPublishCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        JWTSecretKeyGenerator::class,
        SPTesterCommand::class,
        VendorPublishCommand::class,
        AppointmentNotificationCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cron:appointmentnotif')->dailyAt('00:01'); # Send Notification daily for patient scheduled appointment
    }
}
