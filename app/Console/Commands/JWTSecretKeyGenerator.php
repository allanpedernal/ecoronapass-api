<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class JWTSecretKeyGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jwt-secret:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate secret key to be used by jwt api service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $key = self::generateKey();
        if ( Str::contains( file_get_contents( self::envPath() ), 'JWT_SECRET') === false ) {
            #append content in .env file
            if ( file_put_contents( self::envPath(), PHP_EOL . "JWT_SECRET=" . $key, FILE_APPEND ) > 0 ) {
                $this->info("JWT secret key [$key] set successfully.");
            }
        }
    }

    /**
     * @return string
     */
    protected static function envPath(): string
    {
        return base_path('.env');
    }

    /**
     * @return string
     */
    protected static function generateKey(): string
    {
        return Str::random('32');
    }
}
