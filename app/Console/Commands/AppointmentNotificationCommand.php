<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Mail\AppointmentNotificationMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;

use Kreait\Firebase\Database;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\FieldValue;

class AppointmentNotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:appointmentnotif';
    protected $notif_url = 'https://fcm.googleapis.com/fcm/send';
    protected $notif_payload = [
                                    "title" => "Appointment Schedule",
                                    "body" => "You have a scheduled Covid Test today.",
                                    "event" => "appointment_schedule",
                                ];

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification to patients for scheduled appointments';


    protected $pdo_connection;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->pdo_connection = app('pdo_connection')->getPdo();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->format('m/d/Y');
        $firestore = app('firebase.firestore');
        $database = $firestore->database();
        
        $statement = $this->pdo_connection->prepare(
            "EXEC sp_patient_covidtest_notification @date = :date"
        );
        $statement->bindParam(':date', $now);
        $statement->execute();
        $raw = $statement->fetchAll(\PDO::FETCH_ASSOC);

        
        if(count($raw)){
            foreach($raw as $k => $v){
                $patientid = $v['client_id'];
                $device_id = isset($v['device_id']) && !empty($v['device_id']) ? $v['device_id'] : '';

                //send push notif
                $notif_param = [
                    'headers' => [
                        'Authorization' => env('FIREBASE_AUTH'),
                        'project_id' => env('FIREBASE_PROJECTID'),
                        'Content-Type' => 'application/json; charset=utf-8'
                    ],
                    'json' => [
                        'device_id' => $device_id,
                        'notification' => $this->notif_payload,
                        'to' => '/topics/ecoronapass_'.$patientid,
                    ]
                ];
        
                $client = new Client;            
                $notif_response = $client->request('POST', $this->notif_url, $notif_param);
                $notif_response = json_decode($notif_response->getBody(), true);



                if(isset($notif_response['message_id'])){
                    //save to firestore
                    $database->collection('patients')->document($patientid)->collection('notifications')->add([
                        "title" => 'Appointment Schedule',
                        "body" => 'You have a scheduled Covid Test on '.$v['schedule'].' at '.$v['facility_name'],
                        "event" => 'appointment_schedule',
                        "created_at" => FieldValue::serverTimestamp()
                    ]);
                }
                //end send push notif
                
                //send sms
                $contact_number = false;
                if($v['mobile_no'] != null && trim($v['mobile_no']) != ''){
                    $actions = 'sms';
                    $contact_number = str_replace('(', '', $v['mobile_no']);
                    $contact_number = str_replace(')', '', $contact_number);
                    $contact_number = str_replace('-', '', $contact_number);
                    $contact_number = str_replace(' ', '', $contact_number);

                    //send
                    $textmessage    = "Hi ".$v['firstname']." ".$v['lastname']."," . PHP_EOL;
                    $textmessage    .= 'You have a scheduled Covid Test on '.$v['schedule'].' at '.$v['facility_name']."." . PHP_EOL;
                    $textmessage    .= "- eCoronapass";

                    $ret_sms = app('utility')->sendAppointmentSmsNotification($contact_number, $textmessage);
                }

                //send email
                $email = (($v['email'] != null && trim($v['email']) != '') ? trim($v['email']) : false);
                if($email){
                    $actions = 'email';
                    $email_data = [
                        'lastname' => $v['lastname'],
                        'firstname' => $v['firstname'],
                        'schedule' => $v['schedule'],
                        'facility_name' => $v['facility_name']
                    ];

                    Mail::to($email)->send(new AppointmentNotificationMail($email_data));
                }

                $this->info(PHP_EOL . 'Reminder sent to patient '.$patientid." - ".$now);
            }
        }
        

        
    }
}
