<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;

class SPTesterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sp:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stored Procedure Testing.';


    protected $pdo_connection;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->pdo_connection = app('pdo_connection')->getPdo();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        /*$sql = "SELECT c.ID
                    , c.Lastname
                    , c.Firstname
                    , c.username
                    , c.password
                    , r.name
                    , c.*
                FROM dbo.Client c
                JOIN dbo.Model_Has_Roles m
                    ON m.model_uuid = c.ID
                JOIN dbo.Roles r
                    ON r.id = m.role_id
                ORDER BY c.created_at DESC
                OFFSET 0 ROW
                FETCH NEXT 20 ROW ONLY";*/

        $sql = "SELECT c.*
                FROM dbo.Client c
                WHERE c.email = :entity";
        $statement = $this->pdo_connection->prepare($sql);
        #$statement->execute();
        $statement->execute([':entity' => 'smb@dummy.com']);
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        dd($result);
    }
}
