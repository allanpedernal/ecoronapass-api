<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UuidForKey;

class VaccineInformation extends Model
{
    use SoftDeletes, UuidForKey;

    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Vaccine_Information';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $primaryKey = 'id';

    protected $with = ['facility'];

    public $fillable = [
        'facility_id',
        'vaccine_date',
        'cvx',
        'dose',
        'extract_Type',
        'lot_no',
        'mvx',
        'ndc',
        'vaccine_admin_site',
        'vaccine_exp_date',
        'vaccine_route_admin',
        'vaccine_series_complete',
        'cvx_code',
        'mvx_code',
        'brand',
        'dosage',
        'inventory_location',
        'active',
        'inventory_location_id',
        'msh_3',
        'msh_4',
        'msh_8',
        'hl7_targetnamespace',
        'hl7_endpoint',
        'order_provider'
    ];

    public function facility()
    {
        return $this->belongsTo('App\Models\Facility', 'facility_id');
    }

}
