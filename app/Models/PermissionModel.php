<?php

namespace App\Models;

use Spatie\Permission\Models\Permission;

class PermissionModel extends Permission
{
    //
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Permissions';

    protected $keyType = 'string';

    public $incrementing = false;
}
