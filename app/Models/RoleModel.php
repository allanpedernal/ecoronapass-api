<?php

namespace App\Models;
use Spatie\Permission\Models\Role;

class RoleModel extends Role
{
    //
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Roles';

    protected $keyType = 'string';

    public $incrementing = false;

}
