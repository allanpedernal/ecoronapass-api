<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientContraindicationAnswer extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.patient_contraindication_answer';

    protected $keyType = 'string';

    public $incrementing = false;

    public $fillable = [
        'patient_id',
        'vaccine_contraindication_id',
        'vaccine_contraindication_question_id',
        'answer',
    ];
}
