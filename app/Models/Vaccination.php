<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vaccination extends Model
{
    use SoftDeletes;

    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Vaccinations';

    protected $keyType = 'string';

    public $incrementing = false;

    public $fillable = [
        'patientid',
        'facility_id',
        'vaccine_date',
        'inventory_location',
        'lot_no',
        'mvx',
        'ndc',
        'vaccine_exp_date',
        'dosage',
        'vaccine_name',
        'vaccine_result',
        'vaccine_information_id',
        'vaccine_admin_site',
        'vaccine_route_admin',
        'vaccine_series_complete',
        'vaccinator',
        'vaccinator_id',
        'entering_organization',
        'sent_to_adph_date',
        'seq_num',
        'mvx_code',
        'cvx',
        'cvx_code',
        'schedule_id'
    ];

}
