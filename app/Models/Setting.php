<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.settings';

    protected $keyType = 'string';

    public $incrementing = false;
}
