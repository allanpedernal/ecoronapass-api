<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use App\Traits\UuidForKey;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, UuidForKey, Authenticatable, Authorizable;

    protected $primaryKey = 'ID';
    protected $table = 'dbo.Client';
    protected $keyType = 'string';
    protected $with = ['phone'];

    public $fillable = [
        'Firstname',
        'Lastname',
        'Middlename',
        'Photo',
        'username',
        'email',
        'Ssn',
        'Dob',
        'Sex',
        'memberid',
        'Address1',
        'Address2',
        'City',
        'State',
        'Zip',
        'remember_token',
        'created_by',
        'email_verified_at',
        'password',
        'Npi',
        'Organization',
        'Longitude',
        'Latitude',
        'facility_name',
        'Race',
        'Ethnicity',
        'phone',
        'notes',
        'employer_name',
        'employee_id',
        'patient_type',
        'eamc_employee',
        'suffix',
        'County',
        'consent_given',
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'consent_given' => 'boolean',
    ];

    public function getContact() {
        $result = DB::table('dbo.ClientContacts')->where('ClientID', $this->ID)->first();
        return isset($result->Number) ? $result->Number : null;
    }

    public function phone()
    {
        return $this->hasOne('App\Models\Phone', 'ClientID', 'ID');
    }

}
