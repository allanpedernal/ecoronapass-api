<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VaccineRouteAdmin extends Model
{
    use SoftDeletes;

    protected $connection = 'sqlsrv';

    protected $table = 'dbo.VaccineRouteAdmin';

    protected $keyType = 'string';

    public $incrementing = false;
}
