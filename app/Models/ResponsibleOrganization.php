<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsibleOrganization extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'dbo.ResponsibleOrganization';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'id';
}
