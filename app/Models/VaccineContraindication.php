<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VaccineContraindication extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.vaccine_contraindications';

    protected $keyType = 'string';

    public $incrementing = false;
}
