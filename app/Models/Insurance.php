<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Insurance';

    protected $keyType = 'string';
    public $timestamps = false;

    public $incrementing = false;

    protected $dateFormat = 'Y-m-d H:i:s';

    public $fillable = [
        'ClientID',
        'InsuranceNumber',
        'InsuranceType',
        'InsuranceProvider',
        'Priority',
    ];
}
