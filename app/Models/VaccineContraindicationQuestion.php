<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VaccineContraindicationQuestion extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.vaccine_contraindication_question';

    protected $keyType = 'string';

    public $incrementing = false;
}
