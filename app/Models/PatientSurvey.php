<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientSurvey extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.patient_surveys';

    protected $keyType = 'string';

    public $incrementing = false;

    public $fillable = [
        'patient_id',
        'survey',
        'status',
        'type'
    ];
}
