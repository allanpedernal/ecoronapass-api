<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelHasRoles extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Model_Has_Roles';

    protected $keyType = 'string';
    public $timestamps = false;

    public $incrementing = false;

    public $fillable = [
        'role_id',
        'model_type',
        'model_uuid'
    ];
}
