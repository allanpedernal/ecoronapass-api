<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UuidForKey;

class ClientContact extends Model
{
    use UuidForKey;

    protected $connection = 'sqlsrv';
    protected $table = 'dbo.ClientContacts';
    protected $keyType = 'string';
    protected $primaryKey = 'ID';

    public $fillable = [
        'ClientID',
        'Type',
        'Number',
        'Extension'
    ];

}
