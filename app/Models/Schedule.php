<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidForKey;

class Schedule extends Model
{
    use UuidForKey;

    protected $connection = 'sqlsrv';
    protected $primaryKey = 'ID';
    protected $table = 'dbo.schedule';

    protected $keyType = 'string';
    public $timestamps = false;

    public $incrementing = false;

    protected $dateFormat = 'Y-m-d H:i:s';

    public $fillable = [
        'ClientID',
        'Title',
        'Description',
        'Start',
        'End',
        'AllDay',
        'Color',
        'TextColor',
        'type',
        'status',
        'facility_id'
    ];

    const APPOINTMENT_MAX_PATIENT = 20;
    // const APPOINTMENT_START_TIME = "08:00:00";
    // const APPOINTMENT_END_TIME = "18:15:00";

    public static function appointmentMaxPatient()
    {
        return self::APPOINTMENT_MAX_PATIENT;
    }

    public static function appointmentStartTime()
    {
        $param_settings = Setting::where("setting","schedule_start_time")->first();
        return $param_settings->val ?? '7:15';
    }

    public static function appointmentEndTime()
    {
        $param_settings = Setting::where("setting","schedule_end_time")->first();
        return $param_settings->val ?? '14:45';
    }
}
