<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientFlexForm extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.patient_flex_form';

    protected $keyType = 'string';

    public $incrementing = false;

    public $fillable = [
        'client_id',
        'flex_form_id',
        'flex_form_question_id',
        'answer',
        'value',
    ];
}
