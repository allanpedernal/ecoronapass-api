<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UuidForKey;

class HL7Request extends Model
{
    use SoftDeletes, UuidForKey;

    protected $connection = 'sqlsrv';
    protected $table = 'dbo.HL7Request';
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public $fillable = [
        'client_id',
        'status',
        'request',
        'response',
        'vaccinations_id'
    ];

}
