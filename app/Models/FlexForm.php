<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlexForm extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'dbo.flex_form';

    protected $keyType = 'string';

    public $incrementing = false;
}
