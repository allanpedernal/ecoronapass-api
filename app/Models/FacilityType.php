<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilityType extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'dbo.FacilityType';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'id';
}
