<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\UuidForKey;

class Facility extends Model
{
    use SoftDeletes, UuidForKey;

    protected $connection = 'sqlsrv';

    protected $table = 'dbo.Facility';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $primaryKey = 'id';

    public $fillable = [
        'facility_name',
        'street',
        'city',
        'state',
        'zip',
        'phone_number',
        'accession_number',
        'collection_date',
        'sending_facility_name',
        'sending_facility_clia',
        'facility_type_id',
        'facility_type',
        'responsible_organization_id',
        'responsible_organization',
        'full_address',
        'type'
    ];

}
