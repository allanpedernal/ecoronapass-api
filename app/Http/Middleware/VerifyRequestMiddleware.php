<?php

namespace App\Http\Middleware;

use App\Traits\JWTTrait;
use App\User;
use Closure;
use Exception;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use Illuminate\Http\Request;

class VerifyRequestMiddleware
{
    use JWTTrait;
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Pre-Middleware Action
        $response = $next($request);
        $authorize = false;

        $authorization = $request->header('authorization');

        if ($authorization) {
            $token = self::jwtHeaderExtractor($authorization);
            if ($token) {
                try {
                    $token = self::jwtDecode($token);
                    $token = (array) $token;
                    $user_data = $token['data']->user;
                    #dd($user_data);
                    #$user = User::where('ID', $user_data->{'ID'})->first();
                    #dd($user);
                    if ($user_data)
                        $authorize = !$authorize;

                } catch (ExpiredException | BeforeValidException | SignatureInvalidException | Exception $e) {
                    return response()->json([
                        'status' => 'error',
                        'code' => 500,
                        'message' => $e->getMessage()
                    ], 500);
                }
            } else {
                return response()->json([
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Bad request, unable to extract the authorization in header request'
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'code' => 400,
                'message' => 'Bad request, token missing in header request'
            ], 400);
        }

        // Post-Middleware Action
        if ($authorize) return $response;

    }
}
