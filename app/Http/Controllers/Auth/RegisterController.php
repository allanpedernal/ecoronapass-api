<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ModelHasRoles;
use App\Models\RoleModel;
use App\Providers\RouteServiceProvider;
use App\Rules\NpiValidationRules;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest', 'check-user-type']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, $this->userRules($data));
    }

    public function userRules($data)
    {
        $rules = [
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            //'email' => ['required', 'string', 'email', 'max:255', 'unique:Client'],
            'username' => ['required', 'string', 'max:255', 'unique:Client'],
            // 'g-recaptcha-response' => 'required|captcha',
        ];

        if ($data['selecttype'] == 'Provider') {
            $rules['npi'] = new NpiValidationRules($data);
        }
        return $rules;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $provider_role = RoleModel::where('name', 'provider')->first(); //af5078a0-9f35-11ea-9356-8befd8456348
        $patient_role = RoleModel::where('name', 'patient')->first(); //e2d42960-9f35-11ea-a0e0-dfc29dc0f77d
        $model_type = 'App\User';

        $userData = [
            'Lastname' => $data['lastname'],
            'Firstname' => $data['firstname'],
            'Middlename' => $data['middlename'],
            'username' => $data['username'],
            'email' => @$data['email'],
            'Dob' => $data['dob'],
            'Address1' => $data['home_address'],
            'City' => $data['city'],
            'State' => $data['state'],
            'Zip' => $data['zip'],
            'Ssn' => $data['ssn'],
            'Photo' => 'img/default.png',
            'Npi' => $data['npi'],
            'Organization' => $data['organization'],
            'password' => Hash::make($data['password']),
            'Ethnicity' => isset($data['ethnicity']) ? $data['ethnicity'] : "",
            'Race' => $data['race'],
            'Sex' => $data['gender'],
            'County' => $data['county'],
            'employee_id' => isset($data['employee_id']) ? $data['employee_id'] : "",
            'employer_name' => isset($data['employer_name']) ? $data['employer_name'] : "",
            'phone' => $data['phone'],
            'suffix' => $data['suffix'],
            'patient_type' => $data['user_type'],
            'eamc_employee' => $data['eamc_employee'] ?? 0
        ];
        $userobj = User::create($userData);

        //assign to role
        if($userobj->Npi != null && !empty($userobj->Npi) && strtolower($data['selecttype']) == 'test provider' ){
            $obj = ModelHasRoles::updateOrCreate(
                ['role_id' => $provider_role->id, 'model_type' => $model_type, 'model_uuid' => $userobj->ID],
                ['role_id' => $provider_role->id, 'model_type' => $model_type, 'model_uuid' => $userobj->ID]
            );
        }else{
            $obj = ModelHasRoles::updateOrCreate(
                ['role_id' => $patient_role->id, 'model_type' => $model_type, 'model_uuid' => $userobj->ID],
                ['role_id' => $patient_role->id, 'model_type' => $model_type, 'model_uuid' => $userobj->ID]
            );
        }

        return $userobj;
    }

    public function showClientRegisterRedirectPage(Request $request){
        $message = isset($request->message) ? $request->message : "";
        return view('auth.client-register-redirect', compact('message', $message));
    }

    public function register(Request $request)
    {
        #dd($request);
        $this->validator($request->all())->validate();

        #$allowed_county = ["Chambers", "Lee", "Macon"];
        #$eligible_patient_county = in_array($request->county,$allowed_county);
        if ($request->user_type === 'Person age between 65 and 74 years' || $request->user_type === 'Person age 75 years and older') {
            $eligible_patient_age = $request->age >= 65;
        } else {
            $eligible_patient_age = true;
        }

        #$not_eligible_message = "Sorry, you are not eligible. Only residents of Chambers, Lee and Macon countries can schedule a COVID-19 vaccination at East Alabama Medical Center.";
        #$not_eligible_notify_once_eligible_message = "Sorry, you are not eligible at this time. If you would like us to text you once you are eligible, click to continue to verify your phone number.";

        /*if( ($eligible_patient_county == false && $eligible_patient_age == false) || ($eligible_patient_county == false && $eligible_patient_age == true)){
            $message = $not_eligible_message;
            $alert = 1;
            return redirect()->route('home.client-register-redirect', compact('message', 'alert'));
        }*/

        /*if($eligible_patient_county == true && $eligible_patient_age == false){
            event(new Registered($user = $this->create($request->all())));
            $alert = 2;
            $message = $not_eligible_notify_once_eligible_message;
            return redirect()->route('home.client-register-redirect', compact('message', 'alert'));
        }*/

        #event(new Registered($user = $this->create($request->all())));

        $user = $this->create($request->all());

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        if($request->wantsJson()){
            return new Response('', 201);
        }else{
            if($eligible_patient_age == true){
                return redirect()->route('scheduler.index')->with('success', 'Succesfully saved.');
            }else{
                return redirect($this->redirectPath());
            }
        }
    }
}
