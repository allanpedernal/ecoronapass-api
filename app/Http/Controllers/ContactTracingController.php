<?php

namespace App\Http\Controllers;

use PDO;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Aws\Sns\SnsClient;
use Aws\Sns\Exception\SnsException;
use Exception;

class ContactTracingController extends Controller
{
    /**
     * @OA\Post(
     *     path="/v1/contact/tracing",
     *     tags={"Contact Tracing"},
     *     description="Store Contact Tracing",
     *     summary="",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"client_id","lastname","firstname","phone","email"},
     *           @OA\Property(property="client_id", type="uuid", example="DC10507D-BF3F-41E9-B599-F4F45D254C12"),
     *           @OA\Property(property="lastname", type="string", example="Penduko"),
     *           @OA\Property(property="firstname", type="string", example="Pedro"),
     *           @OA\Property(property="phone", type="string", example="035-555-55-55"),
     *           @OA\Property(property="email", type="string", example="p_penduko@mailinator.com")
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Contact tracing is successfully added!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function store(Request $request): JsonResponse
    {
        try
        {
            // get error
            $error = app('utility')->validateRequest(
                Validator::make($request->all(), [
                    'client_id' => 'required',
                    'lastname' => 'required',
                    'firstname' => 'required',
                    'phone' => 'required',
                    'email' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                
                $err_mess = '';
                foreach($error as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            // extract
            extract($request->all());

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

                // set connection
                $connection = \DB::connection('sqlsrv')->getPdo();

                // execute spm contact tracing
                $statement = $connection->prepare("
                    EXEC spm_contact_tracing
                        @flag=0,
                        @client_id=:client_id,
                        @lastname=:lastname,
                        @firstname=:firstname,
                        @phone=:phone,
                        @email=:email,
                        @id=null
                ");
                $statement->bindParam(':client_id',$client_id);
                $statement->bindParam(':lastname',$lastname);
                $statement->bindParam(':firstname',$firstname);
                $statement->bindParam(':phone',$phone);
                $statement->bindParam(':email',$email);
                $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // send sms
            $message = 'Success! Contact tracing is successfully created!';
            $this->sendSMS($message,$phone);

            return response()->json([
                'status'=>true,
                'message'=>'Contact tracing is successfully created!'
            ],200);
        }
        catch(Exception $e)
        {
            return response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Get(
     *     path="/v1/contact/tracing/{client_id}",
     *     tags={"Contact Tracing"},
     *     description="Get Contact Tracing",
     *     summary="",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="client_id",
     *         in="path",
     *         required=true,
     *         description="contact tracing client id",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="contact_tracing",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="uuid", example="4A156951-91EC-EB11-AAC4-023887F26D8E"),
     *                      @OA\Property(property="lastname", type="string", example="Penduko"),
     *                      @OA\Property(property="firstname", type="string", example="Pedro"),
     *                      @OA\Property(property="phone", type="string", example="035-555-55-55"),
     *                      @OA\Property(property="email", type="string", example="p_penduko@mailinator.com"),
     *                      @OA\Property(property="created_at", type="date", example="2021-07-24")
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function show(Request $request, $client_id): JsonResponse
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            // execute provider mobile
            $statement = $connection->prepare("
                EXEC sp_contact_tracing
                    @client_id=:client_id
            ");
            $statement->bindParam(':client_id',$client_id);
            $statement->execute();
            $statement->execute();
            $contact_tracing = $statement->fetchAll(\PDO::FETCH_ASSOC);

            return response()->json([
                'status'=>true,
                'data'=>$contact_tracing,
                'message' => 'success'
            ],200);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/v1/contact/tracing/{id}",
     *     tags={"Contact Tracing"},
     *     description="Update Contact Tracing",
     *     summary="",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="contact tracing id",
     *     ),
     *     @OA\RequestBody(
     *         description="Contact Tracing Update",
     *         required=true,
     *         @OA\JsonContent(
     *           required={"id", "client_id","lastname","firstname","phone","email"},
     *           @OA\Property(property="client_id", type="uuid", example="DC10507D-BF3F-41E9-B599-F4F45D254C12"),
     *           @OA\Property(property="lastname", type="string", example="Penduko"),
     *           @OA\Property(property="firstname", type="string", example="Pedro"),
     *           @OA\Property(property="phone", type="string", example="035-555-55-55"),
     *           @OA\Property(property="email", type="string", example="p_penduko@mailinator.com")
     *         )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Contact tracing is successfully updated!"}
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function update(Request $request, $id): JsonResponse
    {
        try
        {
            // extract
            extract($request->all());

            // get data
            $client_id = $client_id ?? null;
            $lastname = $lastname ?? '';
            $firstname = $firstname ?? '';
            $phone = $phone ?? '';
            $email = $email ?? '';

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

                // set connection
                $connection = \DB::connection('sqlsrv')->getPdo();

                // execute spm contact tracing
                $statement = $connection->prepare("
                    EXEC spm_contact_tracing
                        @flag=1,
                        @client_id=:client_id,
                        @lastname=:lastname,
                        @firstname=:firstname,
                        @phone=:phone,
                        @email=:email,
                        @id=:id
                ");
                $statement->bindParam(':client_id',$client_id);
                $statement->bindParam(':lastname',$lastname);
                $statement->bindParam(':firstname',$firstname);
                $statement->bindParam(':phone',$phone);
                $statement->bindParam(':email',$email);
                $statement->bindParam(':id',$id);
                $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // check phone
            if(isset($phone)&&!empty($phone))
            {
                // send sms
                $message = 'Success! Contact tracing is successfully updated!';
                $this->sendSMS($message,$phone);
            }

            return response()->json([
                'status'=>true,
                'message'=>'Contact tracing is successfully updated!'
            ],200);
        }
        catch(Exception $e)
        {
            return response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/v1/contact/tracing/{id}",
     *     tags={"Contact Tracing"},
     *     description="Delete Contact Tracing",
     *     summary="Delete Contact Tracing",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="contact tracing id",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Contact tracing is successfully deleted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function destroy($id): JsonResponse
    {
        try
        {
            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

                // set connection
                $connection = \DB::connection('sqlsrv')->getPdo();

                // execute spm contact tracing
                $statement = $connection->prepare("
                    EXEC spm_contact_tracing
                        @flag=2,
                        @client_id=null,
                        @lastname='',
                        @firstname='',
                        @phone='',
                        @email='',
                        @id=:id
                ");
                $statement->bindParam(':id',$id);
                $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            return response()->json([
                'status'=>true,
                'message'=>'Contact tracing is successfully deleted!'
            ],200);
        }
        catch(Exception $e)
        {
            return response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    // send sms
    private function sendSMS($message,$phone)
    {
        // create instance
        $sns = new SnsClient([
            'region' => env('SNS_REGION'),
            'version' => env('SNS_VERSION'),
            'credentials' => [
                'key' => env('SNS_KEY'),
                'secret' => env('SNS_SECRET')
            ]
        ]);

        try
        {
            // remove all special characters
            $phone = preg_replace('/[^0-9.]+/','',$phone);

            // send sms
            $result = $sns->publish([
                'Message' => $message,
                'PhoneNumber' => '+1' . $phone
            ]);

            // log
            \Log::channel('SMS_VERIFICATION_LOG')->info('SENT SMS Result [SUCCESS] ' . date('Y-md'), [
                'result' => $result,
                'phone' => $phone
            ]);

            // return
            return true;
        }
        catch (SnsException | Exception $e)
        {
            // log
            \Log::channel('SMS_VERIFICATION_LOG')->info('SENT SMS Result [ERROR] ' . date('Y-md'), [
                'result' => $e->getMessage(),
                'phone' => $phone
            ]);

            // return
            return false;
        }
    }
}
