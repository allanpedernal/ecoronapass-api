<?php

namespace App\Http\Controllers\CovidTest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Facility;

class FacilityController extends Controller
{
    /**
     * @OA\Get(
     *     path="/v1/covid-test/facility/list",
     *     tags={"Covid Testing Facility"},
     *     summary="Get List of Facilities",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="facilities",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="56A03F3E-2371-4299-9F9F-CE880964F59B"),
     *                      @OA\Property(property="facility_name", type="string", example="East Alabama Community COVID-19 Vaccination Clinic"),
     *                      @OA\Property(property="street", type="string", example="1716 Opelika Rd"),
     *                      @OA\Property(property="city", type="string", example="Auburn"),
     *                      @OA\Property(property="state", type="string", example="AL"),
     *                      @OA\Property(property="zip", type="string", example="36830"),
     *                      @OA\Property(property="phone_number", type="string", example="334-528-8222"),
     *                      @OA\Property(property="accession_number", type="string", example=""),
     *                      @OA\Property(property="collection_date", type="string", example=""),
     *                      @OA\Property(property="sending_facility_name", type="string", example=""),
     *                      @OA\Property(property="sending_facility_clia", type="string", example=""),
     *                      @OA\Property(property="facility_type_id", type="string", example="9E151FF7-6C56-EB11-AA80-029723BAB5FE"),
     *                      @OA\Property(property="facility_type", type="string", example="Hospital"),
     *                      @OA\Property(property="responsible_organization_id", type="string", example="37A60AB2-6D56-EB11-AA80-029723BAB5FE"),
     *                      @OA\Property(property="responsible_organization", type="string", example="US Govt."),
     *                      @OA\Property(property="created_at", type="string", example="2021-01-20T21:42:05.897000Z"),
     *                      @OA\Property(property="updated_at", type="string", example="2021-01-20T21:42:05.897000Z"),
     *                      @OA\Property(property="deleted_at", type="string", example=""),
     *                      @OA\Property(property="full_address", type="string", example="1716 Opelika Rd Auburn, AL 36830"),
     *                      @OA\Property(property="covid_status", type="string", example="Covid Free"),
     *                      @OA\Property(property="latitude", type="string", example="32.6243711"),
     *                      @OA\Property(property="longitude", type="string", example="-85.4462739"),
     *                      @OA\Property(property="type", type="string", example="covid_test"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function facilitiesList(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $facilities = Facility::where('type', 'covid_test')->get();

            // return response
            return response()->json([
                'status' => true,
                'data' => $facilities,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/v1/covid-test/facility/get-availability/{facility_id}",
     *     tags={"Covid Testing Facility"},
     *     summary="Check Facility if it has available slot",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="facility_id",
     *         in="path",
     *         required=true,
     *         description="Facility ID (ex. 56A03F3E-2371-4299-9F9F-CE880964F59B).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="available",
     *                 type="string",
     *                 example="248"
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function facilitiesAvailability(Request $request, $facility_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $statement = $connection->prepare("
                EXEC sp_get_total_available_covidtest
                    @facility_id = :facility_id
            ");

            $statement->bindParam(":facility_id", $facility_id);
            $statement->execute();
            $data = $statement->fetch(\PDO::FETCH_ASSOC);

            $available = isset($data['cnt']) ? $data['cnt'] : 0;

            // return response
            return response()->json([
                'status' => true,
                'data' => $available,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
