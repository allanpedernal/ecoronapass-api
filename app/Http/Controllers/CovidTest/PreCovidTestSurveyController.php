<?php

namespace App\Http\Controllers\CovidTest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;

/**
 * @group  Statement
 *
 * APIs for managing covid test result
 */
class PreCovidTestSurveyController extends Controller
{
    /**
     * @OA\Get(
     *     path="/v1/covid-test/pre-covid/get-test-survey-form",
     *     tags={"Pre-Covid Test Survey"},
     *     summary="Get List of Survey Questions",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="questions",
     *                 example={{
     *                    "question": "How would you rate your symptoms?",
     *                    "options": {"No Symptoms","Mild Symptoms","Severe Symptoms"}
     *                  },
     *                  {
     *                    "question": "Describe your exposure to Corona Virus",
     *                    "options": {"No Exposure","Potential Exposure","Suspected Exposure","Known Exposure"}
     *                  }},
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getTestSurveyForm(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $data = [
                0 => [
                    'question' => 'How would you rate your symptoms?',
                    'options' => [
                        'No Symptoms',
                        'Mild Symptoms',
                        'Severe Symptoms',
                    ],
                ],
                1 => [
                    'question' => 'Describe your exposure to Corona Virus',
                    'options' => [
                        'No Exposure',
                        'Potential Exposure',
                        'Suspected Exposure',
                        'Known Exposure',
                    ],
                ],
            ];

            // return response
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/v1/covid-test/pre-covid/submit-survey",
     *     tags={"Pre-Covid Test Survey"},
     *     summary="Submit Survey",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","question_one","question_two"},
     *           @OA\Property(property="patient_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *           @OA\Property(property="question_one", type="string", example="No Symptoms"),
     *           @OA\Property(property="question_two", type="string", example="No Exposure"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Pre-Covid Test Survey is successfully submitted!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function submitSurvey(Request $request)
    {
        try
        {
            // get error
            $errors = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'question_one' => 'required',
                    'question_two' => 'required',
                ])
            );

            // count error
            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $no_symptoms = 0;
            $mild_symptoms = 0;
            $severe_symptoms = 0;
            $no_exposure = 0;
            $potential_exposure = 0;
            $suspected_exposure = 0;
            $known_exposure = 0;

            if($question_one == 'No Symptoms') {
                $no_symptoms = 1;
            } elseif($question_one == 'Mild Symptoms') {
                $mild_symptoms = 1;
            } elseif($question_one == 'Severe Symptoms') {
                $severe_symptoms = 1;
            }

            if($question_two == 'No Exposure') {
                $no_exposure = 1;
            } elseif($question_two == 'Potential Exposure') {
                $potential_exposure = 1;
            } elseif($question_two == 'Suspected Exposure') {
                $suspected_exposure = 1;
            } elseif($question_two == 'Known Exposure') {
                $known_exposure = 1;
            }

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC spm_symptom_quiz
                        @flag = 0
                    ,   @patient_id = :patient_id
                    ,   @no_symptoms = :no_symptoms
                    ,   @mild_symptoms = :mild_symptoms
                    ,   @severe_symptoms = :severe_symptoms
                    ,   @no_exposure = :no_exposure
                    ,   @potential_exposure = :potential_exposure
                    ,   @suspected_exposure = :suspected_exposure
                    ,   @known_exposure = :known_exposure
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':no_symptoms',$no_symptoms);
            $statement->bindParam(':mild_symptoms',$mild_symptoms);
            $statement->bindParam(':severe_symptoms',$severe_symptoms);
            $statement->bindParam(':no_exposure',$no_exposure);
            $statement->bindParam(':potential_exposure',$potential_exposure);
            $statement->bindParam(':suspected_exposure',$suspected_exposure);
            $statement->bindParam(':known_exposure',$known_exposure);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return response()->json([
                'status'=>true,
                'message'=>'Pre-Covid Test Survey is successfully submitted!'
            ],200);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
