<?php

namespace App\Http\Controllers\CovidTest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{
    /**
     * @OA\Get(
     *     path="/v1/covid-test/available-schedules",
     *     tags={"Covid Test Schedule"},
     *     summary="Get test location schedules",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="facility_id",
     *         in="query",
     *         required=true,
     *         description="Facility ID (ex. 56A03F3E-2371-4299-9F9F-CE880964F59B).",
     *     ),
     *     @OA\Parameter(
     *         name="date",
     *         in="query",
     *         required=true,
     *         description="Date (ex. 7/30/2021).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="schedules",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="start_time", type="string", example="07:15"),
     *                      @OA\Property(property="limit", type="string", example="10"),
     *                      @OA\Property(property="cnt", type="string", example="9"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getSchedules(Request $request)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $statement = $connection->prepare("
                EXEC sp_get_all_available_schedule_covidtest
                        @facility_id = :facility_id
                    ,   @start_datetime = :start_datetime

            ");

            $statement->bindParam(":facility_id", $facility_id);
            $statement->bindParam(":start_datetime", $date);
            $statement->execute();
            $schedules = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'status' => true,
                'data' => $schedules,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

}
