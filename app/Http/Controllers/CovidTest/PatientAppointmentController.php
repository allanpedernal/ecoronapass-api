<?php

namespace App\Http\Controllers\CovidTest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @group  Statement
 *
 * APIs for managing covid test result
 */
class PatientAppointmentController extends Controller
{
    /**
     * @OA\Get(
     *     path="/v1/covid-test/patient-appointment/get/{patient_id}",
     *     tags={"Patient Covid Test Appointment"},
     *     summary="Get Appointment",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID (ex. 6B698C2D-8609-4252-A58E-9930A6055002).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(property="id", type="string", example="703DCE78-37EE-EB11-AAC4-023887F26D8E"),
     *                 @OA\Property(property="appointment_schedule", type="string", example="07/30/2021 07:15 AM"),
     *                 @OA\Property(property="facility_id", type="string", example="56A03F3E-2371-4299-9F9F-CE880964F59B"),
     *                 @OA\Property(property="facility_name", type="string", example="test facility"),
     *                 @OA\Property(property="facility_address", type="string", example="123 Test St. New Jersey, USA"),
     *                 @OA\Property(property="facility_longitude", type="string", example="100"),
     *                 @OA\Property(property="facility_latitude", type="string", example="120"),
     *                 @OA\Property(property="patient_address", type="string", example="123 Test St. New Jersey, USA"),
     *                 @OA\Property(property="patient_longitude", type="string", example="100"),
     *                 @OA\Property(property="patient_latitude", type="string", example="120"),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getAppointment(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $statement = $connection->prepare("
                EXEC sp_patient_covidtest_appointment
                    @patient_id = :patient_id
            ");

            $statement->bindParam(":patient_id", $patient_id);
            $statement->execute();
            $appointment = $statement->fetch(\PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'status' => true,
                'data' => $appointment ?: [],
                'message' => 'success',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/v1/covid-test/patient-appointment/save",
     *     tags={"Patient Covid Test Appointment"},
     *     summary="Save Appointment",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id","facility_id","time","date"},
     *           @OA\Property(property="patient_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *           @OA\Property(property="facility_id", type="string", example="56A03F3E-2371-4299-9F9F-CE880964F59B"),
     *           @OA\Property(property="date", type="string", example="07/30/2021"),
     *           @OA\Property(property="time", type="string", example="7:15 AM"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Appointment is successfully saved!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function saveAppointment(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                    'facility_id' => 'required',
                    'date' => 'required',
                    'time' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                $err_mess = '';
                foreach($error as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            $user = User::find($patient_id);
            $start_time = strtotime($time);
            $start_time_parsed = date("H:i:s", $start_time);
            $start_date = $date . " " . $start_time_parsed;
            $patient_name = $user->Firstname . " " . $user->Lastname;

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC spm_covidtest_schedule
                        @patient_id = :patient_id
                    ,   @facility_id = :facility_id
                    ,   @patient_name = :patient_name
                    ,   @start_date = :start_date
                    ,   @Description = 'covid test'
                    ,   @provider_id = null
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->bindParam(':facility_id',$facility_id);
            $statement->bindParam(':patient_name',$patient_name);
            $statement->bindParam(':start_date',$start_date);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return response()->json([
                'status'=>true,
                'message'=>'Appointment is successfully saved!'
            ],200);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/v1/covid-test/patient-appointment/cancel",
     *     tags={"Patient Covid Test Appointment"},
     *     summary="Cancel Appointment",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\RequestBody(
     *       required=true,
     *       description="Description",
     *       @OA\JsonContent(
     *           required={"patient_id"},
     *           @OA\Property(property="patient_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *       ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": false, "message": "Appointment is successfully cancelled!"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="error",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"error": true, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function cancelAppointment(Request $request)
    {
        try
        {
            // get error
            $error = static::validateRequest(
                \Validator::make($request->all(), [
                    'patient_id' => 'required',
                ])
            );

            // count error
            if (count($error) > 0)
            {
                return \Response::json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // start transaction
            \DB::connection('sqlsrv')->beginTransaction();

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract all
            extract($request->all());

            // execute insurance mobile
            $statement = $connection->prepare("
                EXEC spm_cancel_schedule_appointment
                       @patient_id = :patient_id
            ");
            $statement->bindParam(':patient_id',$patient_id);
            $statement->execute();

            // commit
            \DB::connection('sqlsrv')->commit();

            // return response
            return response()->json([
                'error'=>false,
                'message'=>'Appointment is successfully cancelled!'
            ],200);
        }
        catch(\Exception $e)
        {
            return response()->json([
                'error'=>true,
                'message'=>$e->getMessage()
            ],500);
        }
    }
}
