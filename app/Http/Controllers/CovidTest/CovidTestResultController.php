<?php

namespace App\Http\Controllers\CovidTest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;

/**
 * @group  Statement
 *
 * APIs for managing covid test result
 */
class CovidTestResultController extends Controller
{
    /**
     * @OA\Get(
     *     path="/v1/covid-test/patient/history/list/{patient_id}",
     *     tags={"Patient Covid Test History"},
     *     summary="Get List of Patient's Covid Test Results",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="patient_id",
     *         in="path",
     *         required=true,
     *         description="Patient ID (ex. 6B698C2D-8609-4252-A58E-9930A6055002).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="test_results",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="id", type="string", example="1548C01F-4F71-EB11-AAC4-023887F26D8E"),
     *                      @OA\Property(property="patient_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *                      @OA\Property(property="loinc_id", type="string", example="E1AA45BF-6052-EB11-AA80-029723BAB5FE"),
     *                      @OA\Property(property="loinc_code", type="string", example="94500-6"),
     *
     *
     *                      @OA\Property(property="loinc_description", type="string", example="SARS coronavirus 2 RNA [Presence] in Respiratory specimen by NAA with probe detection"),
     *                      @OA\Property(property="manufacturer", type="string", example="1drop Inc."),
     *                      @OA\Property(property="test_kit", type="string", example="^Throat swab^SCT"),
     *                      @OA\Property(property="test_model", type="string", example="1copy COVID-19 qPCR Multi Kit*"),
     *                      @OA\Property(property="test_date", type="string", example="2021-02-18 00:00:00.000"),
     *                      @OA\Property(property="test_result", type="string", example="^Presumptive positive^SCT"),
     *                      @OA\Property(property="result_date", type="string", example="2021-02-18 00:00:00.000"),
     *                      @OA\Property(property="active_date", type="string", example=""),
     *                      @OA\Property(property="expiration_date", type="string", example=""),
     *                      @OA\Property(property="created_at", type="string", example="2021-02-17 13:37:03.063"),
     *                      @OA\Property(property="updated_at", type="string", example=""),
     *                      @OA\Property(property="deleted_at", type="string", example=""),
     *                      @OA\Property(property="analyte", type="string", example="SARS-CoV-2 Result Interpretation"),
     *                      @OA\Property(property="provider_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *                      @OA\Property(property="Lastname", type="string", example="Vanstein"),
     *                      @OA\Property(property="Firstname", type="string", example="John"),
     *                      @OA\Property(property="Middlename", type="string", example="Sebastian"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getTestResults(Request $request, $patient_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $data = DB::table('patient_test_results')
                ->select('patient_test_results.*', 'Client.Lastname', 'Client.Firstname', 'Client.Middlename')
                ->join('Client', 'Client.ID', '=', 'patient_test_results.patient_id')
                ->where('patient_test_results.patient_id', $patient_id)->get();

            // return response
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * @OA\Get(
     *     path="/v1/covid-test/patient/history/detail/{test_result_id}",
     *     tags={"Patient Covid Test History"},
     *     summary="Get Patient Covid Test Result Detail",
     *     security={
     *       {"bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="test_result_id",
     *         in="path",
     *         required=true,
     *         description="Test Result ID (ex. 1548C01F-4F71-EB11-AAC4-023887F26D8E).",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean",
     *                 example="true"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(property="id", type="string", example="1548C01F-4F71-EB11-AAC4-023887F26D8E"),
     *                 @OA\Property(property="patient_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *                 @OA\Property(property="loinc_id", type="string", example="E1AA45BF-6052-EB11-AA80-029723BAB5FE"),
     *                 @OA\Property(property="loinc_code", type="string", example="94500-6"),
     *                 @OA\Property(property="loinc_description", type="string", example="SARS coronavirus 2 RNA [Presence] in Respiratory specimen by NAA with probe detection"),
     *                 @OA\Property(property="manufacturer", type="string", example="1drop Inc."),
     *                 @OA\Property(property="test_kit", type="string", example="^Throat swab^SCT"),
     *                 @OA\Property(property="test_model", type="string", example="1copy COVID-19 qPCR Multi Kit*"),
     *                 @OA\Property(property="test_date", type="string", example="2021-02-18 00:00:00.000"),
     *                 @OA\Property(property="test_result", type="string", example="^Presumptive positive^SCT"),
     *                 @OA\Property(property="result_date", type="string", example="2021-02-18 00:00:00.000"),
     *                 @OA\Property(property="active_date", type="string", example=""),
     *                 @OA\Property(property="expiration_date", type="string", example=""),
     *                 @OA\Property(property="created_at", type="string", example="2021-02-17 13:37:03.063"),
     *                 @OA\Property(property="updated_at", type="string", example=""),
     *                 @OA\Property(property="deleted_at", type="string", example=""),
     *                 @OA\Property(property="analyte", type="string", example="SARS-CoV-2 Result Interpretation"),
     *                 @OA\Property(property="provider_id", type="string", example="6B698C2D-8609-4252-A58E-9930A6055002"),
     *             ),
     *             @OA\Property(
     *                 property="models",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="model", type="string", example="1copy COVID-19 qPCR Multi Kit*"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="specimens",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="vendor_specimen_text", type="string", example="^Anterior nares swab^SCT"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="results",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="vendor_result_text", type="string", example="^Specimen unsatisfactory for evaluation^SCT"),
     *                 ),
     *             ),
     *             @OA\Property(
     *                 property="analytes",
     *                 type="array",
     *                 @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="vendor_analyte_name", type="string", example="SARS-CoV-2 Result Interpretation"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": false, "message": "Error Message"}
     *         )
     *     )
     * )
     */
    public function getTestResultDetail(Request $request, $test_result_id)
    {
        try
        {
            // set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            // extract
            extract($request->all());

            $data = DB::table('patient_test_results')->where('id', $test_result_id)->first();

             # set connection
             $connection = \DB::connection('sqlsrv')->getPdo();

             # models
             $statement = $connection->prepare("
                 SET NOCOUNT ON; EXEC sp_Model_List
                 @Manufacturer = :manufacturer
             ");

             $statement->bindParam(':manufacturer', $data->manufacturer);
             $statement->execute();
             $models = $statement->fetchAll(\PDO::FETCH_ASSOC);

             # specimens
             $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_VendorSpecimen_List
                @Manufacturer = :manufacturer,
                @Model = :model
            ");

            $statement->bindParam(':manufacturer', $data->manufacturer);
            $statement->bindParam(':model', $data->test_model);
            $statement->execute();
            $specimens = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_VendorResult_List
                @Manufacturer = :manufacturer,
                @Model = :model,
                @VendorSpecimen = :specimen,
                @vendor_analyte_name = :analyte
            ");

            $statement->bindParam(':manufacturer', $data->manufacturer);
            $statement->bindParam(':model', $data->test_model);
            $statement->bindParam(':specimen', $data->test_kit);
            $statement->bindParam(':analyte', $data->analyte);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Vendor_Analyte
                @Manufacturer = :manufacturer,
                @Model = :model,
                @VendorSpecimen = :specimen
            ");

            $statement->bindParam(':manufacturer', $data->manufacturer);
            $statement->bindParam(':model', $data->test_model);
            $statement->bindParam(':specimen', $data->test_kit);
            $statement->execute();
            $analytes = $statement->fetchAll(\PDO::FETCH_ASSOC);

            // return response
            return response()->json([
                'status' => true,
                'data' => [
                    'test_results' => $data,
                    'models' => $models,
                    'specimens' => $specimens,
                    'results' => $results,
                    'analytes' => $analytes,
                ],
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
