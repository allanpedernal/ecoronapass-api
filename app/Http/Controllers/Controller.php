<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="eCoronaPass API Documentation",
     *   version="1.0",
     *   @OA\Contact(
     *     email="leo@softbir.com",
     *     name="IT Manager"
     *   )
     * )
     *
     * @OA\Server(
     *     url=API_BASE_URL,
     *     description=""
     * )
     *
     */
    protected static function validateRequest($validator)
    {
        $err_arr = [];
        if ($validator->fails()) {
            $has_error = checkEmptyParam($validator->errors()->all());
            if (count($has_error) > 0) {
                foreach ($validator->errors()->messages() as $key => $err) {
                    if (is_array($err)) {
                        foreach ($err as $item) {
                            $err_arr[$key] = $item;
                        }
                    }
                }
            }
        }
        return $err_arr;
    }
}
