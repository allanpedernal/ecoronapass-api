<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Index
     *
     * @param Request $request
     * @return view
     */
    public function index(Request $request)
    {
        $roles = DB::table('roles')->pluck('name', 'id')->toArray();
        $role_id = Auth::user()->role_id;
        return view('users.index', compact('roles', 'role_id'));
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $id = $data['id'];
            unset($data['_token']);
            unset($data['id']);

            if ($id) {
                $user = $this->userRepository->query()->where('id',$id)->update($data);
            } else {
                $user = $this->userRepository->create($data);
                $id = $user->id;
            }


            return redirect()->route('users.edit', $id)->with('success', 'User successfully saved.');
        } catch (\Exception $e) {
            $message = $e->getMessage();
            // $message = "Internal server error.";
            return redirect()->back()->with('error', $message);
        }
    }

    public function profile()
    {
        $user_id = Auth::id();
        return view('users.profile', compact('user_id'));
    }
}
