<?php

namespace App\Http\Controllers;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function recipient()
    {
        return view('provider.recipient');
    }

}
