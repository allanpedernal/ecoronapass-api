<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;
use App\Models\ModelHasRoles;
use App\Models\RoleModel;
use App\Models\Schedule;
use App\Repositories\AppointmentRepository;
use App\Helpers\SmsHelper;
use App\Notifications\ScheduleNotification;
use App\User;
use Carbon\Carbon;
use Validator;
use DB;
use PDO;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function administrative()
    {
        return view('admin.administrative');
    }

    public function clinical()
    {
        return view('admin.clinical');
    }

    public function report() {
        return view('admin.report');
    }

    public function patientDetailReport() {
        return view('admin.patient-detail-report');
    }

    public function userManagement()
    {
        $roles = DB::table('roles')->get();
        $facilities = DB::table('Facility')->get();
        $patient_role_id = $patient_role = RoleModel::where('name', 'patient')->first()->id;
        return view('admin.user-management', compact('roles', 'facilities', 'patient_role_id'));
    }

    public function users(Request $request)
    {

        $search             = $request->search;
        $columns            = $request->columns;
        $start              = $request->start; // limist start
        $length             = $request->length; //total show
        $order              = $request->order;
        $draw               = $request->draw;
        $search_val         = $search['value'].'%'; //search
        $order_by_column    = $columns[$order[0]['column']]['data']; //order by column
        $order_by_dir       = $order[0]['dir']; //order desc or asc

        if ($order_by_column == 'name') {
            $order_by_column = "Roles." . $order_by_column;
        } else {
            $order_by_column = "Client." . $order_by_column;
        }

        //Query Builder
        $query  = DB::table('Client')->select(
            'Client.*', 'Roles.name',
            DB::raw('Roles.id as role_id'))
        ->join('Model_Has_Roles', 'Model_Has_Roles.model_uuid', '=', 'Client.ID')
        ->join('Roles', 'Roles.id', '=', 'Model_Has_Roles.role_id');

        if ($search['value']) {
            $query->where(function ($queryBuilder) use ($search_val) {
                $search = explode(',', $search_val);
                if (count($search) == 1) {
                    $queryBuilder->orWhere('Client.Lastname', 'LIKE', trim($search[0]));
                } else {
                    $queryBuilder->where('Client.Lastname', 'LIKE', trim($search[0]));
                    $queryBuilder->where('Client.Firstname', 'LIKE', trim($search[1]));
                }
            });
        }

        $recordsTotal       = $query->count();
        $recordsFiltered    = $recordsTotal;
        $data               = $query->offset($start)->limit($length)
        ->orderBy($order_by_column, $order_by_dir)
        ->get();

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function createUser(Request $request)
    {
        $model_type = 'App\User';
        $validator = Validator::make($request->all(), [
            'Lastname'      => 'required',
            'Firstname'     => 'required',
            'username'      => 'required|unique:Client,username',
            'email'         => 'required|email',
            'role_id'       => 'required|exists:Roles,id',
            'password'      => 'nullable|string',
            'facility_id'   => 'nullable'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', implode(", ",$validator->messages()->all()));
        }

        $data = $validator->validate();

        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user = User::create($data);
        $role_id = $data['role_id'];
        $user = ModelHasRoles::updateOrCreate(
            ['role_id' => $role_id, 'model_type' => $model_type, 'model_uuid' => $user->ID],
            ['role_id' => $role_id, 'model_type' => $model_type, 'model_uuid' => $user->ID]
        );

        return redirect()->back()->with('success', 'User created.');
    }

    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required|exists:Client,ID',
            'Lastname'      => 'required',
            'Firstname'     => 'required',
            'username'      => 'required|unique:Client,username,'.$request->id,
            'email'         => 'required|email',
            'role_id'       => 'required|exists:Roles,id',
            'password'      => 'nullable|string',
            'facility_id'   => 'nullable'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', implode(", ",$validator->messages()->all()));
        }

        //get user role id
        $patient_role = RoleModel::where('name', 'patient')->first();

        $data       = $validator->validate();
        $role_id    = $data['role_id'];
        $id         = $data['id'];

        unset($data['role_id']);
        unset($data['id']);

        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        if ($role_id == $patient_role->id) {
            $data['facility_id'] = null;
        }

        //update user
        User::where('id', $id)
        ->update($data);

        //update user role
        ModelHasRoles::where('model_uuid', $id)
        ->update(['role_id' => $role_id]);

        return redirect()->back()->with('success', 'User updated.');
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required|exists:Client,ID',
            'Lastname'      => 'required',
            'Firstname'     => 'required',
            'username'      => 'required|unique:Client,username,'.$request->id,
            'email'         => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', implode(", ",$validator->messages()->all()));
        }

        //get user role id
        $patient_role = RoleModel::where('name', 'patient')->first();

        $data       = $validator->validate();
        $id         = $data['id'];

        unset($data['id']);

        //update user
        User::where('id', $id)
        ->update($data);

        return redirect()->back()->with('success', 'User updated.');
    }

    public function scheduling()
    {
        $start_time = Schedule::appointmentStartTime();
        $end_time =  Schedule::appointmentEndTime();
        return view('admin.scheduling', compact('start_time', 'end_time'));
    }

    public function patientList(Request $request)
    {
        // $patient_role_id    = $patient_role = RoleModel::where('name', 'patient')->first()->id;
        $search             = $request->search;
        $columns            = $request->columns;
        $start              = $request->start; // limist start
        $length             = $request->length; //total show
        $order              = $request->order;
        $draw               = $request->draw;
        $search_val         = $search['value'].'%'; //search
        $order_by_column    = $columns[$order[0]['column']]['data']; //order by column
        $order_by_dir       = $order[0]['dir']; //order desc or asc

        //Query Builder
        $query  = DB::table('Client');
        //->select('Client.*', 'Roles.name',DB::raw('Roles.id as role_id'))
        // ->join('Model_Has_Roles', 'Model_Has_Roles.model_uuid', '=', 'Client.ID')
        // ->join('Roles', 'Roles.id', '=', 'Model_Has_Roles.role_id');
        // ->where('Roles.id', $patient_role_id);
        if ($search['value']) {
            $query->where(function ($queryBuilder) use ($search_val) {
                $search = explode(',', $search_val);
                if (count($search) == 1) {
                    $queryBuilder->orWhere('Client.Lastname', 'LIKE', trim($search[0]));
                } else {
                    $queryBuilder->where('Client.Lastname', 'LIKE', trim($search[0]));
                    $queryBuilder->where('Client.Firstname', 'LIKE', trim($search[1]));
                }
            });
        }

        $recordsTotal       = $query->count();
        $recordsFiltered    = $recordsTotal;
        $data               = $query->offset($start)->limit($length)
        ->orderBy($order_by_column, $order_by_dir)
        ->get();

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function overrideSchedule(Request $request)
    {
        $flag       =  $request->add_follow_up;
        $patient_id =  $request->patient_id;
        $facility_id =  auth()->user()->facility_id;
        $date_time = date('Y-m-d H:i:s', strtotime($request->schedule_date . ' ' .$request->schedule_time));
        $user = User::where('ID', $patient_id)->first();
        $facility = Facility::find($facility_id);

        try {
            DB::beginTransaction();
            $connection = DB::connection('sqlsrv')->getPdo();
            $statement = $connection->prepare("
                EXEC sp_Override_Schedule
                    @flag = :flag
                ,   @PatientID = :PatientID
                ,   @FacilityID = :FacilityID
                ,   @DateTime = :DateTime
            ");

            $statement->bindParam(":flag", $flag);
            $statement->bindParam(":PatientID", $patient_id);
            $statement->bindParam(":FacilityID", $facility_id);
            $statement->bindParam(":DateTime", $date_time);
            $statement->execute();
            $return = $statement->fetch(PDO::FETCH_ASSOC);
            $confirmation_number = $return['new_seq_num'];

            $status = 'success';
            $message = 'Schedule override successful.';

            DB::commit();

            # 02/08/2021 02:45 PM
            $schedule_date = Carbon::parse($date_time)->format('m/d/Y h:i A');

            $br = "Please bring picture ID, proof of employment and wear clothing that allows access to your upper arm for vaccination. Confirmation Number: {$confirmation_number}";
            $smsMessage =  "Hello $user->Firstname. You have a confirmed Appointment to receive the COVID-19 Vaccine on $schedule_date. To review your appointment, please go to https://eastalcovidvaccine.com and sign in. The vaccination location is $facility->full_address. \r\n$br";

            # Sending Email Notification
            $user->notify(new ScheduleNotification($schedule_date, $facility, $confirmation_number));
            $sendSMS = SmsHelper::send($user->phone, $smsMessage);
        } catch (\exception $e) {
            DB::rollBack();
            $status = 'error';
            $message = 'Internal server error.';
        }

        return compact('status', 'message');
    }

    public function updateScheduleStatus(Request $request)
    {
        $schedule_id = $request->schedule_id;
        $status = 'declined';
        $status_type = 'check-in-status';
        $reason = 'No show';
        $updated_by = auth()->user()->ID;
        $date = $request->date;

        $connection = DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare(
            "EXEC sp_UpdateVaccine_ScheduleStatus
                    @ScheduleID = :p_schedule_id
                ,	@Status = :p_status
                ,	@StatusType = :p_statustype
                ,	@Reason = :p_reason
                ,	@updated_by = :updated_by"
        );

        $statement->bindParam(':p_schedule_id', $schedule_id);
        $statement->bindParam(':p_status', $status);
        $statement->bindParam(':p_statustype', $status_type);
        $statement->bindParam(':p_reason', $reason);
        $statement->bindParam(':updated_by', $updated_by);
        $statement->execute();

        //get schedule data
        $statement = $connection->prepare(
            "EXEC sp_Get_Calendar_Appointment_perDateTime
            @start_date = :start_date"
        );

        $statement->bindParam(':start_date', $date);
        $statement->execute();
        $schedule_data = $statement->fetch(PDO::FETCH_ASSOC);

        $patients = app(AppointmentRepository::class)->getEventPatients($date);
        $status = 'success';
        $message = 'Patient decline schedule successful.';

        return compact('status', 'message', 'patients', 'schedule_data');
    }

    public function configureSchedule()
    {
        $start_time = Schedule::appointmentStartTime();
        $end_time =  Schedule::appointmentEndTime();
        $facilities = Facility::all()->pluck('facility_name', 'id')->toArray();
        return view('admin.configure-schedule', compact('start_time', 'end_time', 'facilities'));
    }

    public function getVaccineSchedule(Request $request)
    {
        $schedule_date = $request->schedule_date;
        $facility_id = $request->facility_id;
        $connection = DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_schedule_generation
                @sched_date  = :sched_date,
                @facility_id = :facility_id
        ");
        $statement->bindParam(":sched_date", $schedule_date);
        $statement->bindParam(":facility_id", $facility_id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function updateVaccineConfigure(Request $request)
    {
        try {
            DB::beginTransaction();
            DB::table('Vaccine_Schedule')->where('facility_id',$request->facility_id)->where('schedule_date', $request->schedule_date)->delete();
            $inser_data = [];
            foreach ($request->schedule_slots as $config) {
                $inser_data[] = [
                    'facility_id'   => $request->facility_id,
                    'start_time'    => $config['time'],
                    'schedule_date' => $request->schedule_date,
                    'limit'         => $config['limit'] +  $config['add_to_limit'],
                    'created_at'    => now(),
                    'updated_at'    => now(),
                    'created_by'    => auth()->user()->ID,
                    'updated_by'    => auth()->user()->ID
                ];
            }
            DB::table('Vaccine_Schedule')->insert($inser_data);
            DB::commit();
            $status = 'success';
            $message = 'Vaccine schedule configuration saved.';
        } catch (\Exception $e) {
            DB::rollback();
            $status = 'error';
            $message = $e->getMessage();
        }

        return compact('status', 'message');
    }

    public function patientsReport(){
        return view('admin.reports.patients');
    }

    public function schedulesReport(){
        return view('admin.reports.schedules');
    }
}
