<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(Request $request)
    {

        $start_time = Schedule::appointmentStartTime();
        $end_time = Schedule::appointmentEndTime();
        return view('appointment.index', compact('start_time', 'end_time'));
    }
}
