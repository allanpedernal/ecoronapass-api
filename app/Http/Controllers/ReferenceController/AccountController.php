<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class AccountController extends Controller
{
    public function changePassword()
    {
    	return view('account.change-password');
    }

    public function savePassword(Request $request)
    {
    	try {
    		$data = $request->all();
    		$user = auth()->user();

    		if(!Hash::check($data['old_password'], $user->password)){
    			return redirect()->route('change.password')->with('error', 'Incorrect OLD password');
    		}

    		if($data['password'] !== $data['confirm_password']){
    			return redirect()->route('change.password')->with('error', 'Password and password confirmation doesn\'t match!');
    		}

    		$user->password = bcrypt($data['password']);
    		$user->save();
    		$status = 'success';
    		$message = 'Saved password successfully';
    	} catch (\Exception $e) {
    		$status = 'error';
    		$message = $e->getMessage();
    	}
    	return redirect()->route('change.password')->with($status, $message);
    }
}
