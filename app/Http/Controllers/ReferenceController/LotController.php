<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VaccineInformation;
use DB;

class LotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventory_locations = DB::table('vaccine_inventory_location')->get();
        return view('lot.index', compact('inventory_locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lot.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();
            VaccineInformation::create($data);
            DB::commit();
            $status = 'success';
            $message = 'Successfully added lot';
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
            return redirect()->route('lot.create')->with($status, $message);
        }
        return redirect()->route('lot.index')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lot = VaccineInformation::find($id);
        return view('lot.show', compact('lot'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lot = VaccineInformation::find($id);
        return view('lot.edit', compact('lot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        try {
            DB::beginTransaction();
            $lot = VaccineInformation::find($id);
            $lot->update($data);
            DB::commit();
            $status = 'success';
            $message = 'Successfully updated lot';
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
            return redirect()->route('lot.edit')->with($status, $message);
        }
        return redirect()->route('lot.index')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
