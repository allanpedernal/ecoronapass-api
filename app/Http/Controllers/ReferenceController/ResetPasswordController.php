<?php

namespace App\Http\Controllers;

use App\Services\UtilityService;
use App\Traits\JWTTrait;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PDO;

class ResetPasswordController extends Controller
{
    //
    use JWTTrait;


    public function resetPasswordToken(Request $request): JsonResponse
    {
        try {
            $email = $request->get('email');
            $connection = app('pdo_connection')->getPdo();
            $errors = app(UtilityService::class)->validateRequest(Validator::make($request->all(), [
                'email' => [
                    'required',
                    function($attribute, $value, $fail) use ($connection) {
                        $statement = $connection->prepare("SELECT * FROM [dbo].[Client] WHERE email = :email");
                        $statement->bindParam(':email', $value);
                        $statement->execute();
                        $result = $statement->fetch(PDO::FETCH_ASSOC);
                        if (!$result) {
                            $fail($attribute.' does not exist!.');
                        }
                    }
                ]
            ]));

            if (count($errors) > 0) {
                return response()->json([
                    'success' => false,
                    'data' => $errors
                ], 400);
            }

            #create link.
            $user = User::where('email', $email)->first();
            if ($user) {
                #expiration set to 1h.
                $expiration = time() + (60 * 60);
                $token = self::jwtEncode($user, $expiration);

                return response()->json([
                    'success' => false,
                    'data' => [
                        'reset-token' => $token,
                        'expiration' => $expiration
                    ]
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'User related to this email not found!'
                ], 400);
            }

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }


    public function resetPassword(Request $request): JsonResponse
    {
        try {
            $password = $request->get('password');
            $reset_token = $request->get('reset_token');

            $errors = app(UtilityService::class)->validateRequest(Validator::make($request->all(), [
                'password' => 'required|min:6',
                're_password' => 'required|min:6|same:password',
                'reset_token' => 'required'
            ]));

            if (count($errors) > 0) {
                return response()->json([
                    'success' => false,
                    'data' => $errors
                ], 400);
            }

            $reset_token = self::jwtDecode($reset_token);
            $reset_token = (array) $reset_token;

            $now = Carbon::now();
            $expiration = new Carbon($reset_token['exp']);
            $expired = false;
            if (!$expiration->gte($now)) {
                $expired = true;
            }

            if (!$expired) {
                $user_data = $reset_token['data']->user;
                $user = User::where('email', $user_data->{'email'})->first();
                $user->password = Hash::make($password);
                $user->save();
                return response()->json([
                    'success' => true,
                    'message' => 'Password successfully reset!'
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Token expired!'
                ], 400);
            }
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
