<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Excel;
use App\Exports\ImmPrintExport;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function loinc(Request $request)
    {
        return view('report.loinc');
    }

    public function immPrintRejection()
    {
        $connection = \DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_immprint_rejection");
        $statement->execute();
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $export = new ImmPrintExport($result);
        return Excel::download($export, 'immprint_rejection.xlsx');
    }

    public function immPrintRejects()
    {
        return view('report.immprint');
    }
}
