<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\FlexForm;
use App\Models\PatientContraindicationAnswer;
use App\Models\Schedule;
use App\Models\VaccineContraindication;
use App\Models\VaccineContraindicationQuestion;
use App\Services\UtilityService;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Encryption\Encrypter;
use DNS2D;
use Illuminate\Support\Facades\Validator;
use PDF;
use DB;
use Illuminate\Support\Facades\Auth;
use PDO;

class PatientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if (auth()->user()->hasRole('provider')) {
            return redirect()->route('provider.recipient');
            // return redirect()->route('provider.recipient')->with('error', "You are logged in as vaccinator and not allowed access Patient List. You will be redirected to Recipient List.");
        } else if (auth()->user()->hasRole('patient')) {
            abort(403);
        }

        return view('patient.patient_list');
    }

    public function profile($patient_id, $schedule_id = null)
    {
        try {
            if (DB::table('Client')->where(['ID' => $patient_id])->exists()) {
                $schedule = null;
                if ($schedule_id) {
                    $schedule = Schedule::findOrFail($schedule_id);
                }
                return view('patient.profile', compact('patient_id', 'schedule'));
            } else {
                abort(404);
            }
        } catch (Exception $e) {
            abort(404);
        }
    }

    public function generatePass($test_result_id)
    {
        $test_result = DB::table('patient_test_results')->where('id', $test_result_id)->first();
        if($test_result){
            $patient = DB::table('Client')->where('ID', $test_result->patient_id)->first();
            $raw_data[] = $patient->Mrn;
            $raw_data[] = $patient->Lastname;
            $raw_data[] = $patient->Firstname;
            $raw_data[] = date('Ymd', strtotime($patient->Dob));
            $raw_data[] = $test_result->test_result;
            $raw_data[] = $test_result->expiration_date;

            $raw_data = implode('|', $raw_data);

            //get encryption method and key
            $cipher = env('CIPHER');
            $enc_key = env('ENCRYPT_KEY');

            //use laravel encrypter
            $newEncrypter = new Encrypter( $enc_key, $cipher );
            $encrypted = $newEncrypter->encrypt( $raw_data );

            //generate pdf417 barcode
            $barcode_img = DNS2D::getBarcodePNG($encrypted, 'PDF417', 1.5, 1.5);

            //get member data
            $member_data = [
                'MemberID' => $patient->MemberID,
                'Mrn' => $patient->Mrn,
                'Lastname' => $patient->Lastname,
                'Firstname' => $patient->Firstname,
                'Middlename' => $patient->Middlename,
                'Dob' => date('Y-m-d', strtotime($patient->Dob)),
                'Photo' => $patient->Photo ? $patient->Photo : 'img/default.png',
                'Expiration' => date('Y-m-d', strtotime($test_result->expiration_date)),
                'TestResult' => $test_result->test_result,
            ];

            $pdf = PDF::LoadView('patient.test-result', compact('member_data', 'barcode_img'));
            return $pdf->stream('test-result-' . Carbon::now());
        }else{
            dd('Error generating barcode');
        }
    }

    public function preRegistration($patient_id = null)
    {
        $patient_id = $patient_id ?? auth()->user()->ID;
        $contraindication = 'Pre Registration';
        $brand = VaccineContraindication::where('brand', $contraindication)->firstOrFail();
        $facility_id = Facility::where('facility_name', env('FACILITY_NAME'))->first()->id;
        $id = $brand->id;
        return view('patient.pre-registration-form', compact('contraindication', 'id', 'patient_id', 'facility_id'));
    }

    public function survey($flexForm, $patient_id = null)
    {
        $flexForm = ucwords(str_replace('_', ' ', $flexForm));
        FlexForm::where('name', $flexForm)->firstOrFail();
        return view('patient.flex-form', compact('flexForm', 'patient_id'));
    }

    public function viewPreRegistration($patient_id)
    {
        $survey = 'Pre Registration';
        $brand = VaccineContraindication::where('brand', $survey)->firstOrFail();

        $questions = VaccineContraindicationQuestion::where('vaccine_contraindication_id', $brand->id)
            ->where('options', "<>", '{"0":"Exit"}')
            ->whereNotIn('id', ['66526280-3E50-EB11-AA80-029723BAB5FE', '81537592-FC63-EB11-AA80-029723BAB5FE'])
            ->orderBy('question_number')
            ->get();

        $answers = PatientContraindicationAnswer::where('vaccine_contraindication_id', $brand->id)->where('patient_id', $patient_id)->get();

        return view('patient.view-survey', compact('survey', 'questions', 'answers', 'patient_id'));
    }

    public function patientDetail($id) {
        try {
            $sql = "SELECT * FROM [dbo].[Client] WHERE ID = :id";
            $connection = app('pdo_connection', ['sqlsrv'])->getpDo();
            $statement = $connection->prepare($sql);
            $statement->execute([':id' => $id]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            $user = null;
            if ($result) {
                $user['firstname'] =  $result['Firstname'];
                $user['lastname'] =  $result['Lastname'];
                $user['mobile'] =  $result['phone'];
                $user['email'] =  $result['email'];
                $user['gender'] =  $result['Sex'];
                $user['dob'] =  $result['Dob'];
                $user['race'] =  $result['Race'];
                $user['ethnicity'] =  $result['Ethnicity'];
                $user['ssn'] =  $result['Ssn'];
                $user['ID'] =  $result['ID'];
            }

            return response()->json([
                'success' => false,
                'data' => $user ?: []
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }

    public function updatePatient(Request $request, $id): JsonResponse
    {
        try {
            $connection = app('pdo_connection')->getPdo();

            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $dob = $request->get('dob');
            $email = $request->get('email');
            $mobile = $request->get('mobile');
            $gender = $request->get('sex');
            $race = $request->get('race');
            $ethnicity = $request->get('ethnicity');
            $zip = $request->get('zip');
            $ssn = $request->get('ssn');
            $middlename = $request->get('middlename');
            $county = $request->get('county');
            $address1 = $request->get('address1');
            $city = $request->get('city');
            $state = $request->get('state');
            $suffix = $request->get('suffix');
            $notification = $request->get('notification');

            $errors = app(UtilityService::class)->validateRequest(Validator::make($request->all(), [
                'firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required|date:m/d/Y',
                'sex' => 'required',
                'zip' => 'required',
                'mobile' => [
                    'required',
                    function($attribute, $value, $fail) use (& $id, & $email) {
                        if (!app('utility')->clientExistingEntityVerifier($id, $value, 'phone', '[dbo].[Client]')) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ],
                'email' => [
                    'required',
                    function($attribute, $value, $fail) use (& $id) {
                        if (!app('utility')->clientExistingEntityVerifier($id, $value, 'email', '[dbo].[Client]')) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ]
            ]));

            if (count($errors) > 0) {
                return response()->json([
                    'success' => false,
                    'data' => $errors
                ], 400);
            }

            $statement = $connection->prepare(
                "EXEC spm_update_client_detail
                  @client_id = :client_id,
                  @lastname = :lastname,
                  @firstname = :firstname,
                  @middlename = :middlename,
                  @dob = :dob,
                  @sex = :sex,
                  @ssn = :ssn,
                  @address1 = :address1,
                  @city = :city,
                  @state = :state,
                  @zip = :zip,
                  @county = :county,
                  @race = :race,
                  @ethnicity = :ethnicity,
                  @email = :email,
                  @phone = :phone,
                  @suffix = :suffix
              "
            );
            $statement->execute([
                ':client_id' => $id,
                ':lastname' => $lastname,
                ':firstname' => $firstname,
                ':middlename' => $middlename,
                ':dob' => $dob,
                ':sex' => $gender,
                ':ssn' => $ssn,
                ':address1' => $address1,
                ':city' => $city,
                ':state' => $state,
                ':zip' => $zip,
                ':county' => $county,
                ':race' => $race,
                ':ethnicity' => $ethnicity,
                ':email' => $email,
                ':phone' => $mobile,
                ':suffix' => $suffix,
                ':sms_received_notification' => $notification ? 1 : 0
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Successfully Updated'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
