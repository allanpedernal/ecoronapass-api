<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if (Auth::user()->hasRole('provider')) {
            // return view('provider.index');
            return redirect()->route('provider.recipient');
        } else if (Auth::user()->hasRole('patient')) {
            $patient_id = Auth::user()->ID;
            return view('patient.profile', compact('patient_id'));
        }else if (Auth::user()->hasRole('checkin')) {
            return view('checkin.checkinlist');
        } else if (Auth::user()->hasRole('checkout')) {
            return view('checkout.checkoutlist');
        } else {
            return view('appointment.index');
            // return view('admin.index');
        }
    }

    public function showUserTypeCheckerPage(Request $request){
        return view('auth.user-type-checker');
    }

    public function showClientRegisterRedirectPage(Request $request){
        $message = isset($request->message) ? $request->message : "";
        $alert = isset($request->alert) ? $request->alert : "";
        return view('auth.client-register-redirect', compact('message', $message, 'alert', $alert));
    }
}
