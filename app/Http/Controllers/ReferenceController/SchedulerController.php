<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Facility;
use App\Helpers\SmsHelper;
use App\Repositories\SchedulerRepository;
use App\Mail\AppointmentCancelledEmail;
use App\Notifications\ScheduleNotification;
use App\Models\Schedule;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;
use PDO;


class SchedulerController extends Controller
{
    /**
     * @var SchedulerRepository
     */
    private $schedulerRepository;

    /**
     * HomeController constructor.
     * @param SchedulerRepository $schedulerRepository
     */
    public function __construct(
        SchedulerRepository $schedulerRepository
    ) {
        $this->schedulerRepository = $schedulerRepository;
    }

    public function index()
    {
        return view('scheduler.index');
    }

    public function checkinScheduler($pid)
    {
        // $connection = \DB::connection('sqlsrv')->getPdo();
        $patient_id = $pid ? $pid : auth()->user()->ID;
        // $statement = $connection->prepare("
        //     SET NOCOUNT ON; EXEC sp_GetVaccine_Schedule
        //         @PatientID = :patient_id,
        //         @FacilityID = ''
        // ");
        // $statement->bindParam(":patient_id", $patient_id);
        // $statement->execute();
        // $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);
        // $schedules = collect($schedules);
        // if($schedules->where('status', 'pending')->count() > 0){
        //     return redirect()->route('home')->with('warning', 'You still have pending appointments!');
        // }
        return view('scheduler.index', compact('patient_id'));
    }

    public function create()
    {
        return view('scheduler.create');
    }

    public function confirmation ()
    {
        return view('scheduler.confirmation');
    }

    public function scheduleFacility($id)
    {
        $facility = Facility::find($id);
        if ($facility == null){
            return redirect()->route('scheduler.index')->with('error', 'Facility not found!');
        }

        $connection = \DB::connection('sqlsrv')->getPdo();
        $patient_id = auth()->user()->ID;
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_GetVaccine_Schedule
                @PatientID = :patient_id,
                @FacilityID = :facility_id
        ");
        $statement->bindParam(":patient_id", $patient_id);
        $statement->bindParam(":facility_id", $id);
        $statement->execute();
        $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);
        $schedules = collect($schedules);
        if($schedules->where('status', 'pending')->count() > 0){
            return redirect()->route('home')->with('warning', 'You still have pending appointments!');
        }

        $connection = \DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Total_Available
                @facility_id = :facility_id
        ");
        $statement->bindParam(':facility_id', $id);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC)[0];
        if($result["cnt"] <= 0){
            return redirect()->back()->with('error_schedule', 'Unfortunately there are no more available slots at this time, please select the next convenient date for you.');
        }

        //survey checker
        $survey = $this->schedulerRepository->surveyChecker(auth()->user()->ID);
        $pid = auth()->user()->ID;
        if ($survey) {
            return redirect()->route('patient.survey')->with('warning', 'Please take the survey first before you can schedule an appointment.');
        } else {
            return view('scheduler.create', compact('facility', 'pid'));
        }

    }

    public function checkinScheduleFacility($id, $pid)
    {
        $facility = Facility::find($id);
        if ($facility == null){
            return redirect()->route('scheduler.index')->with('error', 'Facility not found!');
        }

        $connection = \DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_GetVaccine_Schedule
                @PatientID = :patient_id,
                @FacilityID = :facility_id
        ");
        $statement->bindParam(":patient_id", $pid);
        $statement->bindParam(":facility_id", $id);
        $statement->execute();
        $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);
        $schedules = collect($schedules);
        if($schedules->where('status', 'pending')->count() > 0){
            return redirect()->route('home')->with('warning', 'You still have pending appointments!');
        }

        $connection = \DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Total_Available
                @facility_id = :facility_id
        ");
        $statement->bindParam(':facility_id', $id);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC)[0];
        if($result["cnt"] <= 0){
            return redirect()->back()->with('error_schedule', 'Unfortunately there are no more available slots at this time, please select another facility.');
        }

        return view('scheduler.create', compact('facility', 'pid'));

        // $survey = $this->schedulerRepository->surveyChecker($pid);
        // if ($survey) {
        //     return redirect()->route('patient.survey')->with('success', 'Please take the survey first before you can schedule an appointment.');
        // } else {
        //     return view('scheduler.create', compact('facility', 'pid'));
        // }

    }

    public function storeSchedule(Request $request, $facilityId)
    {
        $user = $request->user();

        try {

            \DB::connection('sqlsrv')->beginTransaction();

            $connection = \DB::connection('sqlsrv')->getPdo();
            $color = '#0665d2';
            extract($request->all());
            $facility_id = $facilityId;
            $user = User::find($pid);//auth()->user();
            $start_time = strtotime($time);
            $end_time = $start_time + 900;
            $start_time_parsed = date("H:i:s", $start_time);
            $end_time_parsed = date("H:i:s", $end_time);
            $start_date = $date . " " . $start_time_parsed;
            $end_date = $date . " " . $end_time_parsed;
            $patient_id = $user->ID;
            $patient_name = $user->Firstname . " " . $user->Lastname;
            $description = 'Covid 19 Vaccine';
            /*
                @parameter StartDate format "MM/DD/YYYY H:i:s"
                @parameter EndDate format "MM/DD/YYYY H:i:s"
            */

            $statement = $connection->prepare("
                EXEC sp_Add_VaccineSchedule
                    @PatientID = :patient_id
                ,   @PatientName = :patient_name
                ,   @Description = :description
                ,   @Color = :color
                ,   @FacilityID = :facility_id
                ,   @StartDate = :start_date
                ,   @EndDate = :end_date
            ");

            $statement->bindParam(":patient_id", $patient_id);
            $statement->bindParam(":patient_name", $patient_name);
            $statement->bindParam(":description", $description);
            $statement->bindParam(":color", $color);
            $statement->bindParam(":facility_id", $facility_id);
            $statement->bindParam(":start_date", $start_date);
            $statement->bindParam(":end_date", $end_date);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC)[0];
            $confirmation_number = $result['new_seq_num'];
            //dd($result);
            $description = array_key_exists('description', $result) ? $result['description'] : 'N/A';
            $schedule_date = array_key_exists('schedule_date', $result) ? $result['schedule_date'] : 'Error';
            $status = 'success';
            $message = 'Successfully saved schedule!';

            $sql = "sp_GetVaccine_Schedule @PatientID = :PatientID, @FacilityID = :FacilityID";
            $statement = $connection->prepare($sql);
            $statement->execute([
               ':PatientID' => $patient_id,
               ':FacilityID' => $facility_id
            ]);
            $result2 = $statement->fetchAll(PDO::FETCH_ASSOC);
            $followup_schedule = $result2[1]['Start'];

            $facility = Facility::find($facilityId);
            #$break_point = "<br>";
            $br = "Please bring picture ID, proof of employment and wear clothing that allows access to your upper arm for vaccination. Confirmation Number: {$confirmation_number}";
            $smsMessage =  "Hello $user->Firstname. You have a confirmed Appointment to receive the COVID-19 Vaccine on $schedule_date. To review your appointment, please go to https://eastalcovidvaccine.com and sign in. The vaccination location is $facility->full_address. \r\n$br";

            //commit before sending notifications
            \DB::connection('sqlsrv')->commit();

            # Sending Email Notification
            if($schedule_date != 'Error'){
                $user->notify(new ScheduleNotification($schedule_date, $facility, $confirmation_number));
                $sendSMS = SmsHelper::send($user->phone, $smsMessage);
            }
            else{
                return redirect()->route('scheduler.index')->with('error', 'There was a problem confirming your appointment please try again.');
            }

            return view('scheduler.confirmation', compact('facility', 'description', 'schedule_date', 'followup_schedule'))->with('status', 'message');
        } catch (\Exception $e) {
            \DB::connection('sqlsrv')->rollBack();
            $status = 'error';
            $message = $e->getMessage();
            return redirect()->route('scheduler.index')->with($status, $message);
        }
    }

    public function getAllAvailableSchedule(Request $request)
    {
        try {
            $connection = \DB::connection('sqlsrv')->getPdo();
            extract($request->all());
            $start_datetime = date('m/d/Y', strtotime($start_datetime));
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Get_All_Available_Schedule
                    @facility_id = :facility_id
                ,   @start_datetime = :start_datetime
            ");
            $statement->bindParam(":facility_id", $facility_id);
            $statement->bindParam(":start_datetime", $start_datetime);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $key => $res) {
                $result[$key]['start_time'] = date('h:i A', strtotime($res['start_time']));
            }
            $status = 'success';
            $message = 'Loaded available schedules';
            $code = 200;
        } catch (\Exception $e) {
            $result = [];
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'result' => $result
        ], $code);
    }

    public function getProviderList(Request $request)
    {
        try {
            $connection = \DB::connection('sqlsrv')->getPdo();
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Get_ProviderList
            ");
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $status = 'success';
            $message = 'Loaded providers list';
            $code = 200;
        } catch (\Exception $e) {
            $result = [];
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'result' => $result
        ], $code);
    }

    public function getAvailableSchedule(Request $request)
    {
        try {
            $connection = \DB::connection('sqlsrv')->getPdo();
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Get_Available_Schedule
                    @facility_id = :facility_id
                ,   @provider_id = :provider_id  --<< new parameter
                ,   @start_datetime = :start_datetime
            ");
            $statement->bindParam(":facility_id", $facility_id);
            $statement->bindParam(":provider_id", $provider_id);
            $statement->bindParam(":start_datetime", $start_datetime);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $status = 'success';
            $message = "Loaded available schedule for $start_datetime";
            $code = 200;
        } catch (\Exception $e) {
            $result = [];
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }
    }

    public function cancelSchedule(Request $request, $scheduleId)
    {
        try {
            $updated_by = auth()->user()->ID;
            $connection = \DB::connection('sqlsrv')->getPdo();
            $statement = $connection->prepare("
                EXEC sp_UpdateVaccine_ScheduleStatus
                    @ScheduleID = :schedule_id
                ,   @Status = 'declined'
                ,   @StatusType = 'check-in-status'
                ,   @Reason = ''
                ,	@updated_by = :updated_by
            ");
            $statement->bindParam(":schedule_id", $scheduleId);
            $statement->bindParam(":updated_by", $updated_by);
            $statement->execute();
            $status = 'success';
            $message = 'Cancelled appointment successfully.';

            if (isset($scheduleId) && $scheduleId) {
                $schedule = Schedule::findOrFail($scheduleId);
                if(isset($schedule->ClientID) && $schedule->ClientID){
                    $user = User::find($schedule->ClientID);
                    if(isset($user->email) && $user->email){
                        $bcc = env('MAIL_BCC');
                        $bcc = $bcc ? explode(',',$bcc) : [];
                        if ($bcc) {
                            Mail::to($user->email)
                            ->bcc($bcc)
                            ->send(new AppointmentCancelledEmail([
                                'firstname' => $user->Firstname,
                                'schedule'    => date('M d, Y g:i A', strtotime($schedule->Start))
                            ]));
                        } else {
                            Mail::to($user->email)
                            ->send(new AppointmentCancelledEmail([
                                'firstname' => $user->Firstname,
                                'schedule'    => date('M d, Y g:i A', strtotime($schedule->Start))
                            ]));
                        }
                    }
                }
            }

            return redirect()->back()->with($status, $message);
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
            return redirect()->back()->with($status, $message);
        }
    }

    public function getScheduleByUserId($id)
    {
        try {
            $facility_id = NULL;
            $connection = \DB::connection('sqlsrv')->getPdo();
            // $patient_id = auth()->user()->ID;
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_GetVaccine_Schedule
                    @PatientID = :patient_id,
                    @FacilityID = :facility_id
            ");
            $statement->bindParam(":patient_id", $id);
            $statement->bindParam(":facility_id", $facility_id);
            $statement->execute();
            $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);
            $schedules = collect($schedules);
            $status = 'success';
            $message = "Loaded schedules";
            $code = 200;
        } catch (\Exception $e) {
            $schedules = [];
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'schedules' => $schedules
        ], $code);
    }

    public function getAvailableDates($facilityId)
    {
        try {
            $connection = \DB::connection('sqlsrv')->getPdo();
            // $patient_id = auth()->user()->ID;
            $date_today = Carbon::now()->format("m/d/Y");

            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Get_Total_Available_perDate
                    @facility_id = :facility_id
            ");
            $statement->bindParam(":facility_id", $facilityId);
            $statement->execute();
            $schedules = $statement->fetchAll(PDO::FETCH_ASSOC);

            $schedules = array_filter($schedules, function($v) {
                return !(strtotime($v['schedule_date']) < strtotime(date('m/d/Y')));
            });

            $formatted_schedules = [];
            foreach ($schedules as $schedule) {
                $formatted_schedules[] = [
                    'date' => $schedule['schedule_date'],
                    'formatted_date' => date('m/d/Y',strtotime($schedule['schedule_date']))
                ];
            }
            $schedules = $formatted_schedules;

            $status = 'success';
            $message = "Loaded schedules";
            $code = 200;
        } catch (\Exception $e) {
            $schedules = [];
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'schedules' => $schedules
        ], $code);
    }
}
