<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CheckoutRepository;

class CheckoutApiController extends Controller
{

     /**
     * @var CheckoutRepository
     */
    private $checkoutRepository;

    /**
     * CheckoutApiController constructor.
     * @param CheckoutRepository $checkoutrRepository
     */
    public function __construct(
        CheckoutRepository $checkoutRepository
    ) {
        $this->checkoutRepository = $checkoutRepository;
    }

    public function patients(Request $request)
    {
        return $this->checkoutRepository->patientList($request->all());
    }

}
