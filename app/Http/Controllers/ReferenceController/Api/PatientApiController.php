<?php

namespace App\Http\Controllers\Api;

use App\Repositories\PatientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class PatientApiController extends Controller
{
    /**
     * @var PatientRepository
     */
    private $patientRepository;

    /**
     * PatientController constructor.
     * @param PatientRepository $patientRepository
     */
    public function __construct(
        PatientRepository $patientRepository
    ) {
        $this->patientRepository = $patientRepository;
    }

    public function getPatientList(Request $request)
    {
        return $this->patientRepository->patientList($request->all());
    }

    public function save(Request $request)
    {
        if ($request->ID) {
            return $this->patientRepository->update($request->all());
        } else {
            return $this->patientRepository->store($request->all());
        }

    }

    public function details($patient_id)
    {
        return $this->patientRepository->details($patient_id);
    }

    public function getPreRegistrationQuestion(Request $request)
    {
        return $this->patientRepository->getPreRegistrationQuestion($request->all());
    }

    public function getFlexFormQuestion($flexForm, $patient_id = null)
    {
        $patient_id = $patient_id ?? Auth::user()->ID;
        return $this->patientRepository->getFlexFormQuestion($flexForm, $patient_id);
    }

    public function saveFlexFormQuestion(Request $request)
    {

        $parts = parse_url($request->headers->get('referer'));

        if(isset($parts['query']))
            parse_str($parts['query'], $query);
        $failicty_id = $query['facilityId'] ?? null;

        try {
            $request->validate([
                'answer' => 'required',
            ]);

            $data = $request->all();
            $data['patient_id'] = $request->patient_id ?? Auth::user()->ID;

            //update status only for checkin role
            if(auth()->user()->hasRole('checkin'))
                $this->patientRepository->updateSurvey(['status' => $data['status']], $data['survey'], $data['patient_id']);

            return $this->patientRepository->saveFlexFormQuestion($data, $failicty_id);
        } catch (ValidationException $e) {
            return response()->json([
                'status' =>  'error',
                'message' => 'Please answer all questions.'
            ], 200);
        }
    }

    public function updateSurvey(Request $request, $survey, $patient_id)
    {
        return $this->patientRepository->updateSurvey($request->all(), $survey, $patient_id);
    }


    public function grantConsent(Request $request) {
        $user = $request->user();
        $user = $user->update([
            'consent_given' => true,
        ]);
    }

    public function getclientInfo($patient_id)
    {
        return $this->patientRepository->getclientInfo($patient_id);
    }
}
