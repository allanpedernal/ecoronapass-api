<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VaccineInformation;
use App\Models\Facility;
use DB;

class LotController extends Controller
{
    public function getLots()
    {
    	try {
    		$lots = VaccineInformation::get();
    		$status = 'success';
    		$message = 'Loaded lots list';
    		$code = 200;
    	} catch (\Exception $e) {
    		$lots = [];
    		$status = 'error';
    		$message = $e->getMessage();
    		$code = 500;
    	}

    	return response()->json([
    		'status' => $status,
    		'message' => $message,
    		'lots' => $lots
    	], $code);
    }

    public function getFacilities()
    {
    	try {
    		$facilities = Facility::get();
    		$status = 'success';
    		$message = 'Loaded facilities list';
    		$code = 200;
    	} catch (\Exception $e) {
    		$facilities = [];
    		$status = 'error';
    		$message = $e->getMessage();
    		$code = 500;
    	}

    	return response()->json([
    		'status' => $status,
    		'message' => $message,
    		'facilities' => $facilities
    	], $code);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        //get inventory_location details
        $inventory_details = (array) DB::table('vaccine_inventory_location')->find($request->inventory_location_id);
        $inventory_details['inventory_location'] = $inventory_details['location_name'];
        unset($inventory_details['id']);
        unset($inventory_details['location_name']);
        //combine array
        $vaccine_information_data = array_merge($data, $inventory_details);

        try {
            DB::beginTransaction();
            VaccineInformation::create($vaccine_information_data);
            DB::commit();
            $status = 'success';
            $message = 'Successfully added lot';
            $code = 200;
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }

    	return response()->json([
    		'status' => $status,
    		'message' => $message,
    	], $code);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        //get inventory_location details
        if ($request->inventory_location_id) {
            $inventory_details = (array) DB::table('vaccine_inventory_location')->find($request->inventory_location_id);
            $inventory_details['inventory_location'] = $inventory_details['location_name'];
            unset($inventory_details['id']);
            unset($inventory_details['location_name']);
            //combine array
            $vaccine_information_data = array_merge($data, $inventory_details);
        } else {
            $vaccine_information_data = $data;
        }

        try {
            DB::beginTransaction();
            $lot = VaccineInformation::find($id);
            $lot->update($vaccine_information_data);
            DB::commit();
            $status = 'success';
            $message = 'Successfully updated lot';
            $code = 200;
        } catch (\Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
            $code = 500;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }
}
