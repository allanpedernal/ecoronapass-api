<?php

namespace App\Http\Controllers\Api;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use PDO;

class DemographicsController extends Controller
{

    protected $pdo_connection;

    public function __construct()
    {
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
    }

    public function getList(Request $request)
    {
        try {
            $statement = $this->pdo_connection->prepare(
                "SELECT * FROM vw_eamc_demographics"
            );
            $statement->execute();
            $data = $statement->fetchAll(PDO::FETCH_ASSOC);

            $status = 'success';
            $message = 'Success!';
        } catch (Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        return response()->json([
            'data' => $data ?? [],
            'status' =>  $status,
            'message' => $message
        ]);
    }


}


