<?php

namespace App\Http\Controllers\Api;

use App\Repositories\HomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeApiController extends Controller
{
    /**
     * @var HomeRepository
     */
    private $homeRepository;

    /**
     * HomeController constructor.
     * @param HomeRepository $homeRepository
     */
    public function __construct(
        HomeRepository $homeRepository
    ) {
        $this->homeRepository = $homeRepository;
    }

    public function getStates(Request $request)  {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();


            $statement = $connection->prepare("
                EXEC sp_GetStates
            ");
            
            $statement->execute();
            $states = $statement->fetchAll(\PDO::FETCH_ASSOC);

            # return response
            return \Response::json([
                'success'=>true,
                'states'=>$states
            ],200);
        }
        catch(\Exception $e)
        {
            # return response
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }


}
