<?php

namespace App\Http\Controllers\Api;

use App\Repositories\AppointmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailAppointment;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client as Twilio;
use Illuminate\Support\Facades\Mail;

class AppointmentApiController extends Controller
{
    /**
     * @var AppointmentRepository
     */
    private $appointmentRepository;

    /**
     * AppointmentController constructor.
     * @param AppointmentRepository $appointmentRepository
     */
    public function __construct(
        AppointmentRepository $appointmentRepository
    ) {
        $this->appointmentRepository = $appointmentRepository;
    }

    public function events(Request $request)
    {
        return $this->appointmentRepository->getEvents($request->all());
    }

    public function save(Request $request)
    {
        return $this->appointmentRepository->save($request->all());

    }

    public function getSummary(Request $request)
    {
        return $this->appointmentRepository->summary($request->all());
    }

    public function getEventPatients(Request $request)
    {
        $events = $this->appointmentRepository->getEventPatients($request->start);
        return compact('events');
    }

    public function updateEvent($id, Request $request)
    {
        //validate the payload
        $data = $request->validate([
            'status' => 'required|string',
        ]);

        $event = $this->appointmentRepository->findOrFail($id);
        $this->appointmentRepository->update($event, $data);

        //send sms
        $patient_contact = DB::table('ClientContacts')->where('ClientID', $event->ClientID)->where('Type', 'Mobile')->first();
        $patient = DB::table('Client')->where('ID', $event->ClientID)->first();

        if ($data['status'] == 'approved') {
            $textmessage    = "Your vaccine schedule on " . date('F j, Y, g:i a', strtotime($event->Start))  . 'has been approved.';
        } else {
            $textmessage    = "We regret to inform you that your schedule had been cancelled due to limited providers on site. You may set another vaccine schedule.";
        }

        if (env('APP_ENV') == 'production' && $patient_contact && $patient_contact->number) {
            $client = new Twilio(env('TWILIO_ACCOUNT_SID'), env('TWILIO_AUTH_TOKEN'));
            $resp_sms = $client->messages->create($patient_contact->number, [
                'from' => env('TWILIO_NUMBER'),
                'body' => $textmessage,
            ]);
        }

        //send email
        if(isset($patient->email) && $patient->email){
            $bcc = env('MAIL_BCC');
            $bcc = $bcc ? explode(',',$bcc) : [];
            if ($bcc) {
                Mail::to($patient->email)
                ->bcc($bcc)
                ->send(new EmailAppointment([
                    'firstname' => $patient->Firstname,
                    'status'    => $data['status'],
                    'message'   => $textmessage
                ]));
            } else {
                Mail::to($patient->email)
                ->send(new EmailAppointment([
                    'firstname' => $patient->Firstname,
                    'status'    => $data['status'],
                    'message'   => $textmessage
                ]));
            }
        }

        $events = $this->appointmentRepository->getEventPatients($event->Start, $event->End, $event->facility_id);
        return compact('events');
    }

    public function getFacilities()
    {
        $facilities = $this->appointmentRepository->getFacilities();
        return compact('facilities');
    }

    public function liveStatsToday(Request $request)
    {
        return $this->appointmentRepository->liveStatsToday($request->all());
    }

    public function noShow()
    {
        return $this->appointmentRepository->noShow();
    }
}
