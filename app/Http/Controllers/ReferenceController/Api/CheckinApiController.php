<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CheckinRepository;

class CheckinApiController extends Controller
{

     /**
     * @var CheckinRepository
     */
    private $checkinRepository;

    /**
     * CheckinApiController constructor.
     * @param CheckinRepository $checkinrRepository
     */
    public function __construct(
        CheckinRepository $checkinRepository
    ) {
        $this->checkinRepository = $checkinRepository;
    }

    public function patients(Request $request)
    {
        return $this->checkinRepository->patientList($request->all());
    }

    public function updateStatus(Request $request)
    {
        return $this->checkinRepository->updateStatus($request->all());
    }

}
