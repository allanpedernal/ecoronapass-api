<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProviderRepository;

class ProviderApiController extends Controller
{

     /**
     * @var ProviderRepository
     */
    private $providerRepository;

    /**
     * ProviderApiController constructor.
     * @param ProviderRepository $providerRepository
     */
    public function __construct(
        ProviderRepository $providerRepository
    ) {
        $this->providerRepository = $providerRepository;
    }

    public function dashboard()
    {
        return $this->providerRepository->dashboard();
    }

    public function checkNpi(Request $request)  {
        $this->providerRepository->checkNpi($request->all());
    }

    public function recipient(Request $request)
    {
        return $this->providerRepository->recipient($request->all());
    }

    public function getInventoryLocationList()
    {
        return $this->providerRepository->getInventoryLocationList();
    }

    public function getLotNumberList()
    {
        return $this->providerRepository->getLotNumberList();
    }

    public function getVaccineInformation(Request $request)
    {
        return $this->providerRepository->getVaccineInformation($request->all());
    }

}
