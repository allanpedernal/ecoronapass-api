<?php

namespace App\Http\Controllers\Api;

use App\Models\Facility;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vaccination;
use App\Models\VaccineInformation;
use DB;
use App\Services\Hl7Service;

class VaccineApiController extends Controller
{

    public function __construct() {

    }

    public function list(Request $request)
    {
        $vaccines = VaccineInformation::where('client_id', $request->user_id)->get();

        return response()->json([
                'data' => $vaccines
            ]
        );
    }

    public function saveInformation(Request $request)
    {
        $patient_id = $request->patient_id;
        $vaccine_date = $request->vaccine_date;
        $connection = DB::connection('sqlsrv')->getPdo();

        //check if vaccination already exists
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_vaccinations_count
                @patient_id = :patient_id,
                @vaccine_date = :vaccine_date
        ");
        $statement->bindParam(":patient_id", $patient_id);
        $statement->bindParam(":vaccine_date", $vaccine_date);
        $statement->execute();
        $count = $statement->fetch(\PDO::FETCH_ASSOC);



        if (isset($count['cnt']) && $count['cnt'] > 0) {
            return response()->json([
                'status' =>  'warning',
                'message' => 'This vaccination has already been submitted.'
            ]);
        }

        //check if vaccination already exists
        if (Vaccination::where('patientid', $request->patient_id)->whereDate('vaccine_date', $request->vaccine_date)->exists()) {
            return response()->json([
                'status' =>  'warning',
                'message' => 'This vaccination has already been submitted.'
            ]);
        }

        try {
            $sql = "SET NOCOUNT ON; EXEC sp_add_vaccinations";
            $sql .= " @patient_id = :patient_id";
            $sql .= " ,@facility_id = :facility_id";
            $sql .= " ,@vaccine_date = :vaccine_date";
            $sql .= " ,@inventory_location = :inventory_location";
            $sql .= " ,@lot_no = :lot_no";
            $sql .= " ,@mvx = :mvx";
            $sql .= " ,@ndc = :ndc";
            $sql .= " ,@vaccine_exp_date = :vaccine_exp_date";
            $sql .= " ,@dosage = :dosage";
            $sql .= " ,@vaccine_information_id  = :vaccine_information_id";
            $sql .= " ,@vaccine_admin_site   = :vaccine_admin_site";
            $sql .= " ,@vaccine_route_admin    = :vaccine_route_admin";
            $sql .= " ,@vaccine_series_complete    = :vaccine_series_complete";
            $sql .= " ,@vaccinator_id    = :vaccinator_id";
            $sql .= " ,@vaccinator    = :vaccinator";
            $sql .= " ,@entering_organization    = :entering_organization";
            $sql .= " ,@cvx_code = :cvx_code";
            $sql .= " ,@cvx  = :cvx";
            $sql .= " ,@mvx_code  = :mvx_code ";
            $sql .= " ,@ScheduleID  = :schedule_id";

            $statement = $connection->prepare($sql);
            $statement->execute([
                ':patient_id' => $request->patient_id,
                ':facility_id' => $request->facility_id,
                ':vaccine_date' => $request->vaccine_date . " " . date('H:i:s'),
                ':inventory_location' => $request->inventory_location,
                ':lot_no' => $request->lot_no,
                ':mvx' => $request->mvx,
                ':ndc' => $request->ndc,
                ':vaccine_exp_date' => $request->vaccine_exp_date,
                ':dosage' => $request->dosage,
                ':vaccine_information_id' => $request->info_id,
                ':vaccine_admin_site' => $request->vaccine_admin_site,
                ':vaccine_route_admin' => $request->vaccine_route_admin,
                ':vaccine_series_complete' => $request->vaccine_series_complete,
                ':vaccinator_id' => auth()->user()->ID,
                ':vaccinator' => auth()->user()->Firstname.' '.auth()->user()->Lastname,
                ':entering_organization' => $request->entering_organization,
                ':cvx_code' => $request->cvx_code,
                ':cvx' => $request->cvx,
                ':mvx_code' => $request->mvx_code,
                ':schedule_id' => $request->schedule_id
            ]);

            $vaccination_id = $statement->fetch(\PDO::FETCH_ASSOC)['id'] ?? null;
            $vaccination = Vaccination::find($vaccination_id);

            if ($vaccination) {
                //send imprint
                app(Hl7Service::class)->send($vaccination);
                $status = 'success';
                $message = 'Vaccine information successfully saved.';
            }


        } catch (Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        return response()->json([
            'vaccines' => [],
            'status' =>  $status,
            'message' => $message
        ]);
    }

    public function vaccineInformationList(Request $request)
    {
        $vaccines = $this->getVaccinesInformation($request->user_id);

        return response()->json([
                'data' => $vaccines
            ], 200
        );
    }

    public function getVaccinesInformation($patient_id)
    {
        // return DB::table('Vaccine_Information')
        // ->select('Vaccine_Information.*', 'Facility.responsible_organization', 'Facility.facility_name', 'Facility.facility_type', 'Facility.full_address')
        // ->join('Facility', 'Facility.id', '=', 'Vaccine_Information.facility_id')
        // ->whereNull('Vaccine_Information.deleted_at')/*
        // ->where('Vaccine_Information.patient_id',  $patient_id)*/->get();

        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare(
        "EXEC sp_Get_VaccinationList @patient_id = :p_id"
        );

        $statement->bindParam(':p_id', $patient_id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function vaccineInformationDetails(Request $request)
    {
        $vaccine = VaccineInformation::find($request->id);
        $facility = Facility::find($vaccine->facility_id);

        return response()->json([
                'vaccine' => $vaccine,
                'facility' => $facility
            ], 200
        );
    }

    public function vaccineInformationDelete(Request $request)
    {
        try {
            $id = $request->id;
            $vaccine = VaccineInformation::find($id);
            $patient_id = $vaccine->patient_id;
            $vaccine->delete();
            $vaccines = VaccineInformation::where('patient_id', $patient_id)->get();
            return response()->json([
                'vaccines' => $vaccines,
                'status' => 'success',
                'message' => 'Vaccine history successfully deleted.'
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ], 200
            );
        }
    }

    public function getDropdown()
    {
        # set connection
        $connection = DB::connection('sqlsrv')->getPdo();

        //Facility Types
        // $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_Facility_Type");
        // $statement->execute();
        // $facility_types = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //Dose
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_Dose");
        $statement->execute();
        $dose = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //Extract Type
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_Extract_Type");
        $statement->execute();
        $extract_type = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //mvx
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_MVX");
        $statement->execute();
        $mvx = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //responsible_organization
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_Responsible_Organization");
        $statement->execute();
        $responsible_organization = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //race
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_GetRace");
        $statement->execute();
        $race = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //vaccine_admin_site
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_Vaccine_Admin_Site");
        $statement->execute();
        $vaccine_admin_site = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //vaccine_route_admin
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_Get_Vaccine_Route_Admin");
        $statement->execute();
        $vaccine_route_admin = $statement->fetchAll(\PDO::FETCH_ASSOC);

        //facilities
        $facilities = DB::table('Facility')->get();


        # return response
        return response()->json([
            'success'                   => true,
            // 'facility_types'            => $facility_types,
            'dose'                      => $dose,
            'extract_type'              => $extract_type,
            'mvx'                       => $mvx,
            'vaccine_admin_site'        => $vaccine_admin_site,
            'vaccine_route_admin'       => $vaccine_route_admin,
            'responsible_organization'  => $responsible_organization,
            'facilities'                => $facilities
        ],200);
    }

}
