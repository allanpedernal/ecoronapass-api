<?php

namespace App\Http\Controllers\Api;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vaccination;
use App\Services\Hl7Service;
use App\User;
use Illuminate\Support\Facades\DB;
use PDO;
use App\Exports\PatientsReport;
use App\Exports\SchedulesReport;
use Excel;

class ReportApiController extends Controller
{

    protected $pdo_connection;

    public function __construct()
    {
        $this->pdo_connection = app('sql_srv_connection')->getPdo();
    }

    public function getLoincDatatable()
    {
        $statement = $this->pdo_connection->prepare(
        "exec sp_Medical_Result_Report"
        );
        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return response()->json([
                'data' => $data,
            ], 200
        );
    }

    function getDemographicReport(): JsonResponse
    {
        try {
            $statement = $this->pdo_connection->prepare(
                "EXEC sp_Report_Demographics_Vaccinations"
            );
            $statement->execute();
            $data = $statement->fetchAll(PDO::FETCH_ASSOC);
            return response()->json([
                'status' => 'success',
                'data' => $data,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'info' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ]);
        }
    }

    function getPatientDetailReports(Request $request): JsonResponse
    {
        $firstlastname = $request->firstlastname == 'true' ? true : false;
        $nulldob = $request->nulldob == 'true' ? true : false;
        $ids = [];

        try {

            $data = User::whereHas('roles', function($q) {
                $q->where('name', 'patient');
            });

            if(!$firstlastname && !$nulldob) {
                $ids = array_merge($ids, $data->pluck('id')->toArray());
            }

            if($firstlastname) {
                $zIds = $data->whereRaw('len(Firstname) <= ?', 1)->whereRaw('len(Lastname) <= ?', 1)->pluck('id')->toArray();
                $ids = array_merge($ids, $zIds);
            }

            if($nulldob) {
                $xIds = $data->whereNull('Dob')->pluck('id')->toArray();
                $ids = array_merge($ids, $xIds);
            }

            $data = User::whereIn('ID', $ids)->get();

            return response()->json([
                'status' => 'success',
                'data' => $data,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'info' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ]);
        }
    }

    public function immprintReports()
    {
        $connection = DB::connection('sqlsrv')->getPdo();
        $statement = $connection->prepare("SET NOCOUNT ON; EXEC sp_immprint_rejection");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function resendImmprints()
    {
        $rejectedImmPrints = $this->immprintReports();
        if ($rejectedImmPrints && count($rejectedImmPrints) > 0) {
            foreach($rejectedImmPrints as $immprint) {
                $vaccination = Vaccination::find($immprint['vaccinations_id']);
                if ($vaccination) {
                    app(Hl7Service::class)->send($vaccination);
                }
            }
            return response()->json([
                'status' => 'success',
                'message' => 'Rejected ImmPrints resubmitted.',
            ]);
        } else {
            return response()->json([
                'status' => 'warning.',
                'message' => 'No Rejected Immprints found.',
            ]);
        }

    }

    public function getPatientsReport(Request $request){
        $search             = $request['search'];
        $columns            = $request['columns'];
        $start              = $request['start'];
        $length             = $request['length'];
        $order              = $request['order'];
        $draw               = $request['draw'];
        $order_by_column    = $order[0]['column'];
        $order_by_dir       = $order[0]['dir'];
        $find               = $search['value'] ?? '';
        $from = $request['from_date'];
        $to = $request['to_date'];

        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare("
            EXEC sp_PatientList_UnderAge16
                @p_start = :p_start
            ,   @p_end = :p_end
            ,   @p_sort = :p_sort
            ,   @p_offset = :p_offset
            ,   @p_limit = :p_limit
            ,   @p_find = :p_find
        ");

        $order_inx = 0; //default to lastname
        switch ($order_by_column) {
            case 1: //firstname
                $order_inx = 1;
                break;
            default: //lastname
                $order_inx = 0;
        }

        $sort_value = $order_inx . ' ' . $order_by_dir;
        $statement->bindParam(':p_start', $from);
        $statement->bindParam(':p_end', $to);
        $statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $start);
        $statement->bindParam(':p_limit', $length);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $recordsTotal = $data ? $data[0]['cnt'] : 0;
        $recordsFiltered = $recordsTotal;

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function exportPatientsReport(Request $request){
        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare("
            EXEC sp_PatientList_UnderAge16
                @p_start = :p_start
            ,   @p_end = :p_end
            ,   @p_sort = :p_sort
            ,   @p_offset = :p_offset
            ,   @p_limit = :p_limit
            ,   @p_find = :p_find
        ");

        $sort_value = 0;
        $offset = 0;
        $p_limit = 1000;
        $find = '';
        $from = $request['from_date'];
        $to = $request['to_date'];
        $statement->bindParam(':p_start', $from);
        $statement->bindParam(':p_end', $to);
        $statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $offset);
        $statement->bindParam(':p_limit', $p_limit);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $res = [];

        foreach ($data as $key => $value) {
            array_push($res,[
                "Lastname" => $value["Lastname"],
                "Firstname" => $value["Firstname"],
                "dob" => $value["dob"],
                "phone" => $value["phone"],
                "appointment1" => $value["appointment1"],
                "appointment2" => $value["appointment2"],
                "email" => $value["email"],
                "vaccine_date" => $value["vaccine_date"]
            ]);
        }

        $export = new PatientsReport($res);
        $request->from_date = str_replace("/","-",$request->from_date);
        $request->to_date = str_replace("/","-",$request->to_date);
        return Excel::download($export, 'patient_under_age_16_report'.$request->from_date.'_'.$request->to_date.'.xlsx');
    }

    public function getSchedulesReport(Request $request){
        $search             = $request['search'];
        $columns            = $request['columns'];
        $start              = $request['start'];
        $length             = $request['length'];
        $order              = $request['order'];
        $draw               = $request['draw'];
        $order_by_column    = $order[0]['column'];
        $order_by_dir       = $order[0]['dir'];
        $find               = $search['value'] ?? '';
        $from = $request['from_date'];
        $to = $request['to_date'];

        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare("
            EXEC sp_PatientList_NoAgeLimit
                @p_start = :p_start
            ,   @p_end = :p_end
            ,   @p_sort = :p_sort
            ,   @p_offset = :p_offset
            ,   @p_limit = :p_limit
            ,   @p_find = :p_find
        ");

        $order_inx = 0; //default to lastname
        switch ($order_by_column) {
            case 1: //firstname
                $order_inx = 1;
                break;
            default: //lastname
                $order_inx = 0;
        }

        $sort_value = $order_inx . ' ' . $order_by_dir;
        $statement->bindParam(':p_start', $from);
        $statement->bindParam(':p_end', $to);
        $statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $start);
        $statement->bindParam(':p_limit', $length);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $res = [];

        foreach ($data as $key => $value) {
            if($value["appointment2"] == null && strpos($value["vaccine_series_complete"], '2nd Dose') !== false){
                $value["appointment1"] = null;
                $value["appointment2"] = $value["appointment1"];
            }
            array_push($res,[
                "Lastname" => $value["Lastname"],
                "Firstname" => $value["Firstname"],
                "dob" => $value["dob"],
                "phone" => $value["phone"],
                "appointment1" => $value["appointment1"],
                "appointment2" => $value["appointment2"],
                "email" => $value["email"],
                "vaccine_date" => $value["vaccine_date"]
            ]);
        }

        $recordsTotal = $data ? $data[0]['cnt'] : 0;
        $data = $res;
        $recordsFiltered = $recordsTotal;

        return compact('draw', 'recordsTotal', 'recordsFiltered', 'data', 'columns', 'start', 'length', 'search', 'order');
    }

    public function exportSchedulesReport(Request $request){
        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare("
            EXEC sp_PatientList_NoAgeLimit
                @p_start = :p_start
            ,   @p_end = :p_end
            ,   @p_sort = :p_sort
            ,   @p_offset = :p_offset
            ,   @p_limit = :p_limit
            ,   @p_find = :p_find
        ");

        $sort_value = 0;
        $offset = 0;
        $p_limit = 1000;
        $find = '';
        $from = $request['from_date'];
        $to = $request['to_date'];
        $statement->bindParam(':p_start', $from);
        $statement->bindParam(':p_end', $to);
        $statement->bindParam(':p_sort', $sort_value);
        $statement->bindParam(':p_offset', $offset);
        $statement->bindParam(':p_limit', $p_limit);
        $statement->bindParam(':p_find', $find);
        $statement->execute();
        $data = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $res = [];

        foreach ($data as $key => $value) {
            if($value["appointment2"] == null && strpos($value["vaccine_series_complete"], '2nd Dose') !== false){
                $value["appointment1"] = null;
                $value["appointment2"] = $value["appointment1"];
            }
            array_push($res,[
                "Lastname" => $value["Lastname"],
                "Firstname" => $value["Firstname"],
                "dob" => $value["dob"],
                "phone" => $value["phone"],
                "appointment1" => $value["appointment1"],
                "appointment2" => $value["appointment2"],
                "email" => $value["email"],
                "vaccine_date" => $value["vaccine_date"]
            ]);
        }

        $export = new SchedulesReport($res);
        $request->from_date = str_replace("/","-",$request->from_date);
        $request->to_date = str_replace("/","-",$request->to_date);
        return Excel::download($export, 'schedule_report'.$request->from_date.'_'.$request->to_date.'.xlsx');
    }
}


