<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExternalApiController extends Controller
{

    public function getTotalVaccines(Request $request)
    {

        $year_month = $request->year_month ? $request->year_month : '';
        $connection = $connection = app('sql_srv_connection')->getPdo();
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Vaccine_Count
            @year_month = :year_month,
            @dose = :dose
        ");
        $dose = 1;
        $statement->bindParam(':year_month', $year_month);
        $statement->bindParam(':dose', $dose);
        $statement->execute();
        $first_dose = $statement->fetch(\PDO::FETCH_ASSOC);

        //second dose
        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Get_Vaccine_Count
            @year_month = :year_month,
            @dose = :dose
        ");
        $dose = 2;
        $statement->bindParam(':year_month', $year_month);
        $statement->bindParam(':dose', $dose);
        $statement->execute();
        $second_dose = $statement->fetch(\PDO::FETCH_ASSOC);

        return response()->json([
            'first_dose' => floatval($first_dose['vaccinated']) ?? 0,
            'second_dose' => floatval($second_dose['vaccinated']) ?? 0,
        ],200);
    }
}


