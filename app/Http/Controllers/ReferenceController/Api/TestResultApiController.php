<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;

class TestResultApiController extends Controller
{

    public function __construct() {

    }

    public function getTestList($patient_id)
    {
        return DB::table('patient_test_results')
        ->select('patient_test_results.*', 'Client.Lastname', 'Client.Firstname', 'Client.Middlename')
        ->join('Client', 'Client.ID', '=', 'patient_test_results.patient_id')
        ->where('patient_test_results.patient_id', $patient_id)->get();
    }

    public function getManufacturers(Request $request)  {
        try
        {
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();

            # extract all
            extract($request->all());

            # get care team
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Manufacturer_List
            ");

            $statement->execute();
            $encounters = $statement->fetchAll(\PDO::FETCH_ASSOC);

            # return response
            return \Response::json([
                'success'=>true,
                'manufacturers'=>$encounters
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    public function getModels(Request $request)  {
        try
        {
            $manufacturer = $request->manufacturer;
            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();
            # get care team
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Model_List
                @Manufacturer = :manufacturer
            ");

            $statement->bindParam(':manufacturer', $manufacturer);
            $statement->execute();
            $models = $statement->fetchAll(\PDO::FETCH_ASSOC);

            # return response
            return \Response::json([
                'success'   =>true,
                'models'    =>$models
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    public function getSpecimen(Request $request)  {
        try
        {
            $manufacturer = $request->manufacturer;
            $model = $request->model;

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();
            # speciment
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_VendorSpecimen_List
                @Manufacturer = :manufacturer,
                @Model = :model
            ");

            $statement->bindParam(':manufacturer', $manufacturer);
            $statement->bindParam(':model', $model);
            $statement->execute();
            $specimens = $statement->fetchAll(\PDO::FETCH_ASSOC);

            # return response
            return \Response::json([
                'success'   =>true,
                'specimens'    =>$specimens
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    public function getResult(Request $request)  {
        try
        {
            $manufacturer = $request->manufacturer;
            $model = $request->model;
            $specimen = $request->specimen;
            $analyte = $request->analyte;

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();
            # result
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_VendorResult_List
                @Manufacturer = :manufacturer,
                @Model = :model,
                @VendorSpecimen = :specimen,
                @vendor_analyte_name = :analyte
            ");

            $statement->bindParam(':manufacturer', $manufacturer);
            $statement->bindParam(':model', $model);
            $statement->bindParam(':specimen', $specimen);
            $statement->bindParam(':analyte', $analyte);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            # return response
            return \Response::json([
                'success'   =>true,
                'results'    =>$results
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    public function getAnalyte(Request $request)  {
        try
        {
            $manufacturer = $request->manufacturer;
            $model = $request->model;
            $specimen = $request->specimen;

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();
            # analyte
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Vendor_Analyte
                @Manufacturer = :manufacturer,
                @Model = :model,
                @VendorSpecimen = :specimen
            ");

            $statement->bindParam(':manufacturer', $manufacturer);
            $statement->bindParam(':model', $model);
            $statement->bindParam(':specimen', $specimen);
            $statement->execute();
            $analytes = $statement->fetchAll(\PDO::FETCH_ASSOC);

            # return response
            return \Response::json([
                'success'   =>true,
                'analytes'    =>$analytes
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    public function getLoincCode(Request $request)  {
        try
        {
            $manufacturer = $request->manufacturer;
            $model = $request->model;
            $specimen = $request->specimen;
            $result = $request->result;
            $analyte = $request->analyte;

            # set connection
            $connection = \DB::connection('sqlsrv')->getPdo();
            # loinc
            $statement = $connection->prepare("
                SET NOCOUNT ON; EXEC sp_Vendor_Loinc_Info
                @Manufacturer = :manufacturer,
                @Model = :model,
                @VendorSpecimen = :specimen,
                @vendor_result_text = :result,
                @analyte_name = :analyte
            ");

            $statement->bindParam(':manufacturer', $manufacturer);
            $statement->bindParam(':model', $model);
            $statement->bindParam(':specimen', $specimen);
            $statement->bindParam(':result', $result);
            $statement->bindParam(':analyte', $analyte);
            $statement->execute();
            $loinc_code = $statement->fetch(\PDO::FETCH_ASSOC);


            # return response
            return \Response::json([
                'success'    => true,
                'loinc_code' => $loinc_code
            ],200);
        }
        catch(\Exception $e)
        {
            return \Response::json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    public function getTestResults(Request $request)  {

        $orders = $this->getTestList($request->user_id);

        return response()->json([
                'data' => $orders
            ], 200
        );
    }

    public function saveTestResult(Request $request)
    {
        try {
            DB::beginTransaction();
            $test_results = $request->all();
            $test_results['provider_id'] = Auth::user()->ID;
            $test_results['created_at'] = now();
            DB::table('patient_test_results')->insert($test_results);
            DB::commit();

            $status = 'success';
            $message = 'Save successful.';

        } catch (\Exception $e) {
            DB::rollBack();
            $status = 'error';
            $message = $e->getMessage();
        }


        $test_results = $this->getTestList($request->patient_id);

        return response()->json([
            'data' => $test_results,
            'status' =>  $status,
            'message' => $message
        ], 200);
    }

    public function updateTestResult(Request $request)
    {
        try {
            $result_date = $request->result_date;
            $expiration_date = date('Y-m-d', strtotime('+1 year', strtotime($result_date)));
            $data = [
                'test_result'       => $request->test_result,
                'result_date'       => $result_date,
                'active_date'       => $result_date,
                'expiration_date'   => $expiration_date
            ];

            DB::beginTransaction();
            DB::table('patient_test_results')->where('id', $request->id)->update($data);
            DB::commit();

            $status = 'success';
            $message = 'Save successful.';

        } catch (\Exception $e) {
            DB::rollBack();
            $status = 'error';
            $message = $e->getMessage();
        }


        $orders = $this->getTestList($request->patient_id);

        return response()->json([
            'orders' => $orders,
            'status' =>  $status,
            'message' => $message
        ], 200);
    }

    public function getTestResultData(Request $request)
    {
        $data = DB::table('patient_test_results')->where('id', $request->id)->first();

         # set connection
         $connection = \DB::connection('sqlsrv')->getPdo();

         # models
         $statement = $connection->prepare("
             SET NOCOUNT ON; EXEC sp_Model_List
             @Manufacturer = :manufacturer
         ");

         $statement->bindParam(':manufacturer', $data->manufacturer);
         $statement->execute();
         $models = $statement->fetchAll(\PDO::FETCH_ASSOC);

         # specimens
         $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_VendorSpecimen_List
            @Manufacturer = :manufacturer,
            @Model = :model
        ");

        $statement->bindParam(':manufacturer', $data->manufacturer);
        $statement->bindParam(':model', $data->test_model);
        $statement->execute();
        $specimens = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_VendorResult_List
            @Manufacturer = :manufacturer,
            @Model = :model,
            @VendorSpecimen = :specimen,
            @vendor_analyte_name = :analyte
        ");

        $statement->bindParam(':manufacturer', $data->manufacturer);
        $statement->bindParam(':model', $data->test_model);
        $statement->bindParam(':specimen', $data->test_kit);
        $statement->bindParam(':analyte', $data->analyte);
        $statement->execute();
        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement = $connection->prepare("
            SET NOCOUNT ON; EXEC sp_Vendor_Analyte
            @Manufacturer = :manufacturer,
            @Model = :model,
            @VendorSpecimen = :specimen
        ");

        $statement->bindParam(':manufacturer', $data->manufacturer);
        $statement->bindParam(':model', $data->test_model);
        $statement->bindParam(':specimen', $data->test_kit);
        $statement->execute();
        $analytes = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return response()->json([
            'data' => $data,
            'models' => $models,
            'specimens' => $specimens,
            'results' => $results,
            'analytes' => $analytes,
            'status' => 'success'
        ], 200);
    }

}
