<?php

namespace App\Http\Controllers;


use App\Mail\RegistrationMail;
use App\Traits\JWTTrait;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthenticateController extends Controller
{
    use JWTTrait;

    public function authToken(Request $request): JsonResponse
    {
        try
        {
            // get errors
            $error = app('utility')->validateRequest(
                Validator::make($request->all(), [
                    'client_key' => 'required',
                    'client_secret' => 'required'
                ])
            );

            // count error
            if (count($error) > 0)
            {
                // return response
                return response()->json([
                    'error' => true,
                    'message' => $error
                ], 400);
            }

            // get client credentials
            $client_key = $request->client_key;
            $client_secret = $request->client_secret;

            // get response
            $response = Http::asForm()->post(url('oauth/token'), [
                'grant_type' => 'client_credentials',
                'client_id' => $client_key,
                'client_secret' => $client_secret,
                'scope' => '',
            ]);

            # return response
            return response()->json(json_decode($response->body(),true),$response->status());
        }
        catch(Exception $e)
        {
            # return response
            return response()->json([
                'success'=>false,
                'message'=>$e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/v1/auth/login",
     *     tags={"Auth"},
     *     description="Authenticate User",
     *     summary="",
     *     @OA\RequestBody(
     *         description="Client Credential",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="username",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"username": "johndoe", "password": "password1"}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="success",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="token",
     *                 type="string"
     *             ),
     *             @OA\Property(
     *                 property="client_id",
     *                 type="string"
     *             ),
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function login(Request $request): JsonResponse
    {
        try {
            $username = $request->get('username');
            $password = $request->get('password');

            $errors = app('utility')->validateRequest(Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required'
            ]));

            if (count($errors) > 0) {
                return response()->json([
                    'success' => false,
                    'data' => $errors
                ], 400);
            }


            if ( $user = app('utility')->attempt(['username' => $username, 'password' => $password]) ) {
                $token = self::jwtEncode($user);

                #check verification
                if (!$user->sms_verified_at) {
                    return response()->json([
                        'success' => false,
                        'data' => 'User not yet verified'
                    ]);
                } else {
                    return response()->json([
                        'success' => true,
                        'data' => [
                            'token' => $token,
                            'client_id' => $user->ID
                        ]
                    ]);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'data' => 'Combination of username and password not exist in our system!'
                ], 400);
            }

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }


    /**
     * @OA\Post(
     *     path="/v1/auth/register",
     *     tags={"Auth"},
     *     description="Register Patient",
     *     summary="",
     *     @OA\RequestBody(
     *         description="Patient Registration",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="first_name",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="last_name",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="middle_name",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="suffix",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="ssn",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="email",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="username",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="mobile",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="dob",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="sex",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="race",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="ethnicity",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="address1",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="city",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="state",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="zip_code",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="county",
     *                      type="string"
     *                   ),
     *                  @OA\Property(
     *                      property="notification",
     *                      type="boolean"
     *                   ),
     *               ),
     *           ),
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="first_name",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="middle_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="suffix",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ssn",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                      property="email",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                      property="mobile",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="dob",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                      property="sex",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="race",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                      property="ethnicity",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="address1",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="city",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="state",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="zip_code",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="county",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="notification",
     *                     type="boolean"
     *                 ),
     *                 example={
     *                      "first_name": "client first_name",
     *                      "last_name": "client last_name",
     *                      "middle_name": "client middle_name",
     *                      "ssn": "client ssn",
     *                      "suffix": "client name suffix",
     *                      "username": "client username",
     *                      "email": "client email",
     *                      "mobile": "client mobile",
     *                      "dob": "client dob ex. 07/24/2021",
     *                      "sex": "client gender ex. M|F|U",
     *                      "race": "client race",
     *                      "ethnicity": "client ethncity",
     *                      "address1": "client street address",
     *                      "city": "client city address",
     *                      "state": "client state address",
     *                      "zip_code": "client zip code address",
     *                      "county": "client county address",
     *                      "notification": "boolean ex. 1|0",
     *                 }
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="success",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Successfully registered!"}
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function register(Request $request): JsonResponse {
        try {
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');
            $middle_name = $request->get('middle_name');
            $ssn = $request->get('ssn');
            $suffix = $request->get('suffix');
            $email = $request->get('email');
            $username = $request->get('username');
            $mobile = $request->get('mobile');
            $notification = $request->get('notification');
            $dob = $request->get('dob');
            $sex = $request->get('sex');
            $race = $request->get('race');
            $ethnicity = $request->get('ethnicity');
            $zip_code = $request->get('zip_code');
            $address1 = $request->get('address1');
            $state = $request->get('state');
            $city = $request->get('city');
            $county = $request->get('county');

            $connection = app('pdo_connection')->getPdo();

            $errors = app('utility')->validateRequest(Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'dob' => 'required|date:m/d/Y',
                'sex' => 'required',
                'ethnicity' => 'required',
                'race' => 'required',
                'address1' => 'required',
                'state' => 'required',
                'city' => 'required',
                'zip_code' => 'required|min:5',
                'mobile' => [
                    'required',
                    function($attribute, $value, $fail) {
                        if (!app('utility')->clientExistingEntityVerifier(null, $value, 'phone', '[dbo].[Client]', true)) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ],
                'email' => [
                    'required',
                    function($attribute, $value, $fail) {
                        if (!app('utility')->clientExistingEntityVerifier(null, $value, 'email', '[dbo].[Client]', true)) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ],
                'username' => [
                    'required',
                    function($attribute, $value, $fail){
                        if (!app('utility')->clientExistingEntityVerifier(null, $value, 'username', '[dbo].[Client]', true)) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ]
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            $password = uniqid('e-corona');

            $user = new User();
            $user->Lastname = $last_name;
            $user->Firstname = $first_name;
            $user->Middlename = $middle_name;
            $user->username = $username;
            $user->email = $email;
            $user->Dob = $dob;
            $user->Photo = 'img/default.png';
            $user->password = Hash::make($password);
            $user->phone = $mobile;
            $user->Race = $race;
            $user->Ethnicity = $ethnicity;
            $user->Zip = $zip_code;
            $user->Sex = $sex;
            $user->City = $city;
            $user->Address1 = $address1;
            $user->State = $state;
            $user->County = $county;
            $user->suffix = $suffix;
            $user->Ssn = $ssn;
            $user->sms_received_notification = $notification ? 1 : 0;

            if ($user->save()) {
                Mail::to($user->email)->send(new RegistrationMail(['user' => $user, 'password' => $password]));

                #send verification code.
                $pin = app('utility')->generateRandomPin(4);
                $message = '[' . env('APP_NAME') . '] To verify your account, please enter this verification pin: ' . $pin;

                if (app('utility')->sendVerificationCode($user->phone, $message, $user)) {
                    #save the code for verification.
                    $sql = "EXEC spm_update_sms_code @client_id = :client_id";
                    $sql .= " ,@sms_verification_code = :sms_verification_code";
                    $statement = $connection->prepare($sql);
                    $statement->execute([
                        ':client_id' => $user->{'ID'},
                        ':sms_verification_code' => $pin
                    ]);
                }

                return response()->json([
                    'status' => true,
                    'message' => 'Successfully registered! We have email your credential in registered email.'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
