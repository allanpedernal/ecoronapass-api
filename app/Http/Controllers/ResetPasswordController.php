<?php

namespace App\Http\Controllers;

use App\Services\UtilityService;
use App\Traits\JWTTrait;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PDO;

class ResetPasswordController extends Controller
{
    //
    use JWTTrait;

    /**
     * @OA\Get (
     *     path="/v1/reset-password/verification-code",
     *     tags={"Reset Password"},
     *     description="Get Reset Verification Code",
     *     summary="",
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *         description="user register mobile number.",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="code", type="string", example="verification code")
     *                      )
     *             ),
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function resetPasswordToken(Request $request): JsonResponse
    {
        try {
            $connection = app('pdo_connection')->getPdo();
            $errors = app(UtilityService::class)->validateRequest(Validator::make($request->all(), [
                'phone' => [
                    'required',
                    function($attribute, $value, $fail) use ($connection) {
                        $statement = $connection->prepare("SELECT * FROM [dbo].[Client] WHERE phone = :phone");
                        $statement->bindParam(':phone', $value);
                        $statement->execute();
                        $result = $statement->fetch(PDO::FETCH_ASSOC);
                        if (!$result) {
                            $fail($attribute.' does not exist!.');
                        }
                    }
                ]
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            $phone = $request->get('phone');
            #create link.
            $user = User::where(function ($query) use (& $phone) {
                $query->where('phone', $phone);
            })->first();
            if ($user) {
                $phone = $user->phone;

                #generate verification code.
                $code = app('utility')->generateRandomPin(6);

                #update client table.
                $user->reset_password_code = $code;
                if ($user->save()) {
                    #send sms.
                    app('utility')->sendVerificationCode($phone, $code, $user);
                }

                #$expiration = time() + (60 * 60);
                #$token = self::jwtEncode($user, $expiration);

                return response()->json([
                    'status' => true,
                    'message' => 'Reset Verification Code sent',
                    'data' => [
                        'code' => $code
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'User related to this phone not found!'
                ], 400);
            }

        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }


    /**
     * @OA\Post(
     *     path="/v1/reset-password",
     *     tags={"Reset Password"},
     *     description="Reset Password",
     *     summary="",
     *     @OA\RequestBody(
     *         description="Client Reset Password",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="password",
     *                      type="string",
     *                      format="password"
     *                   ),
     *                  @OA\Property(
     *                      property="re_password",
     *                      type="string",
     *                      format="password"
     *                   ),
     *                  @OA\Property(
     *                      property="code",
     *                      type="string"
     *                   ),
     *               ),
     *           ),
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                 @OA\Property(
     *                      property="password",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="re_password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="code",
     *                     type="string"
     *                 ),
     *                 example={
     *                      "password": "new password",
     *                      "re_password": "re password",
     *                      "code": "verification code",
     *                 }
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"status": true, "message": "Password successfully reset!"}
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function resetPassword(Request $request): JsonResponse
    {
        try {
            $password = $request->get('password');
            $code = $request->get('code');

            $errors = app(UtilityService::class)->validateRequest(Validator::make($request->all(), [
                'password' => 'required|min:6',
                're_password' => 'required|min:6|same:password',
                'code' => 'required|digits:6'
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            #$reset_token = self::jwtDecode($reset_token);
            #$reset_token = (array) $reset_token;

            #$now = Carbon::now();
            #$expiration = new Carbon($reset_token['exp']);
            #$expired = false;
            #if (!$expiration->gte($now)) {
                #$expired = true;
            #}

            #check code if exist.
            /*
             * to be refactored.
             * poor checking.
             * */

            $user = User::where('reset_password_code', $code)->first();

            if ($user) {
                #$user_data = $reset_token['data']->user;
                #$user = User::where('email', $user_data->{'email'})->first();
                $user->password = Hash::make($password);
                $user->reset_password_code = null;
                $user->save();
                return response()->json([
                    'status' => true,
                    'message' => 'Password successfully reset!'
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Code invalid!'
                ], 400);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'data' => $e->getMessage()
            ],500);
        }
    }
}
