<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SMSVerificationController extends Controller
{
    //
    protected $request;

    protected $pdo_connection;

    public function __construct(Request $request) {
        $this->request = $request;
        $this->pdo_connection = app('pdo_connection', ['sqlsrv'])->getPdo();
    }

    /**
     * @OA\Post(
     *     path="/v1/send-sms-verification",
     *     tags={"Verification PIN"},
     *     description="Send SMS Verification PIN",
     *     summary="",
     *     @OA\RequestBody(
     *         description="Phone",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="phone",
     *                      type="string"
     *                 ),
     *                 example={"phone": "1234567890"}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Status",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function sendSMSVerifcation(): JsonResponse
    {
        try {

            $phone = $this->request->get('phone');

            $errors = app('utility')->validateRequest(Validator::make($this->request->all(), [
                'phone' => [
                    'required',
                    'min:10',
                    function($attribute, $value, $fail) {
                        $verify_result = app('utility')->phoneVerification($value);
                        if ($verify_result['invalid']) {
                            $fail($attribute . ' ' . $verify_result['msg']);
                        }
                    }
                ]
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            #get user associated.
            $user = User::where('phone', $phone)->first();

            #send verification code.
            $pin = app('utility')->generateRandomPin(4);
            $message = '[' . env('APP_NAME') . '] To verify your account, please enter this verification pin: ' . $pin;

            if (app('utility')->sendVerificationCode($phone, $message, $user)) {
                #save the code for verification.
                $sql = "EXEC spm_update_sms_code @client_id = :client_id";
                $sql .= " ,@sms_verification_code = :sms_verification_code";
                $statement = $this->pdo_connection->prepare($sql);
                $statement->execute([
                    ':client_id' => $user->{'ID'},
                    ':sms_verification_code' => $pin
                ]);
            }

            return response()->json([
                'status' => true,
                'message' => 'Successfully send verification pin in the associated phone number.'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/v1/validate-pin",
     *     tags={"Verification PIN"},
     *     description="Validate PIN",
     *     summary="",
     *     @OA\RequestBody(
     *         description="Verification PIN and Phone",
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="phone",
     *                      type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="pin",
     *                     type="string"
     *                 ),
     *                 example={"phone": "1234567890", "pin": "1234"}
     *              ),
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Status",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function validatePin(): JsonResponse
    {
        try {
            $phone = $this->request->get('phone');
            $pin = $this->request->get('pin');

            $errors = app('utility')->validateRequest(Validator::make($this->request->all(), [
                'phone' => [
                    'required',
                    'min:10',
                    function($attribute, $value, $fail) {
                        $verify_result = app('utility')->phoneVerification($value);
                        if ($verify_result['invalid']) {
                            $fail($attribute . ' ' . $verify_result['msg']);
                        }
                    }
                ],
                'pin' => 'required|min:4'
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            #check combination.
            $client = User::where(function($query) use (& $phone, & $pin) {
                $query->where('phone', $phone)
                    ->where('sms_verification_code', $pin);
            })->first();

            if ($client) {
                if (!$client['sms_verified_at']) {
                    $client->sms_verified_at = Carbon::now()->format('m/d/Y H:i:s');
                    if ($client->save()) {
                        return response()->json([
                            'status' => true,
                            'message' => 'Successfully verified!'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'This code was already verified at ' . Carbon::parse($client['sms_verified_at'])->format('m/d/Y h:i A')
                    ], 400);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid PIN entered!'
                ], 400);
            }

        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
