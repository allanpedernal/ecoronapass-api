<?php

namespace App\Http\Controllers;

use PDO;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Exception;

class PatientController extends Controller
{
    /**
     * @OA\Get (
     *     path="/v1/patient/{id}",
     *     tags={"Patient"},
     *     description="Patient Detail",
     *     summary="",
     *     security={ {"bearer": {}} },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="user uniqueidentifier",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                      @OA\Items(
     *                          type="object",
     *                          @OA\Property(property="ID", type="string", format="uuid"),
     *                          @OA\Property(property="firstname", type="string", example="John"),
     *                          @OA\Property(property="lastname", type="string", example="Doe"),
     *                          @OA\Property(property="middlename", type="string", example="Doe"),
     *                          @OA\Property(property="suffix", type="string", example="III"),
     *                          @OA\Property(property="last_4_ssn", type="string", example="1234"),
     *                          @OA\Property(property="street_address", type="string", example="1234 Street"),
     *                          @OA\Property(property="city", type="string", example=""),
     *                          @OA\Property(property="state", type="string", example=""),
     *                          @OA\Property(property="county", type="string", example=""),
     *                          @OA\Property(property="zip", type="string", example="12345"),
     *                          @OA\Property(property="is_employer", type="string", example="1"),
     *                          @OA\Property(property="received_notification", type="string", example="1"),
     *                          @OA\Property(property="phone", type="string", example="1231231230"),
     *                          @OA\Property(property="email", type="string", example="john_doe@dummy.com"),
     *                          @OA\Property(property="dob", type="string", example="09/18/1989"),
     *                          @OA\Property(property="gender", type="string", example="M"),
     *                          @OA\Property(property="race", type="string", example="White"),
     *                          @OA\Property(property="ethnicity", type="string", example="Latino"),
     *                      )
     *             ),
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function patientDetail($id)
    {
        try {
            $sql = "SELECT * FROM [dbo].[Client] WHERE ID = :id";
            $connection = app('pdo_connection', ['sqlsrv'])->getpDo();
            $statement = $connection->prepare($sql);
            $statement->execute([':id' => $id]);
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            $user = null;
            if ($result) {
                $user['firstname'] =  $result['Firstname'];
                $user['lastname'] =  $result['Lastname'];
                $user['middlename'] =  $result['Middlename'];
                $user['suffix'] =  $result['suffix'];
                $user['mobile'] =  $result['phone'];
                $user['email'] =  $result['email'];
                $user['gender'] =  $result['Sex'];
                $user['dob'] =  $result['Dob'];
                $user['race'] =  $result['Race'];
                $user['ethnicity'] =  $result['Ethnicity'];
                $user['last_4_ssn'] =  substr($result['Ssn'], -4);

                $user['street_address'] =  $result['Address1'];
                $user['city'] =  $result['City'];
                $user['state'] =  $result['State'];
                $user['county'] =  $result['County'];
                $user['zip'] = $result['Zip'];
                $user['is_employer'] = $result['eamc_employee'];
                $user['received_notification'] = $result['sms_received_notification'];

                $user['ID'] =  $result['ID'];
                $user['device_id'] =  isset($result['device_id']) ? $result['device_id'] : '';
            }

            return response()->json([
                'status' => true,
                'message' => 'success',
                'data' => $user ?: []
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Put(
     *     path="/v1/patient/{id}",
     *     tags={"Patient"},
     *     description="Update Patient",
     *     summary="",
     *     security={ {"bearer": {}} },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="user uniqueidentifier",
     *     ),
     *     @OA\RequestBody(
     *         description="Patient Detail Update",
     *         required=true,
     *         @OA\JsonContent(
     *           required={"id", "patient_id","carrier_id","carrier_name","priority","member_id","amd_insurance_id"},
     *           @OA\Property(property="firstname", type="string", example="John"),
     *           @OA\Property(property="lastname", type="string", example="Doe"),
     *           @OA\Property(property="middlename", type="string", example="Sy"),
     *           @OA\Property(property="ssn", type="string", example="1234567890"),
     *           @OA\Property(property="email", type="string", example="johndoe@dummy.com"),
     *           @OA\Property(property="mobile", type="string", example="1234567890"),
     *           @OA\Property(property="dob", type="string", example="04/22/1982"),
     *           @OA\Property(property="sex", type="string", example="M"),
     *           @OA\Property(property="race", type="string", example="White"),
     *           @OA\Property(property="ethnicity", type="string", example="Asian"),
     *           @OA\Property(property="address1", type="string", example="123 address"),
     *           @OA\Property(property="city", type="string", example="Indianapolis"),
     *           @OA\Property(property="state", type="string", example="IN"),
     *           @OA\Property(property="zip", type="string", example="12345"),
     *           @OA\Property(property="county", type="string", example="Vermontt"),
     *           @OA\Property(property="suffix", type="string", example="Jr."),
     *           @OA\Property(property="notification", type="boolean", example="true"),
     *           @OA\Property(property="employee", type="boolean", example="true"),
     *         )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"success": true, "message": "Successfully updated!"}
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function updatePatient(Request $request, $id): JsonResponse
    {
        try {
            $connection = app('pdo_connection')->getPdo();

            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $dob = $request->get('dob');
            $email = $request->get('email');
            $mobile = $request->get('mobile');
            $gender = $request->get('sex');
            $race = $request->get('race');
            $ethnicity = $request->get('ethnicity');
            $zip = $request->get('zip');
            $ssn = $request->get('ssn');
            $middlename = $request->get('middlename');
            $county = $request->get('county');
            $address1 = $request->get('address1');
            $city = $request->get('city');
            $state = $request->get('state');
            $suffix = $request->get('suffix');
            $notification = $request->get('notification');
            $employee = $request->get('employee');

            $errors = app('utility')->validateRequest(Validator::make($request->all(), [
                'firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required|date:m/d/Y',
                'sex' => 'required',
                'zip' => 'required',
                'mobile' => [
                    'required',
                    function($attribute, $value, $fail) use (& $id, & $email) {
                        if (!app('utility')->clientExistingEntityVerifier($id, $value, 'phone', '[dbo].[Client]')) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ],
                'email' => [
                    'required',
                    function($attribute, $value, $fail) use (& $id) {
                        if (!app('utility')->clientExistingEntityVerifier($id, $value, 'email', '[dbo].[Client]')) {
                            $fail($attribute.' is already taken.');
                        }
                    }
                ]
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            $statement = $connection->prepare(
                "EXEC spm_update_client_detail
                  @client_id = :client_id,
                  @lastname = :lastname,
                  @firstname = :firstname,
                  @middlename = :middlename,
                  @dob = :dob,
                  @sex = :sex,
                  @ssn = :ssn,
                  @address1 = :address1,
                  @city = :city,
                  @state = :state,
                  @zip = :zip,
                  @county = :county,
                  @race = :race,
                  @ethnicity = :ethnicity,
                  @email = :email,
                  @phone = :phone,
                  @suffix = :suffix,
                  @sms_received_notification = :sms_received_notification,
                  @employee = :employee
              "
            );
            $statement->execute([
                ':client_id' => $id,
                ':lastname' => $lastname,
                ':firstname' => $firstname,
                ':middlename' => $middlename,
                ':dob' => $dob,
                ':sex' => $gender,
                ':ssn' => $ssn,
                ':address1' => $address1,
                ':city' => $city,
                ':state' => $state,
                ':zip' => $zip,
                ':county' => $county,
                ':race' => $race,
                ':ethnicity' => $ethnicity,
                ':email' => $email,
                ':phone' => $mobile,
                ':suffix' => $suffix,
                ':sms_received_notification' => $notification ? 1 : 0,
                ':employee' => $employee ? 1 : 0
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Successfully Updated'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }

    /**
     * @OA\Post(
     *     path="/v1/patient/update-deviceid/{id}",
     *     tags={"Patient"},
     *     description="Update Patient Device Id",
     *     summary="",
     *     security={ {"bearer": {}} },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="user uniqueidentifier",
     *     ),
     *     @OA\RequestBody(
     *         description="Patient Device ID Update",
     *         required=true,
     *         @OA\JsonContent(
     *           required={"id"},
     *           @OA\Property(property="device_id", type="string", example="1234"),
     *         )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="success",
     *                 type="boolean"
     *             ),
     *             @OA\Property(
     *                 property="message",
     *                 type="string"
     *             ),
     *             example={"success": true, "message": "Successfully updated Patient Device ID!"}
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Error"
     *     )
     * )
     */
    public function updatePatientDeviceId(Request $request, $id): JsonResponse
    {
        try {
            $connection = app('pdo_connection')->getPdo();

            $device_id = $request->get('device_id');
            
            $errors = app('utility')->validateRequest(Validator::make($request->all(), [
                'device_id' => 'required'
            ]));

            if (count($errors) > 0) {
                $err_mess = '';
                foreach($errors as $ek => $ev){
                    $err_mess = $ev;
                    break;
                }

                return response()->json([
                    'status' => false,
                    'message' => $err_mess,
                    'data' => null
                ], 400);
            }

            ////////////////////////////////////////////////////////////
            $connection = app('pdo_connection')->getPdo();
            $statement = $connection->prepare("UPDATE [dbo].[Client] SET device_id = :device_id where ID = :patient_id");
            $statement->bindParam(':device_id', $device_id);
            $statement->bindParam(':patient_id', $id);
            $statement->execute();
            ////////////////////////////////////////////////////////////

            return response()->json([
                'status' => true,
                'message' => 'Successfully Updated Patient Device ID'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ],500);
        }
    }
}
