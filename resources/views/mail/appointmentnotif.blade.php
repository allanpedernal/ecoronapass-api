Dear <strong>{{ ucwords($data['firstname']) }} {{ ucwords($data['lastname']) }}</strong>
<br>
<p>
You have a scheduled Covid Test on {{ $data['schedule'] }} at {{ $data['facility_name'] }}.
</p>

<p style="margin: 0; padding: 0">
	Regards,
</p>
<p style="margin: 0; padding: 0">
	eCoronpass
</p>