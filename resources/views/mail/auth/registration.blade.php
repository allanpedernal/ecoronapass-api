@component('mail::message')
    <div>
        <div class="content">
            <p>Hi {{ $data['user']['Firstname'] }} {{ $data['user']['Lastname']  }},</p>
            <p>Welcome! Thank you for using our app, below is your credential accessing the application.</p>
            <p>Username: <strong>{{ $data['user']['username'] }}</strong></p>
            <p>Password: <strong>{{ $data['password'] }}</strong></p>
            <p>SMS verification code has been sent to your registered mobile number.</p>
            <p><i>System generated, do not reply.</i></p>
        </div>
    </div>
@endcomponent
