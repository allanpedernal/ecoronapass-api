<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// api/v1
$router->group(['prefix' => 'v1'], function () use ($router) {
    // token
    $router->post('auth/token', 'AuthenticateController@authToken');

    // auth
    $router->group(['prefix' => 'auth'], function() use ($router) {
        $router->post('/login', 'AuthenticateController@login');
        $router->post('/register', 'AuthenticateController@register');
    });

    // reset password
    $router->get('/reset-password/verification-code', 'ResetPasswordController@resetPasswordToken');
    $router->post('/reset-password', 'ResetPasswordController@resetPassword');

    // SMS Verification.
    $router->post('/send-sms-verification', 'SMSVerificationController@sendSMSVerifcation');
    $router->post('/validate-pin', 'SMSVerificationController@validatePin');

    // protected endpoints.
    $router->group(['middleware' => 'authorize'], function() use ($router) {
        // test
        $router->get('/test', function() { return 'Welcome'; });

        // patient
        $router->get('/patient/{id}', 'PatientController@patientDetail');
        $router->put('/patient/{id}', 'PatientController@updatePatient');
        $router->post('/patient/update-deviceid/{id}', 'PatientController@updatePatientDeviceId');

        // covid test
        $router->group(['prefix' => 'covid-test'], function() use ($router) {
            // PATIENT COVID TEST RESULT
            $router->get('/patient/history/list/{patient_id}', 'CovidTest\CovidTestResultController@getTestResults');
            $router->get('/patient/history/detail/{test_result_id}', 'CovidTest\CovidTestResultController@getTestResultDetail');

            // Facility Covid Testing Location
            $router->get('/facility/list', 'CovidTest\FacilityController@facilitiesList');
            $router->get('/facility/get-availability/{facility_id}', 'CovidTest\FacilityController@facilitiesAvailability');

            //  Pre-Covid Test Survey
            $router->get('/pre-covid/get-test-survey-form', 'CovidTest\PreCovidTestSurveyController@getTestSurveyForm');
            $router->post('/pre-covid/submit-survey', 'CovidTest\PreCovidTestSurveyController@submitSurvey');

            $router->get('/patient-appointment/get/{patient_id}', 'CovidTest\PatientAppointmentController@getAppointment');
            $router->post('/patient-appointment/save', 'CovidTest\PatientAppointmentController@saveAppointment');
            $router->post('/patient-appointment/cancel', 'CovidTest\PatientAppointmentController@cancelAppointment');

            // Facility Test Schedules
            $router->get('/available-schedules', 'CovidTest\ScheduleController@getSchedules');
        });

        $router->group(['prefix' => 'lookups'], function() use ($router) {
            $router->get('/race/list', 'LookupController@raceList');
            $router->get('/ethnicity/list', 'LookupController@ethnicityList');
            $router->get('/state/list', 'LookupController@stateList');
        });

        // contact tracing
        $router->group(['prefix' => 'contact/tracing'], function() use ($router) {
            $router->get('/{client_id}', 'ContactTracingController@show');
            $router->put('/{id}', 'ContactTracingController@update');
            $router->post('/', 'ContactTracingController@store');
            $router->delete('/{id}', 'ContactTracingController@destroy');
        });
    });
});




